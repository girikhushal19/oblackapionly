let mobile
let tshirt
let belt

mobile:
    price
    images
    brand
    color
    features
    dates

tshirt:
    price
    images
    brand
    size
    color
    features
    dates

belt:
    price
    images
    size
    color
    features
    dates