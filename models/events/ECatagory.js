const mongoose=require("mongoose");
const ECatagorySchema=new mongoose.Schema({
   
    name:{type:String},
    photos:{type:Array,default:[]}
},
{
    timestamps:true
}
);


module.exports=mongoose.model("ECatagori",ECatagorySchema);