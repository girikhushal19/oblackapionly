const mongoose = require("mongoose");
const Schema = require("mongoose");
// const {cartSchema}=require("../cart/Cart");
const deliveryInstructionSchema=new mongoose.Schema({
    accesscode:{type:String},
    interphone:{type:String},
    requiredcode:{type:String},
    instructions:{type:String}
})
const AddressSchema=new mongoose.Schema({
    type:{type:String,default:""},
    country:{type:String,default:""},
    name:{type:String,default:""},
    countryCode:{type:String,default:""},
    telephone:{type:String,default:""},
    address:{type:Object,default:{}},
    pobox:{type:String,default:""},
    city:{type:String,default:""},
    delivery_instructions:{type:deliveryInstructionSchema},
    is_shipping_address:{type:Boolean,default:false},
    is_billing_address:{type:Boolean,default:false},
    Location: {
     type: { type: String,default:"Point" },
     coordinates: []
    }

})
const favEventCatSchema=new mongoose.Schema({
  category:{type:String,default:null}
});
const cartSchema=new mongoose.Schema({
  product:{type:Array,default:[]},
  quantity: {
      type: Number,
      required: true,
    },
  price: {
      type: Number,
      required: true,
    },
    variation_color:{type:String,default:""},
    variation_image:{type:String,default:""},
    variation_name:{type:String,default:""},
    variation_value:{type:String,default:""},
    variation_id:{type:String,default:""},
    is_available:{type:Boolean,default:true},
    total_quantity:{type:Number,default:0},
    // variationdata:{type:Array,default:[]},
    // variations:{type:Array,default:[]},
  // totalPrice:{
  //     type: Number,
  //     required: true,
  // }
})

const eventcartSchema=new mongoose.Schema({
  event:{type:Array,default:[]},
  quantity: {
      type: Number,
      required: true,
    },
  price: {
      type: Number,
      default: 0,
    },
    edate:{type:Date,default:new Date()},
    etime:{type:String,default:""},
    sdate:{type:Date,default:new Date()},
    stime:{type:String,default:""},
    ticketype:{type:String,default:""},
})
const userSchema = new mongoose.Schema({
  photo: {type:String,default:""},
  user_type: { type: String,enum : ['user','merchant'], default: 'user' },
  first_name: { type: String,required:true },
  last_name: { type: String, required:true },
  email: { type: String, unique: true },
  phone:{type:String,default:""},
  country_code:{type:String,default:""},
  password: { type: String },
  address: { type: [AddressSchema],default:[]},
 
  token: { type: String, default: null },
  companyName: { type: String, default: null },
  userAddress: { type: String, default: null },
  userLocation: {
   type: { type: String },
   coordinates: []
  },
  forgotPasswordLink: { type: String, default: null },
  forgotPasswordTime: { type: Date, default: null },
  forgotPasswordUsed: { type: Number, default: 0 },
  is_deleted:{type:Boolean,default:false},
  is_active:{type:Boolean,default:true},
  fcm_token:{type:String},

  isverfied:{type:Boolean,default:false},
  verification_code:{type:String},
  otp_date_time: { type: Date, default: null },
  otp_used_or_not:{type:Number, default:0},
  reset_password_token: {
    type: String
  },
  reset_password_expires: {
    type: Date
  },
  credit_limit: { type: Number, default: 0 },
  extProvider:{type:Boolean,default:false},
  loggedin_time: { type: Date, default: null },
  cart:{type:[cartSchema]},
  eventcart:{type:[eventcartSchema]},
  fav_event_cat:{type:[favEventCatSchema],default:[]},
  last_login_time:{type:Date,default:null},
},
{
  timestamps:true
});




// {
//   "userid":"63bd435dee7014ce11fe2cf3",
//   "productid":"63ca9c02ea3da53ed403982e",
//   "quantity":"4"
// }
//address type="billing" or "shipping" or type=""
userSchema.index({userLocation: '2dsphere'});
module.exports = mongoose.model("user", userSchema);