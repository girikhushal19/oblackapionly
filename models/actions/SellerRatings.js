const mongoose=require("mongoose");
const SellerRatingsSchema=new mongoose.Schema({
    user_id:{type:String},
    seller_id:{type:String},
    user_name:{type:String},
    seller_name:{type:String},
  
    rating:{type:Number},
    review:{type:String},
    media:{type:Array}
    
},{
    timestamps:true
});
module.exports=mongoose.model("SellerRatings",SellerRatingsSchema);