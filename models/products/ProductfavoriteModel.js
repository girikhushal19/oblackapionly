
const mongoose = require("mongoose");
const ProductfavoriteSchema = new mongoose.Schema({
    user_id: { type: String, required: true },
    product_id: { type: String, required: true }
});
module.exports = mongoose.model("productfavorite", ProductfavoriteSchema);