const mongoose=require("mongoose");
const CatagorySchema=new mongoose.Schema({
   mastercatagory:{type:mongoose.Types.ObjectId},
   //title:{type:String,unique:true},
   title:{type:String,default:''},
   image:{type:String,default:""},
   is_featured:{type:Boolean,default:false},
   is_active:{type:Boolean,default:true},
   // is_parent:{type:Boolean},
//    price:{type:Number}
});
module.exports=mongoose.model("Catagory",CatagorySchema);