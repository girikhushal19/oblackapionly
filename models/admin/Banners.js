const mongoose=require("mongoose");
const BannerSchema=mongoose.Schema({
    location:{type:String,default:""},
    url:{type:String,default:""},
    title:{type:String,default:""},
    text:{type:String,default:""},
    image:{type:String,default:""},
});
module.exports=mongoose.model("Banner",BannerSchema);