const mongoose=require("mongoose");
const AbuseReportsSchema=new mongoose.Schema({
   
    reason:{type:String},
    product:{type:String},
    productId:{type:String},
    vendor:{type:String},
    vendorId:{type:String},
    content:{type:String},
    media:{type:Array},
    reportedbyName:{type:String},
    reportedbyId:{type:String},
    is_resolved:{type:Boolean,default:false}
    
},
{
    timestamps:true
}
);


module.exports=mongoose.model("AbuseReports",AbuseReportsSchema);