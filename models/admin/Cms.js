const mongoose=require("mongoose");
const CmsSchema=mongoose.Schema({
   
   page_name:{type:String,required:true},
    page_content:{type:String,required:true},
});
module.exports=mongoose.model("Cms",CmsSchema);