const mongoose=require("mongoose");
const EPaymentRequestSchema=mongoose.Schema({
   
    org_id:{type:String,required:true},
    email:{type:String,required:true},
    bank:{type:String,required:true},
    accountname:{type:String,required:true},
    iban:{type:String,required:true},
    bic:{type:String,required:true},
    org_name:{type:String,required:true},
    request_amount:{type:String,required:true},
    request_date:{type:Date,default:new Date()},
    payment_status:{type:Boolean,default:false},
    is_rejected:{type:Boolean,default:false},
    note:{type:String,default:""}
},{
    timestamps:true
  });
module.exports=mongoose.model("EPaymentRequest",EPaymentRequestSchema);