const mongoose = require("mongoose");

const DriversSchema = new mongoose.Schema({
  fullName: { type: String, default: null },
  email: { type: String, unique: true },
  mobileNumber: { type: Number, default: null },
  vehicleType: { type: String, default: null },
  shiftStartTime: { type: Date, default: null },
  shiftEndTime: { type: Date, default: null },
  breakStartTime: { type: Date, default: null },
  breakEndTime: { type: Date, default: null },
  password: { type: String },
  token: { type: String, default: null },
  deviceToken: { type: String, default: null },
  startAddress: { type: String, default: null },
  startLocation: {
   type: { type: String },
   coordinates: []
  },
  endAddress: { type: String, default: null },
  endLocation: {
   type: { type: String },
   coordinates: []
  },
  status: { type: Number, default: 1 }, // 0 Not verified 1 verified 2 unapproved
  deleted_at: { type: Number, default: 0 },
  loggedIn: { type: Number, default: 0 },//0 logout 1 login
  onLineOffLine:{ type: Number, default: 0 },//0 off line 1 on line
  trip_status:{ type: Number, default: 0 },//0 free  1 busy
  userImage: { type: String, default: null },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

DriversSchema.index({startLocation: '2dsphere'});
DriversSchema.index({endLocation: '2dsphere'});
module.exports = mongoose.model("drivers", DriversSchema);