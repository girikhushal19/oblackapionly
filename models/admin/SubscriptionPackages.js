const mongoose=require('mongoose');
const SubscriptionPakagesSchema=new mongoose.Schema({
   
    package:{type:String,required:true},
    plusperproduct:{type:Number,default:0},
    extracommission:{type:Number,default:0},
    price:{type:Number,required:true},
    Numberofitems:{type:Number,required:true},
    duration:{type:Number,required:true},
    description:{type:String,required:true},
    type:{type:String,required:true},
    is_limited:{type:Boolean,default:true}
    

});
module.exports=mongoose.model('SubscriptionPackages',SubscriptionPakagesSchema);