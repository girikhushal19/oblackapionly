const mongoose = require("mongoose");
const ServicesSchema = new mongoose.Schema({
   
    service_type: { type: String, required: true ,unique:true},
    service_description: { type: String, required: true },
});
module.exports = mongoose.model("Services", ServicesSchema);