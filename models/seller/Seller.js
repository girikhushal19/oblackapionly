const mongoose=require("mongoose");
const bankSchema=new mongoose.Schema({
    is_default:{type:Boolean,default:false},
    bank_name: { type: String },
    account_number: { type: String },
    account_name: { type: String },
    bank_address: { type: String },
    latlong:{type:Array,default:[]},
    iban:{ type: String },
    bic:{ type: String },
});
const AddressSchema=new mongoose.Schema({
    latlong:{type:Array,default:[]},
    address:{type:String,default:""}

})
const SellerSchema=mongoose.Schema({
    vendortype:{type:String,default:""},
    fullname:{type:String,default:""},
    companyname:{type:String,default:""},
    ICorPassport:{type:String,default:""},
    ICorPassportimage:{
        front:{type:String,default:""},
        back:{type:String,default:""}
    },
    isverfied:{type:Boolean,default:false},
    verification_code:{type:String,default:""},
    shopname:{type:String,default:""},
    shopdesc:{type:String,default:""},
    shopphoto:{type:String,default:""},
    pickupaddress:{type:[AddressSchema],default:[]},
    email:{type:String,unique:true},
    countrycode:{type:String,default:""},
    phone:{type:String,default:null},
    photo:{type:String,default:""},
    password:{type:String,default:""},
    is_approved:{type:Boolean,default:false},
    is_active:{type:Boolean,default:false},
    is_deleted:{type:Boolean,default:false},
    banks:[bankSchema],
    payment_mobile:{ type: String,default:"" },
    token: { type: String ,default:""},
    reset_password_token: {
        type: String,default:""
    },
    reset_password_expires: {
        type: Date
    },
    fcm_token:{type:String,default:""},
  // payment_frequency:{type:String,default:""},
    totalpaidamount:{type:Number,default:0},
    totalpendingamount:{type:Number,default:0},
    web_reg_step:{type:Number,default:2},
    extProvider:{type:Boolean,default:false},
    is_subscribed:{type:Boolean,default:false},
    is_registered:{type:Boolean,default:false},
    admin_commission_type:{type:String,default:""},
    admin_commission:{type:Number,default:0},
    sub_id:{type:String,default:""},
    sub_type:{type:Number,default:0},
    forgot_pw_date:{type:Date,default:null},
    company_number:{type:String,default:""},
    registration_card:{type:String,default:null},
    company_registration_number:{type:String,default:null},
    created_at:{type:Date,default: new Date().toISOString() },
    updated_at:{type:Date,default: new Date().toISOString() },
    last_login_time:{type:Date,default:null},
},{
    timestamps:true
  });
module.exports=mongoose.model("Seller",SellerSchema);