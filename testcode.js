checkoutcart: async (req, res) => {
    const {
        billingaddress,
        shippingaddress,
        from,
        shipping_city,
        shipping_details,
        code,
        id
    } = req.body;
    let userId = id;
    const coupon = await CoupensModel.findOne({ code: code });
    const user = await User.findById(id);
    console.log("coupon", coupon)
    let shippingmethod;
    const shipping_address = user?.address?.filter(e => e.is_shipping_address == true)[0] || user?.address[0];
    const billing_address = user?.address?.filter(e => e.is_billing_address == true)[0] || user?.address[0];
    const productsincoupon = coupon?.products || [];
    const coupontype = coupon?.type;
    const couponamount = coupon?.discountamount;
    let products = (await User.findById(id))?.cart;
    console.log("shipping_address", shipping_address, "billing_address", billing_address, "user", user)
    const istheresomethingincart = products?.length;
    let totalprice = 0;
    let totalcommission = 0;
    let totalshippingcost = 0;
    let totalproductsinqty = 0;
    let totalproductsinorder = 0;
    let finalprice = 0;
    let totaltax = 0;
    let discounted_amount = 0;
    let totalpaymentamount = 0;
    let totalplusperproduct = 0;
    let nonshippableproducts = [];
    let shippingmethods = [];
    if (istheresomethingincart) {
        let totalprice = 0
        totalproductsinorder = products.length
        const shippingcostsArray = [];
        if (products.length) {

            for (let i = 0; i < products.length; i++) {
                product = products[i]
                let seller_will_bear_shipping = product?.seller_will_bear_shipping;
                let productprice = product.varientPrice || product?.price;
                let qty = product.quantity;
                // console.log("productprice=====>",productprice,"product.varientPrice",product.varientPrice,"product?.price",product?.price)
                let data3 = {
                    productprice: productprice, product_id: product.product._id, product_qty: qty, shipping_city: shipping_city
                }

                await calculateShippingcostforproducts(data3).then((result) => {
                    // console.log("product",product,"result",result)
                    if (!result.result.length) {
                        nonshippableproducts.push(product);

                        product["is_shippable"] = false;
                        product["shipping_cost"] = 0;
                        product["discountedPrice"] = productprice;
                        // discounted_amount=discounted_amount+(productprice)*qty;

                        console.log("discounted price in case of not shippable====>", discounted_amount, "(productprice)*qty", (productprice) * qty)
                    } else {
                        if (productsincoupon?.includes(product.product._id.toString())) {
                            if (coupontype == "a") {
                                product["couponApplied"] = coupon.code;
                                product["discountedPrice"] = productprice - couponamount;
                                discounted_amount = discounted_amount + (productprice - couponamount) * qty;
                                totalpaymentamount = totalpaymentamount + (productprice * qty);
                            } else if (coupontype == "p") {
                                product["couponApplied"] = coupon.code;
                                product["discountedPrice"] = productprice - (productprice * couponamount / 100);
                                discounted_amount = discounted_amount + (productprice - ((productprice * couponamount) / 100)) * qty;
                                totalpaymentamount = totalpaymentamount + (productprice * qty);
                            }
                        } else {
                            product["discountedPrice"] = productprice;
                            discounted_amount = discounted_amount + (productprice) * qty;
                            totalpaymentamount = (totalpaymentamount + productprice) * qty;
                        }
                        totalshippingcost = totalshippingcost + result.result[0].shippingcostforproducts
                        product["is_shippable"] = true;
                        product["shipping_cost"] = result.result[0].shippingcostforproducts
                        product["plusperproduct"] = result.plusperproduct;
                        totalplusperproduct += result.plusperproduct;
                        product["varientPrice"] = productprice
                        product["tax"] = result.producttax
                        totaltax = totaltax + product["tax"];
                        product["commission"] = result.productcommission;

                        let sellerprofit = 0;
                        let pricetouser = 0;
                        if (product?.seller_will_bear_shipping) {
                            pricetouser = product["discountedPrice"] * qty;
                            sellerprofit = product["discountedPrice"] * qty - result.plusperproduct - product["shipping_cost"] - result.productcommission - result.producttax;
                        } else {
                            pricetouser = product["discountedPrice"] * qty + product["shipping_cost"]
                            sellerprofit = product["discountedPrice"] * qty - result.plusperproduct - result.productcommission - result.producttax;
                        }
                        product["variation_id"] = product.variation_id
                        shippingmethod = result.result[0];
                        // shippingmethods.push(shippingmethod)
                        product["shipping_details"] = shippingmethod;
                        product["shipping_id"] = shippingmethod._id + ""
                        totalcommission += result.productcommission
                        finalprice = result.plusperproduct + product["shipping_cost"] + product["discountedPrice"] * qty + result.productcommission + result.producttax;
                        sellerprofit = sellerprofit;
                        product["finalprice"] = finalprice
                        product["sellerprofit"] = sellerprofit
                        product["pricetouser"] = pricetouser
                        product["variation_id"] = product.variation_id
                        product["variations"] = product.variations
                    }

                    // console.log("productsincoupon===>",productsincoupon,"product.product._id",product.product._id)
                    // data={
                    //     productcommission:result.productcommission,
                    //     producttax:result.producttax,
                    //     shippingcost:result[0].costforproducts
                    // }
                    // shippingcostsArray.push(data);
                })

            }
            products = products.filter(elem => elem.is_shippable == true)
            totalprice = products?.reduce((accm, elem) => {
                // console.log("elem", elem)
                if ((elem?.finalprice) && elem.quantity) {
                    totalproductsinqty += elem.quantity;
                    let productprice = elem?.finalprice;
                    return accm + (productprice)
                }
                return accm;
            }, 0);
        }

        const groupbyshippingmethods = groupByfunc(products, 'shipping_id');
        const keys = Object.keys(groupbyshippingmethods)
        // groupbyshippingmethods=null
        console.log("keys===>", keys)
        const allorders = [];
        keys?.map((key) => {
            productsobject = groupbyshippingmethods[key]
            productsobject?.map(product => allorders.push(product))
            allorders.push()
        })
        console.log("allorders===>", allorders)
        if (groupbyshippingmethods.length == 0) {
            return res.send({
                status: false,
                message: "product/products are not shipable"
            })
        } else if (groupbyshippingmethods.length == 1) {
            const newOrder = new orders();
            newOrder.products = products;
            // newOrder.address = address;
            newOrder.totalproductsinqty = totalproductsinqty;
            newOrder.totalproductsinorder = totalproductsinorder;
            newOrder.userId = userId;
            newOrder.totaltax = totaltax;
            newOrder.shippingaddress = shipping_address;
            newOrder.billingaddress = billing_address;
            newOrder.discounted_amount = discounted_amount;
            newOrder.totalPrice = totalprice;
            newOrder.totalplusperproduct = totalplusperproduct;
            newOrder.totalcommission = totalcommission;
            newOrder.payment_amount = totalpaymentamount;
            newOrder.totalshipping = totalshippingcost;
            newOrder.shipping_details = shipping_details;
            // newOrder.totalproductsinorder = products.length;
            newOrder.save().then((result) => {
                if (result) {
                    const url = BASE_URL + "/api/common/getpayment/" + result._id + "/" + result.totalPrice + "/" + result.userId + "/" + from;

                    return res.send({
                        status: true,
                        url: url,
                        nonshippableproducts: nonshippableproducts,
                        message: "success"
                    })
                } else {
                    return res.send({
                        status: false,
                        message: "failed"
                    })
                }
            })
        } else {
            const combo_id = stringGen(20);
            const allorders = [];
            keys?.map((key) => { allorders.push(groupbyshippingmethods[key]) })
            const allsavedorders = []
            for (let j = 0; j < keys.length; j++) {
                let oneorder = allorders[key[j]];
                console.log(oneorder)
                const newOrder = new orders();
                newOrder.products = oneorder.oneorder;
                newOrder.combo_id = combo_id;
                // newOrder.address = address;
                newOrder.totalproductsinqty = oneorder.totalproductsinqty;
                newOrder.totalproductsinorder = oneorder.totalproductsinorder;
                newOrder.userId = userId;
                newOrder.totaltax = oneorder.totaltax;
                newOrder.shippingaddress = oneorder.shipping_address;
                newOrder.billingaddress = oneorder.billing_address;
                newOrder.discounted_amount = oneorder.discounted_amount;
                newOrder.totalPrice = oneorder.totalprice;
                newOrder.totalplusperproduct = oneorder.totalplusperproduct;
                newOrder.totalcommission = oneorder.totalcommission;
                newOrder.payment_amount = oneorder.totalpaymentamount;
                newOrder.totalshipping = oneorder.totalshippingcost;
                newOrder.shipping_details = oneorder.shipping_details;
                allsavedorders.push(newOrder)
                // newOrder.totalproductsinorder = products.length;
                newOrder.save();

            }
            console.log("allsavedorders", allsavedorders)
            const totalprice = allorders.reduce((acc, e) => {
                acc += e.pricetouser
                return acc
            }, 0)
            console.log("totalprice", totalprice)
            if (totalprice) {
                const url = BASE_URL + "/api/common/getpaymentforcombo/" + combo_id + "/" + totalprice + "/" + userId + "/" + from;

                return res.send({
                    status: true,
                    url: url,
                    nonshippableproducts: nonshippableproducts,
                    message: "success"
                })
            } else {
                return res.send({
                    status: false,
                    message: "failed"
                })
            }
        }
        // const totalcommissions=200;



    } else {
        return res.send({
            status: false,
            message: "cart is empty"
        })
    }

}