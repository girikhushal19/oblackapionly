const express =require('express');
const UserController = require('./UserController');
const router = express.Router();
const verifyToken= require("../../middleware/authClient");
const multer=require("multer");
const path=require("path");
const user_path=process.env.USER_PATH;
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.resolve(user_path))
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
  });
const uploadTo = multer({ storage: storage });
router.post('/createuser',uploadTo.fields([
    { 
      name: 'photo', 
      maxCount: 10
    }
  ]
  ),UserController.createnewuser);
router.post("/userRegistration",UserController.userRegistration);
router.get("/markuserasactive/:id",verifyToken,UserController.markUserAsActive)
router.get("/getalluserforfilter",UserController.getalluserforfilter)
router.get("/markuserasinactive/:id",UserController.markUserAsInActive)
router.post('/login',UserController.login)
router.post('/forgotpassword',UserController.forgotpassword);

router.post('/userForgotPassword',UserController.userForgotPassword);
router.post("/userSubmitForgotPassword", UserController.userSubmitForgotPassword);

router.get("/logout/:id", UserController.logout);
router.get("/getuserprofile/:id", UserController.getuserprofile);
router.post("/markaddressasbillingaddress", UserController.markaddressasbillingaddress);
router.post("/markaddressasshippingaddress", UserController.markaddressasshippingaddress);
router.post("/getbillingaddress", UserController.getbillingaddress);
router.post("/getshippingaddress", UserController.getshippingaddress);
router.post("/deletemultipleuser", UserController.deletemultipleuser);
router.post("/updateaddress", UserController.updateaddress);
router.get("/reset_password",UserController.render_reset_password_template);
router.post("/reset_password",UserController.reset_password);
router.post("/deleteaddress",UserController.deleteaddress);
router.post("/verifycode", UserController.verifycode);
router.get("/push_testing/:id", UserController.push_testing);
router.post("/resendOtp", UserController.resendOtp);
router.post("/checkEmailExist", UserController.checkEmailExist);
router.post("/updateAddressDetail", UserController.updateAddressDetail);
router.post("/getUserSingleAddress", UserController.getUserSingleAddress);

module.exports=router