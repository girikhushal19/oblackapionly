const User = require("../../models/user/User");
const bycrypt = require("bcryptjs");
const mongoose=require("mongoose")
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const async = require("async");
const base_url = process.env.BASE_URL;
const path = require("path");
const ejs = require('ejs');
const customConstant = require('../../helpers/customConstant');
const {sendmail}=require("../../modules/sendmail");
/////const hbs = require('nodemailer-express-handlebars');
const apptext_cmsmodel=require("../../models/admin/Apptext");

var FCM = require('fcm-node');
var serverKey = process.env.FCM_SERVER_KEY; //put your server key here
var fcm = new FCM(serverKey);
const transport = nodemailer.createTransport({
    name: "Oblack",
    host: process.env.MAILER_HOST,
    port: process.env.MAILER_PORT,
    auth: {
      user: process.env.MAILER_EMAIL_ID,
      pass: process.env.MAILER_PASSWORD,
    }
});

function generatePassword(digit) {
    var length = digit,
        charset = "0123456789",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
}
async function sendemail(req, res, data, done) {

    var smtpTransport = nodemailer.createTransport({
        host: process.env.MAILER_HOST,
        port: process.env.MAILER_PORT,
        secure: false,
        auth: {
            user: process.env.MAILER_EMAIL_ID,
            pass: process.env.MAILER_PASSWORD,
        },
        tls: {
            rejectUnauthorized: false
        }
    });

    smtpTransport.verify(function (error, success) {

        if (error) {
            console.log(error);
        } else {
            console.log('Server is ready to take our messages');
        }

    })
    const htmldata = await ejs.renderFile(path.resolve("./views/auth/" + data.template), {
        name: data.context.name,
        url: data.context.url
    });
    
    data.html = htmldata;

   
    smtpTransport.sendMail(data, function (err) {
        if (!err) {
            return res.json({
                status: true,
                message: "Veuillez vérifier votre e-mail pour de plus amples instructions",
                errmessage: "",
                data: null,
            });
        } else {
            console.log("error in email", err)
            return done(err);
        }
    });
};
module.exports = {
    createnewuser: async (req, res) => {
        // console.log("here 70");
         //console.log(req.files);
        
          console.log(req.body);
        //  console.log(JSON.stringify(req.body));
         //return false;
        
        const {
            first_name,
            last_name,
            email,
            password,
            address,
            phone,
            newpassword,
            status,
            from,
            countryCode
        } = req.body;

        var country_code = req.body.countrycode;
        let photo;
        let encryptedPassword
        //console.log("req.body",req.body)
        if (!email) {
            return res.send({
                status: false,
                message: "something is required"
            })
        }
        //console.log("req.body",req.body)
        if (req?.files?.photo) {
            photo = "user/" + req.files.photo[0].filename;
        }
        let hashedPass;
        if(newpassword&&password){
            encryptedPasswordtest = await bycrypt.hash(password, 10);
            
            const user = await User.findOne({ email:email });
            //console.log(user)
           const ispasswordcorrect=await bycrypt.compare(password, user.password);
           if(!ispasswordcorrect){
            return res.send({
              status:false,
              message:"L'ancien mot de passe n'est pas correct",
              errmessage:"L'ancien mot de passe n'est pas correct",
              data:null
            })
           }else{
            hashedPass= await bycrypt.hash(newpassword, 10);
           }
           }
        else if (password) {
            hashedPass= await bycrypt.hash(password, 10);
            
        }else if(!password&&newpassword){
            if(from=="admin"){
                hashedPass = await bycrypt.hash(newpassword, 10);
            }else{
                return res.send({
                    status:false,
                    message:"",
                    errmessage:"L'ancien mot de passe est requis",
                    data:null
                  })
            }
        }
        let newaddress;
        try {
            // {
            //     "type":"billing",  
            //     "country":"india",
            //     "name":"rohit sharma RJS",
            //     "telephone":"+91-8290650729",
            //     "address":"up some avenue which is unreacheable",
            //     "city":"jaipur",
            //     "delivery_instructions":{
            //         "accesscode":"222222",
            //         "interphone":"603",
            //         "requiredcode":"164"
            //     },
            //     "is_shipping_address":true,
            //     "is_billing_address":false,
            //     "_id":"63d7c70e44fd6fb9c597bb26"
            //     }
            newaddress = JSON.parse(address);
           
            console.log("try newaddress", newaddress,"newaddress.address",newaddress.address)
            // parsedaddress=JSON.parse(newaddress.address)
            // newaddress.address=parsedaddress
        } catch (e) {
            newaddress = address;
            // console.log("catch newaddress", e)
        }
        let newstatus;
        if(status){
            if(status=="active"||status=="1"){
                newstatus=true
            }else if(status=="inactive"||status=="0"){
                newstatus=false
            }
        }
        let datatosave = {
            first_name,
            last_name,
            email,
            password: hashedPass,
            address:newaddress,
            phone:phone,
            country_code:country_code
        }
        let olduser = await User.findOne({ email: email });
        let isaddressalreadythere=olduser?.address.filter((add)=>{
            // add._id=mongoose.Types.ObjectId(newaddress._id)
            if(add?._id==newaddress?._id){
                console.log("newaddress._id",newaddress._id);
                return add
            }
            
        })
        console.log("isaddressalreadythere",isaddressalreadythere)
        console.log("newaddress",newaddress)
        if (olduser)
        {
            if(isaddressalreadythere.length){
                const query = { email:olduser.email };
                const newdelivery_instructions={
                    ...(newaddress.delivery_instructions.accesscode&&{accesscode:newaddress.delivery_instructions.accesscode}),
                    ...(newaddress.delivery_instructions.interphone&&{interphone:newaddress.delivery_instructions.interphone}),
                    ...(newaddress.delivery_instructions.requiredcode&&{requiredcode:newaddress.delivery_instructions.requiredcode}),
                    ...(newaddress.delivery_instructions.instructions&&{instructions:newaddress.delivery_instructions.instructions}),
                }
                const updateDocument = {
                    "address.$[item]": {
                        is_shipping_address:isaddressalreadythere[0].is_shipping_address,
                        is_billing_address:isaddressalreadythere[0].is_billing_address,
                        _id:mongoose.Types.ObjectId(newaddress._id),
                        ...(newaddress.type&&{type:newaddress.type}),
                        ...(newaddress.country&&{country:newaddress.country}),
                        ...(newaddress.name&&{name:newaddress.name}),
                        ...(newaddress.countryCode&&{name:newaddress.countryCode}),
                        
                        ...(newaddress.telephone&&{telephone:newaddress.telephone}),
                        ...(newaddress.address&&{address:newaddress.address}),
                        ...(newaddress.pobox&&{pobox:newaddress.pobox}),
                        ...(newaddress.city&&{city:newaddress.city}),
                        ...(Object.keys(newdelivery_instructions).length
                            &&{delivery_instructions
                                :newdelivery_instructions
                            })
                    } 
                };
                console.log("updateDocument",updateDocument,"newaddress",newaddress)
                const options = {
                  arrayFilters: [
                    {
                      "item._id": newaddress._id,
                     
                    },
                  ],
                };
                const result = await User.updateOne(query, updateDocument, options);
                console.log("result",result,"cinetpayForSub.ejs")
            }
            await User.findByIdAndUpdate(olduser?._id,{
                ...(first_name && { first_name: first_name }),
                ...(last_name && { last_name: last_name }),
                ...(address&&!isaddressalreadythere.length &&{$push: { address: newaddress }}),
                ...(phone&&{phone:phone}),
                ...(photo && { photo: photo }),
                ...(hashedPass && { password: hashedPass }),
                ...(status&&{is_active:newstatus}),
                ...(country_code&&{country_code:country_code})
            },{returnDocument: 'after'}).then((re) => {
                return res.send({
                    status: true,
                    message: "Les détails ont été mis à jour avec succès",
                    data:re
                })
            })
        } else {
            return res.send({
                status: false,
                message: "L'identifiant de l'utilisateur n'est pas valide"
            })
        }
    },
    userRegistration: async (req, res) => {
        // console.log("here 70");
         //console.log(req.files);
        
          //console.log(req.body);
        //  console.log(JSON.stringify(req.body));
         //return false;
        
        const {
            first_name,
            last_name,
            email,
            password,
            phone,
            status
        } = req.body;

        var country_code = req.body.countrycode;
        let photo;
        let encryptedPassword
        //console.log("req.body",req.body)
        if (!email) {
            return res.send({
                status: false,
                message: "L'email est requis"
            })
        }
        //console.log("req.body",req.body)
         
        let hashedPass;
        if (password) {
            hashedPass= await bycrypt.hash(password, 10);
        }
        
        
        let datatosave = {
            first_name,
            last_name,
            email,
            password: hashedPass,
            phone:phone,
            country_code:country_code
        }
        let olduser = await User.findOne({ email: email,is_deleted:false });
        if(olduser)
        {
            return res.send({
                status:false,
                message:"Cet email est déjà dans nos archives"
            })
        }
            await User.create(datatosave).then(async(result) => {
                const page_name=process.env.register_confirm_email
            const apptext=await apptext_cmsmodel.findOne({page_name:page_name});
            // Create token
            const pathtofile=path.resolve("views/auth/confirm-register-email-user.ejs");
            //console.log(apptext,"result",result);
            let code=generatePassword(6);
            const user=await User.findById(result._id);
            user.verification_code=code;
            user.otp_date_time = new Date();
            user.otp_used_or_not = 0;
            let emailtext;
            let emailsubject;
            let emailtosent=email;
            if(apptext?.page_content){
            emailtext=apptext.page_content;
            emailsubject=apptext.page_heading;
            }else{
                emailtext="Inscription réussie, continuez et mettez à jour votre profil avant de vérifier votre CA auprès de cet organisme."
                emailsubject="Inscription réussie"
            }
            // var data = {
            //   to: emailtosent,
            //   from: process.env.MAILER_EMAIL_ID,
            //   template: "confirm-register-email-user",
            //   subject: emailsubject,
            //   context: {
            //     name: first_name+" "+last_name,
            //     text:emailtext,
            //     code:code
               
            //   },
            // };
            user.save().then((result)=>{
                

                let fullname = first_name+" "+last_name;
                var base_url_server = customConstant.base_url;
                let text_msg = "L'inscription est réussie, continuez et mettez à jour votre profil et ceci est votre otp "+code+" pour vérifier votre compte email.";
                var imagUrl = base_url_server+"public/uploads/logo.png";
                var html = "";
                html += "<div class='mail-content' style='position: relative;display: block;font-size: 14px;line-height: 25px;font-family: Poppins, sans-serif;'>";
                html += "<div class='content'></div>";
                html += "<p>	Bienvenue ,</p>";
                html += "<p>"+fullname+"</p>";
                html += "<p>"+text_msg+"</p>";
                html += "</div>";
                html += "<div class='mail-footer'>";
                html += "<img src='"+imagUrl+"' height='150' width='250'>";
                html += "</div>";
                html += "<i>Oblack</i>";


                let text_msg2 = "L'un des nouveaux vendeurs "+ fullname +" a été enregistré sur votre plateforme avec l'adresse e-mail "+ email +" , vérifiez ses coordonnées et mettez le compte à jour";
                var html2 = "";
                html2 += "<div class='mail-content' style='position: relative;display: block;font-size: 14px;line-height: 25px;font-family: Poppins, sans-serif;'>";
                html2 += "<div class='content'></div>";
                html2 += "<p>	Bienvenue ,</p>";
                html2 += "<p>L'administrateur</p>";
                html2 += "<p>"+text_msg2+"</p>";
                html2 += "</div>";
                html2 += "<div class='mail-footer'>";
                html2 += "<img src='"+imagUrl+"' height='150' width='250'>";
                html2 += "</div>";
                html2 += "<i>Oblack</i>";

                const message = {
                    from: process.env.MAILER_FROM_EMAIL_ID, // Sender address
                    to: email,         // recipients
                    subject: "Inscription réussie", // Subject line
                    html: html
                };
                transport.sendMail(message, function(err, info) {
                    if (err)
                    {
                        console.log("email sending error ->>> "+err);
                        var return_response = {
                            status:true,
                            message:"Succès de l'enregistrement de l'utilisateur",
                             data:result
                        };
                        res.status(200)
                        .send(return_response);
                    }else{
                        console.log('mail has sent.');
                        const message = {
                            from: process.env.MAILER_FROM_EMAIL_ID, // Sender address
                            to: 'contact@oblackmarket.ci',         // recipients
                            subject: "Nouveau vendeur enregistré ici sur O'Black", // Subject line
                            html: html2
                        };
                        transport.sendMail(message, function(err, info) {
                            if (err)
                            {
                                console.log("email sending error 2nd time ->>> "+err);
                                var return_response = {
                                    status:true,
                                    message:"Succès de l'enregistrement de l'utilisateur",
                                    data:result
                                };
                                res.status(200)
                                .send(return_response);
                        
                            }else{
                                console.log('mail has sent.');
                                //console.log(info);
                                var return_response = {
                                    status:true,
                                    message:"Succès de l'enregistrement de l'utilisateur",
                                    data:result
                                };
                                res.status(200)
                                .send(return_response);
                        
                            }
                        });
                    }
                });

            })
           
            }).catch((e) => {
                return res.send({
                    status: false,
                    message: e.message
                })
            })
        
    },
    resendOtp:async(req,res)=>{
        try{
            var date_time = new Date();
            let code = generatePassword(6);
            var email = req.body.email;
            const user = await User.findOne({email:email});
            if(!user)
            {
                return res.send({
                    status:false,
                    message:"Données utilisateur non valides.",
                    errmessage:"Données utilisateur non valides."
                  })
            }else{
                //console.log("user ",user);return false;
                if(user.is_active == false)
                {
                    return res.send({
                        status:false,
                        message:"Votre compte n'est pas activé contact administrateur.",
                        errmessage:"Votre compte n'est pas activé contact administrateur."
                      })
                }else{
                    await User.updateOne({_id:user._id},{
                        isverfied:false,
                        verification_code:code,
                        otp_used_or_not:0,
                        otp_date_time:date_time,
                    });
                    let lastInsertId = user._id;
                    var email_verify_url = customConstant.base_url_web_site+"otp/"+lastInsertId;

                    // html = "";
                    

                    let fullname = user.first_name+" "+user.last_name;
                    var base_url_server = customConstant.base_url;
                    let text_msg = "L'inscription est réussie, continuez et mettez à jour votre profil et ceci est votre otp "+code+" pour vérifier votre compte email.";
                    var imagUrl = base_url_server+"public/uploads/logo.png";
                    var html = "";
                    html += "<div class='mail-content' style='position: relative;display: block;font-size: 14px;line-height: 25px;font-family: Poppins, sans-serif;'>";
                    html += "<div class='content'></div>";
                    html += "<p>	Bienvenue ,</p>";
                    html += "<p>"+fullname+"</p>";
                     html+= "Vous devez vérifier votre adresse e-mail pour accéder à cette application, votre code de vérification est "+code+" ";
                     html+= '<a href="'+email_verify_url+'">'+' pour la vérification de email, cliquez ici '+'</strong>';
                    html += "</div>";
                    html += "<div class='mail-footer'>";
                    html += "<img src='"+imagUrl+"' height='150' width='250'>";
                    html += "</div>";
                    html += "<i>Oblack</i>";

                    var messageEmail = html;
                    const message = {
                    from: process.env.MAILER_FROM_EMAIL_ID, // Sender address
                    to: email,         // recipients
                    subject: "Vérifier otp", // Subject line
                    html: messageEmail // Plain text body
                    };
                    transport.sendMail(message, function(err, info) {
                        if (err) {
                            console.log("errr",err);
                            //error_have = 1;
                            return res.send({
                                status:true,
                                message:"Veuillez vérifier votre courriel et votre compte"
                            })
                        } else {
                            console.log("no err");
                            return res.send({
                                status:true,
                                message:"Veuillez vérifier votre courriel et votre compte"
                            })
                        }
                    });
                }
                
            }
        }catch(error)
        {
            return res.send({
                status:false,
                message:error.message,
                errmessage:error.message
              })
        }
    },
    getuserprofile:async(req,res)=>
    {
        //mongoose.set('debug', true);
        const id=req.params.id
        console.log("id",id)
        if(id){
            await User.findById(id).then((result)=>{
                return res.send({
                    status:true,
                    data:result
                })
            })
        }else{
        return res.send({
            status:false,
            data:"no id"
        })
    }
    },
    login: async (req, res) => {
        try {
            // Get user input
            const { email, password, fcm_token } = req.body;
            let last_login_time = new Date();
            // Validate user input
            if (!(email)) {
                res.status(400).send(
                    {
                        status: false,
                        message: "",
                        errmessage: "Toute contribution est requise",
                        data: null
                    }
                );
            }
            // Validate if user exist in our database
            //mongoose.set("debug",true);
            const user = await User.findOne({ email:email,is_deleted:false });
            //console.log("user ",user);return false;
            if(user)
            {
                if (password != "") {
                    if (user && (await bycrypt.compare(password, user.password)))
                    {
                        
                        if(user.is_active == true)
                        {
                            if(user.isverfied ==true)
                            {
                                // Create token
                                const token = jwt.sign(
                                    { user_id: user._id, email },
                                    process.env.TOKEN_KEY,
                                    {
                                        expiresIn: "24h",
                                    }
                                );
                                user.fcm_token = fcm_token;
                                // save user token
                                user.token = token;
                                user.last_login_time = last_login_time;
                                user.save((result) => {
                                    return res.status(200).json({
                                        status: true,
                                        errmessage: "",
                                        message: "Connexion réussie",
                                        data: user
                                    });
                                })
                                // user
                            }else{
                                return res.status(200).send({
                                    status: false,
                                    message: "Votre compte e-mail n'a pas encore été vérifié, veuillez vérifier d'abord",
                                    errmessage: "Votre compte e-mail n'a pas encore été vérifié, veuillez vérifier d'abord",
                                    data: null
                                });
                            }
                        }else{
                            return res.status(200).send({
                                status: false,
                                message: "Votre compte est inactivé, vous pouvez contacter l'administrateur",
                                errmessage: "Votre compte est inactivé, vous pouvez contacter l'administrateur",
                                data: null
                            });
                        }
    
                    } else {
                        return res.status(200).send({
                            status: false,
                            message: "",
                            errmessage: "L'adresse électronique ou le mot de passe est incorrect",
                            data: null
                        });
                    }
                } else {
                    if (user.extProvider) {
                        const token = jwt.sign(
                            { user_id: user._id, email },
                            process.env.TOKEN_KEY,
                            {
                                expiresIn: "24h",
                            }
                        );
                        // save user token
                        user.fcm_token = fcm_token;
                        user.token = token;
                        user.save((result) => {
                            return res.status(200).json({
                                status: true,
                                errmessage: "",
                                message: "Connexion réussie",
                                data: user
                            });
                        })
                    } else {
                        return res.send({
                            status: false,
                            message: "",
                            errmessage: "Vous ne vous êtes pas réveillé en utilisant cette méthode",
                            data: null,
                        });
                    }
                }
            }else{
                return res.send({
                    status: false,
                    message: "",
                    errmessage: "Identifiant et mot de passe invalides",
                    data: null,
                });
            }
            

        } catch (err) {
            console.log(err);
        }
        // Our register logic ends here
    },
       verifycode:async(req,res)=>{
        const id=req.body.id;
        const code=req.body.code;
        const user=await User.findById(id);
        if(user)
        {
          if(code.toString()==user.verification_code.toString())
          {
            if(user.otp_used_or_not == 0)
            {
                //console.log("user ",user);
                const date = new Date();
                var hours = Math.abs(date.getTime() - new Date(user.otp_date_time).getTime()) / 3600000;
                //console.log("hours "+hours);

               // return false;
                if(hours > 24)
                {
                    return res.send({
                        status:false,
                        message:"Le code est expiré",
                        errmessage:"Le code est expiré",
                        data:null
                      })
                }else{
                    user.verification_code="",
                    user.isverfied=true;
                    user.otp_used_or_not=1;
                    user.otp_date_time=null;
                    user.save().then((result)=>{
                        return res.send({
                        status:true,
                        message:"La vérification est réussie",
                        errmessage:"",
                        data:null
                        })
                    })
                }
                
            }else{
                return res.send({
                    status:false,
                    message:"Vous avez déjà utilisé ce code",
                    errmessage:"Vous avez déjà utilisé ce code",
                    data:null
                  })
            }
            
           }else{
             return res.send({
               status:false,
               message:"",
               errmessage:"Le code n'est pas valide",
               data:null
             })
           }
        }else{
          return res.send({
            status:false,
            message:"",
            errmessage:"Identité d'utilisateur invalide",
            data:null
          })
        }
      },
    forgotpassword: (req, res) => {
        try {
            async.waterfall(
                [
                    function (done) {
                        User.findOne({
                            email: req.body.email,
                        }).exec(function (err, user) {
                            if (user) {
                                done(err, user);
                            } else {
                                done({
                                    status: false,
                                    message: "utilisateur non trouvé",
                                    errmessage: "",
                                    data: null,
                                });
                            }
                        });
                    },
                    function (user, done, err) {
                        // create a unique token
                        var tokenObject = {
                            email: user.email,
                            id: user._id,
                        };
                        var secret = user._id + "_" + user.email + "_" + new Date().getTime();
                        var token = jwt.sign(tokenObject, secret);
                        done(err, user, token);
                    },
                    function (user, token, done) {
                        User.findByIdAndUpdate(
                            { _id: user._id },
                            {
                                reset_password_token: token,
                                reset_password_expires: Date.now() + 86400000,
                            },
                            { new: true }
                        ).exec(function (err, new_user) {
                            done(err, token, new_user);
                        });
                    },
                    function (token, user, done) {
                        var data = {
                            to: user.email,
                            from: process.env.MAILER_EMAIL_ID,
                            template: "forgot-password-email.ejs",
                            subject: "L'aide pour les mots de passe est arrivée !",

                            context: {
                                url: base_url + "/api/user/reset_password?token=" + token,
                                name: user.first_name,
                            },
                        };
                        // console.log(data)
                        sendemail(req, res, data, done);
                    },
                ],
                function (err) {
                    return res.status(422).json({
                        status: false,
                        message: "",
                        errmessage: err.message,
                        data: null,
                    });
                }
            );
        } catch (err) {
            console.log(err);
        }
    },
    
    userForgotPassword:async function(req,res,next)
    {
      try{
        var base_url = customConstant.base_url;
        var imagUrl = base_url+"public/uploads/logo.png";
       // var imagUrl = base_url_server+"public/uploads/logo.png";
        //console.log("process.env.MAILER_FROM_EMAIL_ID "+process.env.MAILER_FROM_EMAIL_ID);
        
        //console.log(req.body);
        var randomNumber = Array.from(Array(20), () => Math.floor(Math.random() * 36).toString(36)).join('');
        var base_url_web_site = customConstant.base_url_user_site;
        var resetPWUrl = base_url_web_site+"userResetPassword/"+randomNumber;
        var dateobj = new Date();
        var isoDate = dateobj.toISOString();
        //console.log("isoDate "+isoDate);
        var {email} = req.body;
        var result = await  User.findOne({ email ,is_deleted: false});
        if(!result)
        {
            return res.status(200)
              .send({
                  error: true,
                  status: false,
                  success: false,
                  errorMessage: "Cet e-mail n'est pas valide"
              });
        }
        //console.log("result ",result);return false;
        if(result.is_active == true)
            {
              var html = "";
              html += "<div class='mail-content' style='position: relative;display: block;font-size: 14px;line-height: 25px;font-family: Poppins, sans-serif;'>";
              html += "<div class='content'></div>";
              html += "<p>	Bienvenue "+result.first_name+' '+result.last_name+",</p>";
              //html += "<p>"+result.first_name+' '+result.last_name+"</p>";
              html += "<p>Il semble que vous ayez oublié votre mot de passe pour O'Black. Si c'est le cas, cliquez ci-dessous pour réinitialiser votre mot de passe.</p>";
              html += "<p><a href='"+resetPWUrl+"'>Réinitialiser mon mot de passe</a></p>";
              html += "<p>Si vous n'avez pas oublié votre mot de passe, vous pouvez ignorer cet e-mail.</p>";
              html += "</div>";
              html += "<div class='mail-footer'>";
              html += "<img src='"+imagUrl+"' height='150' width='250'>";
              html += "</div>";
              html += "<i>Oblack</i>";
              //console.log(randomNumber);
              User.updateOne({ "_id":result._id }, 
              {forgotPasswordLink:randomNumber,forgotPasswordTime:isoDate,forgotPasswordUsed:0}, function (uerr, docs) {
              if (uerr)
              {
                  //console.log(uerr);
                  return res.status(200)
                    .send({
                        error: true,
                        status: false,
                        success: false,
                        errorMessage: uerr,
                        //userRecord:user
                    });
              }else{
                  const message = {
                    from: process.env.MAILER_FROM_EMAIL_ID, // Sender address
                    to: email,         // recipients
                    subject: "Mot de passe oublié", // Subject line
                    html: html
                  };
                  transport.sendMail(message, function(err, info) {
                    if (err)
                    {
                      console.log(err);
                      var return_response = {"error":true,
                      status: false,success: false,errorMessage:"Le courrier électronique ne fonctionne pas, veuillez contacter le support technique"};
                      return  res.status(200)
                      .send(return_response);
  
                    }else{
                      //console.log('mail has sent.');
                      //console.log(info);
                      var return_response = {"error":false,
                      status: true,success: true,errorMessage:"Nous avons envoyé un lien sur votre adresse e-mail enregistrée, vous pouvez réinitialiser le mot de passe à partir de là."};
                      return  res.status(200)
                      .send(return_response);
                    }
                  });
                }
              });
            }else{
                return  res.status(200)
              .send({
                  error: true,
                  status: false,
                  success: false,
                  errorMessage: "Votre compte n'est pas actif, vous pouvez contacter l'administrateur."
              });
            }
      }catch(error)
      {
        console.log(error);
        res.status(200)
        .send({
            error: true,
            status: false,
            success: false,
            errorMessage: error
        });
      }
    },

    userSubmitForgotPassword:async(req,res)=>
    {
        try{
            var {id,new_password} = req.body;
            
            if(id == "" || id == null || new_password == "" || new_password == null)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant et le mot de passe sont tous deux nécessaires"
                });
            }else{
                var encryptedPassword = await bycrypt.hash(new_password, 10);
                const seller = await User.findOne({forgotPasswordLink:id},{forgotPasswordTime:1});
                if(seller)
                {
                   // console.log("seller "+seller);
                    var current_date = new Date();
                    var hours = Math.abs(current_date.getTime() - new Date(seller.forgotPasswordTime).getTime()) / 3600000;
                    //console.log("hours "+hours);
                    hours = parseInt(hours);
                    if(hours < 24)
                    {
                        await User.updateOne({_id:seller._id},{password:encryptedPassword}).then((result)=>{
                            return res.send({
                                status:true,
                                message:"Votre mot de passe a été mis à jour avec succès. Vous pouvez vous connecter avec ce mot de passe."
                            });
                        }).catch((error)=>{
                            return res.send({
                                status:false,
                                message:error
                            });
                        });
                    }else{
                        return res.send({
                            status:false,
                            message:"Le lien a expiré, vous pouvez demander à nouveau le mot de passe oublié."
                        });
                    }
                }else{
                    return res.send({
                        status:false,
                        message:"Donnée non valide"
                    });
                }
            }
        }catch(error)
        {
            return res.send({
                status:false,
                message:error
            });
        }
    },
    logout: async (req, res) => {
        const id = req.params.id;
        const Lawyer = await User.findById(id);
        Lawyer.token = "";
        Lawyer.fcm_token = "";
        Lawyer.save();
        res.status(200).json({
            status: true,
            errmessage: "",
            message: "Deconnexion réussie",
            data: null
        });
    },
    deleteuser:async(req,res)=>
    {
        try{
            const id=req.params.id;
            const user=await User.findById(id);
            user.email=user.email+"_"+"deleted";
            user.is_deleted=true;
            user.save().then((result)=>{
                return res.send({
                    status:true,
                    errorMessage:"L'utilisateur a supprimé le succès complet"
                })
            })
        }catch(e)
        {
            return res.send({
                status:false,
                errorMessage:e.message
            })
        }
    },
    deletemultipleuser:async(req,res)=>
    {
        try{
            const ids=req.body.ids;
            for(let i=0;i<ids.length;i++)
            {
                let id=ids[i];
                const user=await User.findById(id);
                user.email=user.email+"_deleted";
                user.is_deleted=true;
                user.save().then((result)=>{ })
            }
            return res.send({
                status:true,
                errorMessage:"L'utilisateur a supprimé le succès complet"
            })
        }catch(e)
        {
            return res.send({
                status:false,
                errorMessage:e.message
            })
        }
    },
    markUserAsActive:async(req,res)=>{
        try{
            const id=req.params.id;
            const user=await User.findById(id);
            user.is_active=true;
            user.save().then((result)=>{
                return res.send({
                    status:true,
                    errorMessage:"Activation du compte d'utilisateur entièrement réussie "
                })
            })
        }catch(e)
        {
            return res.send({
                status:false,
                errorMessage:e.message
            })
        }
    },
    markUserAsInActive:async(req,res)=>{
        try{
            const id=req.params.id;
            const user=await User.findById(id);
            user.is_active=false;
            user.save().then((result)=>{
                return res.send({
                    status:true,
                    errorMessage:"Compte d'utilisateur dans l'activation du succès complet "
                })
            })
        }catch(e)
        {
            return res.send({
                status:false,
                errorMessage:e.message
            })
        }
        
    },
    render_reset_password_template: (req, res) => {
        return res.render(path.resolve('./views/auth/reset-password.ejs'), {
            url: base_url
        });
    },
    reset_password: async (req, res) => {
        const page_name = process.env.reset_password_confirm_email
        const apptext = await apptext_cmsmodel.findOne({ page_name: page_name });
        // Create token
        console.log(apptext);
        let emailtext;
        let emailsubject;
        if (apptext?.page_content) {
            emailtext = apptext.page_content;
            emailsubject = apptext.page_heading;
        } else {
            emailtext = "Votre mot de passe a été réinitialisé avec succès, vous pouvez maintenant vous connecter avec votre nouveau mot de passe."
            emailsubject = "Confirmation de la réinitialisation du mot de passe"
        }
        //  console.log(req.body)
        User.findOne({
            reset_password_token: req.body.token,
            reset_password_expires: {
                $gt: Date.now(),
            },
        }).exec(async function (err, user) {
            // console.log(user,err)
            if (!user) {
                return res.send({
                    status: false,
                    message: "page expired",
                    errmessage: "La page a expiré, demandez à nouveau la réinitialisation du mot de passe"
                })
            }
            if (user && !err) {

                if (req.body.newPassword == req.body.verifyPassword) {
                    // console.log("password matched")
                    let encryptedPassword = await bycrypt.hash(req.body.newPassword, 10);
                    user.password = encryptedPassword;
                    user.reset_password_expires = undefined;
                    user.reset_password_token = undefined;
                    user.save(async function (err, user) {
                        if (!err) {
                            var data = {
                                to: user.email,
                                from: process.env.MAILER_EMAIL_ID,
                                template: 'reset-password-email.ejs',
                                subject: emailsubject,
                                context: {
                                    name: user.first_name,
                                    emailtext: emailtext
                                }
                            };
                            var smtpTransport = nodemailer.createTransport({
                                host: process.env.MAILER_HOST,
                                port: process.env.MAILER_PORT,
                                secure: false,
                                auth: {
                                    user: process.env.MAILER_EMAIL_ID,
                                    pass: process.env.MAILER_PASSWORD,
                                },
                                tls: {
                                    rejectUnauthorized: false
                                }
                            });


                            const htmldata = await ejs.renderFile(path.resolve("./views/auth/" + data.template), {
                                name: data.context.name,
                                url: data.context.url
                            });
                            
                            data.html = htmldata;
                            // console.log("data in email",data)
                            
                            smtpTransport.sendMail(data, function (err) {
                                if (!err) {
                                    return res.status(200).json({
                                        status: true,
                                        message: "Confirmer le succès du mot de passe",
                                        errmessage: "",
                                        data: null,
                                    });
                                } else {
                                    // console.log(err)
                                    return res.send(
                                        {
                                            status: false,
                                            message: "",
                                            errmessage: err.message,
                                            data: null,
                                        }
                                    );
                                }
                            });
                        } else {
                            return res.status(422).send({
                                status: false,
                                message: "",
                                errmessage: err.message,
                                data: null,
                            });
                        }
                    })

                }
            }
        });
    },
    getbillingaddress:async(req,res)=>{
        const userid=req.body.id;
        const addressid=req.body.addressid;
        const user=await User.findById(userid);
        
        const billingaddress=user.address.filter((add)=>{
            console.log("add",add)
            if(add.is_billing_address==true){
                return add
            }
        })
        return res.send({
            status:true,
            data:billingaddress
        })
          
    },
    getshippingaddress:async(req,res)=>{
        const userid=req.body.id;
        const addressid=req.body.addressid;
        const user=await User.findById(userid);
        //console.log("user "+user);return false;
        var billingaddress = [];
        if(user)
        {
            if(user.address)
            {
                billingaddress=user.address.filter((add)=>{
                    //console.log("add 702",add)
                    if(add.is_shipping_address==true)
                    {
                        return add
                    }
                })
            }
        }
        return res.send({
            status:true,
            data:billingaddress
        })
    },
    markaddressasshippingaddress:async(req,res)=>{
        const email=req.body.email;
        const addressid=req.body.addressid;
        const query = { email:email };
        const newid=mongoose.Types.ObjectId(addressid)
        const updateDocument1 = {
            "address.$[item].is_shipping_address": false 
        };
        const options1 = {
          arrayFilters: [
            {
                "item._id":{$ne:newid}
             
            },
          ],
        };
        const updateDocument2 = {
            "address.$[item].is_shipping_address": true 
        };
        const options2 = {
          arrayFilters: [
            {
                "item._id":{$eq:newid}
             
            },
          ],
        };
        const result1 = await User.updateOne(query, updateDocument1, options1);
        const result2 = await User.updateOne(query, updateDocument2, options2);
        console.log("result1",result1,"result2",result2)
        return res.send({
            status:true,
            message:"success"
        })
    
    },
    markaddressasbillingaddress:async(req,res)=>{
        const email=req.body.email;
        const addressid=req.body.addressid;
        console.log("req.body",req.body)
        const query = { email:email };
        const newid=mongoose.Types.ObjectId(addressid)
        const updateDocument1 = {
            "address.$[item].is_billing_address": false 
        };
        const options1 = {
          arrayFilters: [
            {
                "item._id":{$ne:newid}
             
            },
          ],
        };
        const updateDocument2 = {
            "address.$[item].is_billing_address": true 
        };
        const options2 = {
          arrayFilters: [
            {
                "item._id":{$eq:newid}
             
            },
          ],
        };
        const result1 = await User.updateOne(query, updateDocument1, options1);
        const result2 = await User.updateOne(query, updateDocument2, options2);
        console.log("result1",result1,"result2",result2)
        return res.send({
            status:true,
            message:"success"
        })
    
    },
    updateaddress:async(req,res)=>{
        const email=req.body.email;
        const addressid=req.body.addressid;
        const query = { email:email };
        const type=req.body.type;
        const country=req.body.country;
        const name=req.body.name;
        const telephone=req.body.telephone;
        const address=req.body.address;
        const city=req.body.city;
        const delivery_instructions=req.body.delivery_instructions;
        const is_shipping_address=req.body.is_shipping_address;
        const is_billing_address=req.body.is_billing_address;
        const newid=mongoose.Types.ObjectId(addressid)
        const updateDocument1 = {
            ...(type&&{"address.$[item].type":type}),
            ...(country&&{"address.$[item].country":country}),
            ...(name&&{"address.$[item].name":name}),
            ...(telephone&&{"address.$[item].telephone":telephone}),
            ...(address&&{"address.$[item].address":address}),
            ...(city&&{"address.$[item].city":city}),
            ...(delivery_instructions&&{"address.$[item].delivery_instructions":delivery_instructions}),
            ...(is_shipping_address&&{"address.$[item].is_shipping_address":is_shipping_address}),
            ...(is_billing_address&&{"address.$[item].is_billing_address":is_billing_address})
        };
        const options1 = {
          arrayFilters: [
            {
                "item._id":newid
             
            },
          ],
        };
       
        const result1 = await User.updateOne(query, updateDocument1, options1);
        console.log("result1",result1)
        return res.send({
            status:true,
            message:"success"
        })
    },
    deleteaddress:async(req,res)=>{
        const email=req.body.email;
        const addressid=req.body.addressid;
        // const newid=mongoose.Types.ObjectId(addressid)
        
        const addresses=(await User.findOne({email:email})).address;
        let addresstodelete=addresses?.filter((e)=>e._id+""==addressid);
        let updateaddressarray=addresses?.filter((e)=>e._id+""!=addressid);
       if(updateaddressarray?.length>0){
        await User.updateOne({email:email}, {address:updateaddressarray});
       
       
       }
       return res.send({
        status:true,
        message:"success"
    })
    },
    getalluserforfilter:async(req,res)=>{
        const allproducts=await User.aggregate([
            {$match:{is_deleted:false}},
            {$project:{
                _id:1,
                first_name:1,
                last_name:1
            }}
        ]);
        return res.send({
            status:true,
            data:allproducts
        })
    },
    push_testing:async(req,res)=>{
        const id=req.params.id
        console.log("id",id)
        if(id){
            await User.findById(id).then((result)=>{
                var deviceToken = result.fcm_token;
                console.log("deviceToken --->>>>> "+deviceToken);
                if(deviceToken && deviceToken != null)
                {
                    var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
                        to: deviceToken, 
                        collapse_key: 'your_collapse_key',
                        
                        notification: {
                            title: 'Oblack', 
                            body: 'hello ayush testing push message',
                        },
                        
                        data: {  //you can send only notification or only data(or include both)
                            my_key: 'dsfdf',
                            my_another_key: 'hello ayush testing push message'
                        }
                    };
                    fcm.send(message, function(err, response){
                        if(err){
                        console.log("Something has gone wrong!");
                        console.log(err);
                        }else{
                        console.log("Successfully sent with response: ", response);
                        }
                    });
                }
            })
        }else{
            return res.send({
                status:false,
                data:"no id"
            })
        }
    },
    checkEmailExist:async(req,res)=>{
        try{
            let email = req.body.email;
            if(!email)
            {
                return res.send({
                    status: false,
                    message: "L'adresse électronique est requise"
                })
            }
            await User.findOne({email:email,is_deleted: false}).then((result)=>{
                if(!result)
                {
                    return res.send({
                        status: false,
                        message: "ID courriel invalide"
                    })
                }else{
                    return res.send({
                        status: false,
                        message: "Succès",
                        data:result
                    })
                }
            }).catch((error)=>{
                return res.send({
                    status: false,
                    message: error.message
                })
            })
        }catch(error)
        {
            return res.send({
                status: false,
                message: error.message
            })
        }
    },
    updateAddressDetail:async(req,res)=>{
        try{
            //console.log(req.body);return false;
            let {type,country,name,countryCode,telephone,address,pobox,city,email,address_id,addresss} = req.body;
 
            let newaddress;
            let new_address;
            try {
                newaddress = JSON.parse(address);
                new_address = JSON.parse(addresss);
            } catch (e) {
                newaddress = address;
                new_address =  addresss;
            }


            // let latlong = bank_address.latlong;
            // if(!latlong)
            // {
            //     return res.send({
            //         status: false,
            //         message: "L'adresse de ramassage devrait être l'adresse de Google"
            //     })
            // }
            if(!email)
            {
                return res.send({
                    status: false,
                    errorMessage: "Les données de l'utilisateur ne sont pas valides"
                })
            }
            // if(!bank_id)
            // {
            //     return res.send({
            //         status: false,
            //         message: "Une pièce d'identité bancaire est requis"
            //     })
            // }
            //mongoose.set("debug",true);
            let seller_record = await User.findOne({email:email});
            //console.log("seller_record ", seller_record);
            if(!seller_record)
            {
                return res.send({
                    status: false,
                    errorMessage: "Les données de l'utilisateur ne sont pas valides"
                })
            }else{

                const newdelivery_instructions={
                    ...(newaddress.delivery_instructions.accesscode&&{accesscode:newaddress.delivery_instructions.accesscode}),
                    ...(newaddress.delivery_instructions.interphone&&{interphone:newaddress.delivery_instructions.interphone}),
                    ...(newaddress.delivery_instructions.requiredcode&&{requiredcode:newaddress.delivery_instructions.requiredcode}),
                    ...(newaddress.delivery_instructions.instructions&&{instructions:newaddress.delivery_instructions.instructions}),
                }


                if(!address_id)
                {
                    let old_bank = seller_record.address;
                    //console.log("heree 779");
                    //console.log(old_bank);
                    //let address = bank_address.address;
                    const newbank={  
                        ...(newaddress.type&&{type:newaddress.type}),
                        ...(newaddress.country&&{country:newaddress.country}),
                        ...(newaddress.name&&{name:newaddress.name}),
                        ...(newaddress.countryCode&&{countryCode:newaddress.countryCode}),
                        
                        ...(newaddress.telephone&&{telephone:newaddress.telephone}),
                        ...(new_address&&{address:new_address}),
                        ...(newaddress.pobox&&{pobox:newaddress.pobox}),
                        ...(newaddress.city&&{city:newaddress.city}),
                        ...(Object.keys(newdelivery_instructions).length
                            &&{delivery_instructions
                                :newdelivery_instructions
                            })
                    };
                    // console.log("newbank " , newbank); 
                    // return false;
                    old_bank.push(newbank);

                    //console.log("new bank ", old_bank);

                    await User.updateOne({email:email},{
                        address:old_bank
                    }).then((result)=>{
                        return res.send({
                            status:true,
                            errorMessage:"Votre adresse a ajouté un succès complet "
                        })
                    })

                }else{
                    let bankRec = seller_record.address.map(val=>{
                        if(val._id == address_id)
                        { 
                            val.type = newaddress.type;
                            val.country = newaddress.country;
                            val.name = newaddress.name;
                            val.countryCode = newaddress.countryCode;
                            val.address = new_address;
                            val.telephone = newaddress.telephone;
                            val.pobox = newaddress.pobox;
                            val.city = newaddress.city;
                            val.delivery_instructions = newdelivery_instructions;
                        }
                        return val;
                    })
                    //console.log("bankRec ", bankRec);return false;
                    await User.updateOne({email:email},{address:bankRec}).then((value)=>{
                        return res.send({
                            status:true,
                            errorMessage:"Votre adresse a été mise à jour avec succès",
                        });
                    }).catch((e)=>{
                        //console.log("here 819");
                        return res.send({
                            status:false,
                            errorMessage:e.message,
                        });
                    });
                }
                
            }
        }catch(e){
            return res.send({
                status:false,
                errorMessage:e.message,
            });
        }
    },
    getUserSingleAddress:async(req,res)=>{
        try{
            //console.log(req.body);
            let final_data = [];
            let {id,user_id} = req.body;
            let user_rec = await User.findOne({_id:user_id},{address:1});
            if(user_rec)
            {
                final_data = user_rec.address.filter((val)=>{
                    if(val._id == id)
                    {
                        return val;
                    }
                })
                if(final_data.length>0)
                {
                    return res.send({
                        status:true,
                        errorMessage:"Succès",
                        data:final_data[0]
                    });
                }else{
                    return res.send({
                        status:false,
                        errorMessage:"Identifiant invalide",
                    });
                }
            }else{
                return res.send({
                    status:false,
                    errorMessage:"Identifiant invalide",
                });
            }
        }catch(e){
            return res.send({
                status:false,
                errorMessage:e.message,
            });
        }
    }
}