const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const async = require("async");
const base_url = process.env.BASE_URL;
const path = require("path");
const fs = require('fs');
const ClientsModel = require("../../models/admin/ClientsModel");
const customConstant = require('../../helpers/customConstant');
const mongoose = require("mongoose");
//var bcrypt = require('bcryptjs');
//const jwt = require("jsonwebtoken");
//const nodemailer = require('nodemailer');
const transport = nodemailer.createTransport({
    name: "Oblack",
    host: process.env.MAILER_HOST,
    port: process.env.MAILER_PORT,
    auth: {
      user: process.env.MAILER_EMAIL_ID,
      pass: process.env.MAILER_PASSWORD,
    }
});



module.exports = {
  addClientSubmit:async function(req,res,next)
  {
    try{
      console.log(req.body);
      var {full_name,email,contact_person_name,mobile_number,comment,is_checked,contact_person,streat_po_box,office_code,building_number,lift,floor,intercom,locality,sender_contact_number,address,latitude,longitude,user_id} = req.body;
      if(latitude == "" || longitude == null || latitude == "" || longitude == null)
      {
        var return_response = {"error":true,success: false,errorMessage:"l'adresse finale doit être une adresse google"};
        res.status(200)
        .send(return_response);
      }else{
        ClientsModel.create({
          user_id:user_id,
          full_name:full_name,
          email: email.toLowerCase(), // sanitize: convert email to lowercase
          contact_person_name:contact_person_name,
          mobile_number:mobile_number,
          comment:comment,
          is_checked: is_checked,
          contact_person: contact_person,
          streat_po_box: streat_po_box,
          office_code: office_code,
          building_number: building_number,
          lift: lift,
          floor:floor,
          intercom:intercom,
          locality:locality,
          sender_contact_number:sender_contact_number,
          address:address,
          location: {
            type: "Point",
            coordinates: [latitude,longitude]
          },
        },function(err,result){
          if(err)
          {
            //console.log(err);
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Informations sur le client ajoutées avec succès"};
              res.status(200)
              .send(return_response);
          }
        });
      }
    }catch(error)
    {
      console.log(error);
    }
  },

  allClientMerchant:async function(req,res,next)
	{
		
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
	    	//mongoose.set('debug', true);
        var { user_id } = req.body;
        var String_qr = {}; 
        String_qr['user_id'] = user_id;
        //console.log(String_qr);	    	
	      ClientsModel.find( String_qr, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
	      {
	        if(err)
	        {
	          console.log(err);
	        }else{
	          //console.log("result ");
	          //console.log(typeof result);
	          //console.log("result"+result);
	          if(result.length > 0)
	          {
	             var return_response = {"error":false,errorMessage:"Succès","record":result};
	                  res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	      });

	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
  allClientMerchantCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);

	    try{
        var { user_id } = req.body;
        var String_qr = {}; 
        String_qr['user_id'] = user_id;
        ClientsModel.countDocuments(String_qr).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
  getClientDetail:async function(req,res,next)
	{
    try{
      //mongoose.set('debug', true);
      var { order_id } = req.body;
      var String_qr = {}; 
      String_qr['_id'] = order_id;
      //console.log(String_qr);	    	
      ClientsModel.find( String_qr, {}).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
            res.status(200).send(return_response);
          }else{
            var return_response = {"error":true,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }
        }
      });
    }catch(err){
      console.log(err);
      var return_response = {"error":true,errorMessage:err };
      res.status(200).send(return_response);
    }
	},
  getSingleClientDetail:async function(req,res,next)
	{
    try{
      //mongoose.set('debug', true);
      var { order_id } = req.body;
      var String_qr = {}; 
      String_qr['_id'] = order_id;
      //console.log(String_qr);	    	
      ClientsModel.findOne( String_qr, {}).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
            res.status(200).send(return_response);
          }else{
            var return_response = {"error":true,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }
        }
      });
    }catch(err){
      console.log(err);
      var return_response = {"error":true,errorMessage:err };
      res.status(200).send(return_response);
    }
	},

  allClientOnShipment:async function(req,res,next)
	{
	    try{
	    	//mongoose.set('debug', true);
        var { user_id } = req.body;
        var String_qr = {}; 
        String_qr['user_id'] = user_id;
        //console.log(String_qr);	    	
	      ClientsModel.find( String_qr, {full_name:1,mobile_number:1}).exec((err, result)=>
	      {
	        if(err)
	        {
	          console.log(err);
	        }else{
	          //console.log("result ");
	          //console.log(typeof result);
	          //console.log("result"+result);
	          if(result.length > 0)
	          {
	             var return_response = {"error":false,errorMessage:"Succès","record":result};
	                  res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	      });

	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
  allClientOnShipmentSingle:async function(req,res,next)
	{
	    try{
	    	//mongoose.set('debug', true);
        var { client_id } = req.body;
        var String_qr = {}; 
        String_qr['_id'] = client_id;
        //console.log(String_qr);	    	
	      ClientsModel.findOne( String_qr, {}).exec((err, result)=>
	      {
	        if(err)
	        {
	          console.log(err);
	        }else{
	          //console.log("result ");
	          //console.log(typeof result);
	          //console.log("result"+result);
	          if(result)
	          {
	             var return_response = {"error":false,errorMessage:"Succès","record":result};
	                  res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	      });

	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
  editClientSubmit:async function(req,res,next)
  {
    try{
      var {full_name,email,contact_person_name,mobile_number,comment,contact_person,streat_po_box,office_code,building_number,lift,floor,intercom,locality,sender_contact_number,address,latitude,longitude,id} = req.body;
      if(latitude == "" || longitude == null || latitude == "" || longitude == null)
      {
        var return_response = {"error":true,success: false,errorMessage:"l'adresse finale doit être une adresse google"};
        res.status(200)
        .send(return_response);
      }else{
        ClientsModel.updateOne({_id:id},{
          full_name:full_name,
          email: email.toLowerCase(), // sanitize: convert email to lowercase
          contact_person_name:contact_person_name,
          mobile_number:mobile_number,
          comment:comment,
          contact_person: contact_person,
          streat_po_box: streat_po_box,
          office_code: office_code,
          building_number: building_number,
          lift: lift,
          floor:floor,
          intercom:intercom,
          locality:locality,
          sender_contact_number:sender_contact_number,
          address:address,
          location: {
            type: "Point",
            coordinates: [latitude,longitude]
          },
        },function(err,result){
          if(err)
          {
            //console.log(err);
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Informations sur le client ajoutées avec succès"};
              res.status(200)
              .send(return_response);
          }
        });
      }
    }catch(err)
    {
      console.log(err);
      var return_response = {"error":true,errorMessage:err };
      res.status(200).send(return_response);
    }
  }

};