const inventory = require("../../models/inventory/Inventory");
module.exports = {
    createInventory: async (req, res) => {
        const {
            product_id,
            quantity,
            provider_id
        } = req.body;
        const product=await inventory.findOne({product_id:product_id});
        if(product){
            product.quantity=product.quantity+quantity;
            product.save().then((result)=>{
                return res.send({
                    status:true,
                    message:"updated successfully"
                })
            })
        }else{
          const newInventory=await inventory();
          newInventory.product_id=product_id;
          newInventory.quantity=quantity;
          newInventory.provider_id=provider_id;
          newInventory.save().then((result)=>{
            return res.send({
                status:true,
                message:"inventory created successfully"
            })
          })
        }
        
    },
    deleteInventory: async (req, res) => {
        const id = req.body.id;
        await inventory.findByIdAndDelete(id)
            .then((result) => {
                return res.send({
                    status: true,
                    message: "deleted successfully"
                })
            });
    },
    subtractproductfrominventory:async(req,res)=>{},
    removeproductfrominventory:async(req,res)=>{},
    updateproductininventory:async(req,res)=>{},
    markproductasinactiveininventory:async(req,res)=>{},
    markproductasactiveininventory:async(req,res)=>{},
    checkifproductisininventory:async(req,res)=>{},
}