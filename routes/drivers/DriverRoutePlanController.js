const Seller = require("../../models/seller/Seller");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const async = require("async");
const base_url = process.env.BASE_URL;
const path = require("path");
const fs = require('fs');
const DriversModel = require("../../models/admin/DriversModel");
const customConstant = require('../../helpers/customConstant');
const RoutePlansModel = require('../../models/admin/RoutePlansModel');
const StopsModel = require('../../models/admin/StopsModel');
const ShipmentModel = require("../../models/admin/ShipmentModel");
const OrderModel = require("../../models/orders/order");
const {updateorderstatus} = require("../../modules/OrderDeliveryModule");
const seller=require("../../models/seller/Seller");
const AdminModel = require("../../models/admin/AdminModel");
const Notifications = require("../../models/Notifications");
const mongoose = require("mongoose");
//var bcrypt = require('bcryptjs');
//const jwt = require("jsonwebtoken");

module.exports = {


  getDriverCurrentRoutePlan:async function(req,res,next)
  {
    //console.log("get profile");
    try{
      //var base_url = customConstant.base_url;
      //var imagUrl = base_url+"public/uploads/driver/";
      //console.log(req.body);return false;
      var {driver_id,startDate,endDate,status} = req.body;
      if(driver_id == "" || driver_id == null)
      {
        res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: "Une carte d'identité de chauffeur est requise"
        });
      }else{

        if(endDate)
        {
          var dd = endDate+" 23:59:59";
          //console.log(dd);return false;
          var newendDatedate = new Date(dd);
          console.log("newendDatedate"+newendDatedate.toISOString("yyyy-MM-dd'T'29:59:59.SSS'Z'"));
        }
        var String_qr = {};
        if(endDate != "" && endDate != null && startDate != "" && startDate != null )
        {
          String_qr['created_at'] = { $lte: newendDatedate,$gte: startDate };
        }
        if(status == 1)
        {
          String_qr['missedStop'] = 0;
        }else if(status == 2)
        {
          String_qr['completedStop'] = 0;
        }
        
        String_qr['driver_id'] = driver_id;
        String_qr['status'] = {$lt:3};
        //{driver_id:driver_id,status:{$lt:3}}
        //mongoose.set("debug",true);
        RoutePlansModel.find(String_qr).exec((err,result)=>{
          if(err)
          {
            res.status(200)
            .send({
              error: true,
              success: false,
              errorMessage: err
            });
          }else{
            //console.log(result);
            res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Succès",
                record:result
            });
          }
        });
      }
      
    }catch(error){
      console.log(error);
      res.status(200)
      .send({
        error: true,
        success: false,
        errorMessage: error
      });
    }
  }, 

  getDriverPastRoutePlan:async function(req,res,next)
  {
    //console.log("get profile");
    try{
      //mongoose.set('debug', true);
      //var base_url = customConstant.base_url;
      //var imagUrl = base_url+"public/uploads/driver/";
      //console.log(req.body);return false;
      var {driver_id,dayValue,status,startDate,endDate} = req.body;
      if(driver_id == "" || driver_id == null)
      {
        res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: "Une carte d'identité de chauffeur est requise"
        });
      }else{
        console.log("startDate "+startDate);
        var String_qr = {};
        if(endDate)
        {
          var dd = endDate+" 23:59:59";
          //console.log(dd);return false;
          var newendDatedate = new Date(dd);
          console.log("newendDatedate"+newendDatedate.toISOString("yyyy-MM-dd'T'29:59:59.SSS'Z'"));
        }
        
        if(endDate != "" && endDate != null && startDate != "" && startDate != null )
        {
          String_qr['created_at'] = { $lte: newendDatedate,$gte: startDate };
        }
        String_qr['driver_id'] = driver_id;
        if(status == 1)
        {
          String_qr['missedStop'] = 0;
        }else if(status == 2)
        {
          String_qr['completedStop'] = 0;
        }
        String_qr['status'] = 3;
        //console.log("String_qr "+ JSON.stringify(String_qr));
        RoutePlansModel.find(String_qr).sort({created_at:-1}).exec((err,result)=>{
          if(err)
          {
            res.status(200)
            .send({
              error: true,
              success: false,
              errorMessage: err
            });
          }else{
            //console.log(result);
            res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Succès",
                record:result
            });
          }
        });
      }
      
    }catch(error){
      console.log(error);
      res.status(200)
      .send({
        error: true,
        success: false,
        errorMessage: error
      });
    }
  }, 
  getDriverAllRouteStop:async function(req,res,next)
  {
    try{
      //console.log("heree "+req.body);
      var {route_id} = req.body;
      if(route_id == "" || route_id == null)
      {
        res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: "L'identifiant de la route est requis"
        });
      }else{
        StopsModel.find({route_id:route_id,routeType: { $nin: [ 'start','end']}}).sort({"position":1}).exec((err,result)=>{
          if(err)
          {
            res.status(200)
            .send({
              error: true,
              success: false,
              errorMessage: err
            });
          }else{
            //console.log(result);
            res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Succès",
                record:result
            });
          }
        });
      }
    }catch(error){
      console.log(error);
      res.status(200)
      .send({
        error: true,
        success: false,
        errorMessage: error
      });
    }
  }, 
  getDriverSingleRouteStop:async function(req,res,next)
  {
    try{
      //console.log("heree "+req.body);
      var image_url = customConstant.base_url+"public/uploads/routePlan/";
      var {id} = req.body;
      if(id == "" || id == null)
      {
        res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: "L'identité est requise"
        });
      }else{
        StopsModel.findOne({_id:id}).exec((err,result)=>{
          if(err)
          {
            res.status(200)
            .send({
              error: true,
              success: false,
              image_url:image_url,
              errorMessage: err
            });
          }else{
            //console.log(result);
            res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Succès",
                image_url:image_url,
                record:result
            });
          }
        });
      }
    }catch(error){
      console.log(error);
      res.status(200)
      .send({
        error: true,
        success: false,
        errorMessage: error
      });
    }
  }, 
  getDriverSingleRouteWithStop:async function(req,res,next)
  {
    try{
      //var routeRecord = {};
      //console.log("heree "+req.body);
      var {route_id} = req.body;
      if(route_id == "" || route_id == null)
      {
        return res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: "L'identifiant de la route est requis"
        });
      }else{
        let routeRecord = await RoutePlansModel.findOne({_id:route_id});
        let resultStop = await StopsModel.find({route_id:route_id }).sort({position:1}).lean(true);
        let new_arr = [];

        const arr = [{id: 1}, {id: 2}];

        resultStop.forEach(object => {
          let tt = parseFloat(object.distanceTime);
          let oldDateObj = new Date();
          let newDateObj = new Date();
          newDateObj.setTime(oldDateObj.getTime() + ( tt * 60 * 1000));
          // console.log("newDateObj" , newDateObj);
          // let time_for_delivery = {time_for_delivery:newDateObj};
          object.time_for_delivery = newDateObj;
        });
        
        // 👇️ [{id: 1, color: 'red'}, {id: 2, color: 'red'}]
        //console.log(arr);

        // for(let x=0; x<resultStop.length; x++)
        // {
        //   console.log("resultStop",resultStop[x].distanceTime);
          
        //   //resultStop.push(time_for_delivery);
        // }
        console.log("new_arr ",new_arr);
        // let new_arr = resultStop.map((val)=>{
        //   //console.log("val",val);
        //   console.log("val",val.distanceTime);
        //   let tt = parseFloat(val.distanceTime);
        //   let oldDateObj = new Date();
        //   let newDateObj = new Date();
        //   newDateObj.setTime(oldDateObj.getTime() + ( tt * 60 * 1000));
        //   console.log("newDateObj" , newDateObj);
        //   let time_for_delivery = {time_for_delivery:newDateObj};
        //   val.push(time_for_delivery);
        //   return val;
        // });
        //console.log("new_arr ",new_arr);
        return res.status(200)
          .send({
            error: false,
            success: true,
            errorMessage: "Succès",
            record:routeRecord,
            stopRecord:resultStop
        });
        // RoutePlansModel.findOne({_id:route_id}).exec((err,result)=>{
        //   if(err)
        //   {
        //     res.status(200)
        //     .send({
        //       error: true,
        //       success: false,
        //       errorMessage: err
        //     });
        //   }else{
        //     routeRecord = result;
        //     //console.log(result);,routeType: { $nin: [ 'start','end']}
        //     StopsModel.find({route_id:route_id }).sort({position:1}).exec((errStop,resultStop)=>{
        //       if(errStop)
        //       {
        //         res.status(200)
        //         .send({
        //           error: true,
        //           success: false,
        //           errorMessage: errStop
        //         });
        //       }else{
        //         //console.log(result);
        //         res.status(200)
        //         .send({
        //             error: false,
        //             success: true,
        //             errorMessage: "Succès",
        //             record:routeRecord,
        //             stopRecord:resultStop
        //         });
        //       }
        //     });
        //   }
        // });
        
      }
    }catch(error){
      console.log(error);
      res.status(200)
      .send({
        error: true,
        success: false,
        errorMessage: error
      });
    }
  }, 
  deliveredStop:async function(req,res,next)
  {
    try{
      // console.log(req.body);
      // return false;
      //console.log(req.files);
      var {deliveryText,deliveryNoteClient,deliveryNoteCompany,stop_id,completeStop} = req.body;
      var deliverySignature = "";var deliveryPhoto = "";
      if(req.files.deliverySignature)
      {
        deliverySignature = req.files.deliverySignature[0].filename;
      }
      if(req.files.deliveryPhoto)
      {
        deliveryPhoto = req.files.deliveryPhoto[0].filename;
      }
      if(stop_id == "" || stop_id == null)
      {
        res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: "L'identité est requise"
        });
      }else{
        // StopsModel.findOne({ "_id":stop_id }, 
        //   {}, function (err, resultStop) {
        //   if (err){
        //     //console.log(err);
        //     res.status(200)
        //       .send({
        //           error: true,
        //           success: false,
        //           errorMessage: err
        //       });
        //   }else{}
        // });
        var resultStop = await StopsModel.findById(stop_id);
        //console.log("resultStop 332 --------->>>>>>>>> "+resultStop);return false;

        //console.log("resultStop 332 --------->>>>>>>>> "+resultStop.orderType);
            
            //return false;

        if(resultStop)
        {
          if(resultStop.orderType == 'user_order')
          {
            if(resultStop.stopType == 'Delivery')
            {

              var admin_record = await AdminModel.findOne({});
              //console.log("resultStop 337 first --->>>> "+resultStop);
              let user_order_id_arr = resultStop.user_order_id_arr;
              //console.log("user_order_id_arr 339 first --->>>> "+JSON.stringify(user_order_id_arr));
              if(user_order_id_arr.length > 0)
              {
                var less_val_order = user_order_id_arr.length - 1;
                var cc = 0;
                for(let mm=0; mm<user_order_id_arr.length; mm++)
                {
                  //console.log("user_order_id_arr 340 first --->>>> "+user_order_id_arr[mm].item_id);
                  var explode_user_order_id = user_order_id_arr[mm].item_id.split(",");
                  if(explode_user_order_id.length > 0)
                  {
                    let o_ID = explode_user_order_id[0];
                    let part_id_string = explode_user_order_id[1];
                    var explode_part_id = part_id_string.split("_");
                    if(explode_part_id.length>1)
                    {
                      let part_id = explode_part_id[1];

                      // console.log("o_ID ",o_ID);
                      // console.log("part_id ",part_id);
                      
                      let order_cnancel_check = await OrderModel.findOne({_id:o_ID,products:{$elemMatch:{part_id:part_id}} } );
                      let chekc_condition = order_cnancel_check.products.filter((val)=>{
                        //console.log("val ",val);
                        if(val.part_id == part_id && val.status == 2)
                        {
                          return val;
                        }
                      });
                      //console.log("chekc_condition ",chekc_condition);
                      //console.log("order_cnancel_check  ", order_cnancel_check);
                      //return false;
                      if(chekc_condition.length > 0)
                      {
                        res.status(200)
                        .send({
                          error: true,
                          success: false,
                          errorMessage: "Annulation de la commande par l'utilisateur"
                        });
                      }
                      var response_order_api = await updateorderstatus(OrderModel,o_ID,part_id, "" ,"","Delivery",3);
                      //console.log("hereeeeeeeeeeeeee 473 ");return false;
                      if(response_order_api == true)
                      {
                        cc++;
                      }
                    }
                  }
                }

                let part_iddd_d = "";
                let provider_id = "";
                let userId = "";
                let seller_totoal_profit = 0;
                let chekc_condition2 = [];
                let o_ID = "";
                for(let mm=0; mm<user_order_id_arr.length; mm++)
                {
                  //console.log("user_order_id_arr 340 first --->>>> "+user_order_id_arr[mm].item_id);
                  var explode_user_order_id = user_order_id_arr[mm].item_id.split(",");
                  if(explode_user_order_id.length > 0)
                  {
                    o_ID = explode_user_order_id[0];
                    let part_id_string = explode_user_order_id[1];
                    var explode_part_id = part_id_string.split("_");
                    if(explode_part_id.length>1)
                    {
                      let part_id = explode_part_id[1];

                      // console.log("o_ID ",o_ID);
                      // console.log("part_id ",part_id);
                      //return false;

                       let order_cnancel_check2 = await OrderModel.findOne({_id:o_ID  } );
                       userId = order_cnancel_check2.userId;
                       //provider_id
                       //console.log("order_cnancel_check2 ",order_cnancel_check2);
                       //userId
                       //products // provider_id
                       //return false;
                      chekc_condition2 = order_cnancel_check2.products.map((val)=>{
                        provider_id = val.provider_id;
                        
                        //console.log("val ",val);
                        //  return val;
                        if(val.part_id == part_id && val.status != 2 && val.delivery_status == 3 && val.is_paid == false)
                        {
                          part_iddd_d = val.part_id;
                          seller_totoal_profit = seller_totoal_profit + val.sellerprofit;
                          val.is_paid = true;
                          return val;
                        }else{
                          return val;
                        }
                      });
                      //console.log("chekc_condition ",chekc_condition);
                    }
                  }
                }
                // console.log("chekc_condition2 ",chekc_condition2);
                // console.log("seller_totoal_profit ",seller_totoal_profit);
                console.log("provider_id ",provider_id);
                console.log("userId ",userId);
                console.log("o_ID ",o_ID);

                 
                 

                let message = "La commande a été livrée avec succès dans son intégralité lorsque l'identifiant de la commande est - "+part_iddd_d;
                let data_save = {
                    to_id:provider_id,
                    user_type:"seller",
                    title:"Commande livrée",
                    message:message,
                    status: 'unread',
                    notification_type:"order_delivered"
                }
                await Notifications.create(data_save);
                let data_save_2 = {
                  to_id:userId,
                  user_type:"user",
                  title:"Commande livrée",
                  message:message,
                  status: 'unread',
                  notification_type:"order_delivered"
                }
                await Notifications.create(data_save_2);
                if(admin_record)
                {
                    let admin_id = admin_record._id.toString();
                    let data_save_1 = {
                        to_id:admin_id,
                        user_type:"admin",
                        title:"Commande livrée",
                        message:message,
                        status: 'unread',
                        notification_type:"order_delivered"
                    }
                    await Notifications.create(data_save_1);

                }


                //return false;
                const sellerdoc = await seller.findById(provider_id);
                sellerdoc.totalpendingamount=sellerdoc.totalpendingamount+seller_totoal_profit;
                sellerdoc.save()

                await OrderModel.updateOne({_id:o_ID},{products:chekc_condition2});
                //return false;

                if(cc == user_order_id_arr.length)
                {
                  RoutePlansModel.findOne({_id:resultStop.route_id}).exec((error,resultRoute)=>{
                    if(error)
                    {
                      res.status(200)
                      .send({
                          error: true,
                          success: false,
                          errorMessage: error
                      });
                    }else{
                      if(resultRoute.completedStop == null || resultRoute.completedStop == "null")
                      {
                        var completedStop = 1;
                      }else{
                        var completedStop = resultRoute.completedStop + 1;
                      }
                      RoutePlansModel.updateOne({ "_id":resultRoute._id }, 
                        {
                          "completedStop":completedStop
                        }, function (uerr, docs) {
                        if (uerr){
                          //console.log(uerr);
                          res.status(200)
                            .send({
                                error: true,
                                success: false,
                                errorMessage: uerr
                            });
                        }else{
                            StopsModel.updateOne({ "_id":stop_id }, 
                            {
                              "status":2,
                              "deliveryText":deliveryText,
                              "deliverySignature":deliverySignature,
                              "deliveryPhoto":deliveryPhoto,
                              "deliveryNoteClient":deliveryNoteClient,
                              "deliveryNoteCompany":deliveryNoteCompany
                            }, function (uerr, docs) {
                            if (uerr){
                              //console.log(uerr);
                              res.status(200)
                                .send({
                                    error: true,
                                    success: false,
                                    errorMessage: uerr
                                });
                            }else{
                              if(completeStop == 1)
                              {
                                RoutePlansModel.updateOne({ "_id":resultRoute._id }, 
                                {
                                  "status":3
                                }, function (uerr, docs) {
                                  if (uerr){
                                    //console.log(uerr);
                                    res.status(200)
                                      .send({
                                          error: true,
                                          success: false,
                                          errorMessage: uerr
                                      });
                                  }else{
                                    var return_response = {"error":false,errorMessage:"Livré avec succès"};
                                    res.status(200).send(return_response);
                                  }
                                });
                              }else{
                                var return_response = {"error":false,errorMessage:"Livré avec succès"};
                                res.status(200).send(return_response);
                              }
                              
                            }
                          });
                        }
                      });
                    }
                  });
                }else{
                  var return_response = {"error":true,errorMessage:"Quelque chose n'a pas fonctionné"};
                  res.status(200).send(return_response);
                }

              }
              
            }else{
              console.log("resultStop 337 --->>>> "+resultStop);
              let user_order_id = resultStop.user_order_id;
              console.log("user_order_id 339 --->>>> "+user_order_id);
              if(user_order_id)
              {
                var explode_user_order_id = user_order_id.split(",");
                //console.log("explode_user_order_id 343 -->>> "+explode_user_order_id);
                if(explode_user_order_id.length > 0)
                {
                  let o_ID = explode_user_order_id[0];
                  let part_id_string = explode_user_order_id[1];
                  var explode_part_id = part_id_string.split("_");
                  if(explode_part_id.length>1)
                  {
                    let part_id = explode_part_id[1];

                    let order_cnancel_check = await OrderModel.findOne({_id:o_ID,products:{$elemMatch:{part_id:part_id}} },{products:1});
                      let chekc_condition = order_cnancel_check.products.filter((val)=>{
                        //console.log("val ",val);
                        if(val.part_id == part_id && val.status == 2)
                        {
                          return val;
                        }
                      });
                      //console.log("chekc_condition ",chekc_condition);
                      //console.log("order_cnancel_check  ", order_cnancel_check);
                      //return false;
                      if(chekc_condition.length > 0)
                      {
                        res.status(200)
                        .send({
                          error: true,
                          success: false,
                          errorMessage: "Annulation de la commande par l'utilisateur"
                        });
                      }

                      
                    try{

                      //console.log(OrderModel+" - "+ o_ID+" - "+ part_id);
                      var response_modules = await updateorderstatus(OrderModel,o_ID,part_id, "" ,"","Pickup",1);
                      console.log("response_modules "+ response_modules);
                      //console.log("response_modules "+ JSON.stringify(response_modules));
                      if(response_modules ==true)
                      {
                        
                        RoutePlansModel.findOne({_id:resultStop.route_id}).exec((error,resultRoute)=>{
                          if(error)
                          {
                            res.status(200)
                            .send({
                                error: true,
                                success: false,
                                errorMessage: error
                            });
                          }else{
                            if(resultRoute.completedStop == null || resultRoute.completedStop == "null")
                            {
                              var completedStop = 1;
                            }else{
                              var completedStop = resultRoute.completedStop + 1;
                            }
                            RoutePlansModel.updateOne({ "_id":resultRoute._id }, 
                              {
                                "completedStop":completedStop
                              }, function (uerr, docs) {
                              if (uerr){
                                //console.log(uerr);
                                res.status(200)
                                  .send({
                                      error: true,
                                      success: false,
                                      errorMessage: uerr
                                  });
                              }else{
                                  StopsModel.updateOne({ "_id":stop_id }, 
                                  {
                                    "status":2,
                                    "deliveryText":deliveryText,
                                    "deliverySignature":deliverySignature,
                                    "deliveryPhoto":deliveryPhoto,
                                    "deliveryNoteClient":deliveryNoteClient,
                                    "deliveryNoteCompany":deliveryNoteCompany
                                  }, function (uerr, docs) {
                                  if (uerr){
                                    //console.log(uerr);
                                    res.status(200)
                                      .send({
                                          error: true,
                                          success: false,
                                          errorMessage: uerr
                                      });
                                  }else{
                                    if(completeStop == 1)
                                    {
                                      RoutePlansModel.updateOne({ "_id":resultRoute._id }, 
                                      {
                                        "status":3
                                      }, function (uerr, docs) {
                                        if (uerr){
                                          //console.log(uerr);
                                          res.status(200)
                                            .send({
                                                error: true,
                                                success: false,
                                                errorMessage: uerr
                                            });
                                        }else{
                                          var return_response = {"error":false,errorMessage:"Livré avec succès"};
                                          res.status(200).send(return_response);
                                        }
                                      });
                                    }else{
                                      var return_response = {"error":false,errorMessage:"Livré avec succès"};
                                      res.status(200).send(return_response);
                                    }
                                    
                                  }
                                });
                              }
                            });
                          }
                        });


                      }else{
                        var return_response = {"error":true,errorMessage:"Quelque chose n'a pas fonctionné"};
                        res.status(200).send(return_response);
                      }
                    }catch(error){
                      var return_response = {"error":true,errorMessage:error};
                      res.status(200).send(return_response);
                    }
                    
                  }
                }
              }
            }
          }else{

            //console.log("resultStop 332 --------->>>>>>>>> "+resultStop);
            


            if(resultStop.shipment_id.length > 0)
            {
              var shipstatus = 2;
              if(resultStop.stopType == "Pickup")
              {
                shipstatus = 2;

                if(resultStop.orderType == "merchant_order")
                {
                  if(resultStop.stopType == 'Pickup')
                  {
                    let jj = resultStop.shipment_id.toString();
                    var shipMentRecord  = await ShipmentModel.findById(jj);
                    //console.log("shipMentRecord ", shipMentRecord);
                    if(shipMentRecord)
                    {
                      let order_id = shipMentRecord.order_id;
                      let provider_id = shipMentRecord.user_id.toString();
                      let message = "Commande enlevée par le chauffeur avec l'identifiant de la commande - "+order_id;
                      let data_save = {
                          to_id:provider_id,
                          user_type:"user",
                          title:"Enlèvement de la commande",
                          message:message,
                          status: 'unread',
                          notification_type:"driver_pickup_order"
                      }
                      await Notifications.create(data_save);

                    }
                  }
                }


              }else if(resultStop.stopType == "Delivery"){
                shipstatus = 3;

                if(resultStop.orderType == "merchant_order")
                {
                  if(resultStop.stopType == 'Delivery')
                  {
                    let jj = resultStop.shipment_id.toString();
                    var shipMentRecord  = await ShipmentModel.findById(jj);
                    //console.log("shipMentRecord ", shipMentRecord);
                    if(shipMentRecord)
                    {
                      let order_id = shipMentRecord.order_id;
                      let provider_id = shipMentRecord.user_id.toString();
                      let message = "Commande livrée par un chauffeur avec l'identifiant de la commande - "+order_id;
                      let data_save = {
                          to_id:provider_id,
                          user_type:"user",
                          title:"Enlèvement de la commande",
                          message:message,
                          status: 'unread',
                          notification_type:"driver_delivery_order"
                      }
                      await Notifications.create(data_save);

                    }
                  }
                }

              }else{
                shipstatus = 2;
              }

              ShipmentModel.updateOne({_id:resultStop.shipment_id[0]},{"status":shipstatus},function(shipError,shipResult){
                if(shipError)
                {
                  res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: shipError
                  });
                }else{
                  RoutePlansModel.findOne({_id:resultStop.route_id}).exec((error,resultRoute)=>{
                    if(error)
                    {
                      res.status(200)
                      .send({
                          error: true,
                          success: false,
                          errorMessage: error
                      });
                    }else{
                      if(resultRoute.completedStop == null || resultRoute.completedStop == "null")
                      {
                        var completedStop = 1;
                      }else{
                        var completedStop = resultRoute.completedStop + 1;
                      }
                      RoutePlansModel.updateOne({ "_id":resultRoute._id }, 
                        {
                          "completedStop":completedStop
                        }, function (uerr, docs) {
                        if (uerr){
                          //console.log(uerr);
                          res.status(200)
                            .send({
                                error: true,
                                success: false,
                                errorMessage: uerr
                            });
                        }else{
                            StopsModel.updateOne({ "_id":stop_id }, 
                            {
                              "status":2,
                              "deliveryText":deliveryText,
                              "deliverySignature":deliverySignature,
                              "deliveryPhoto":deliveryPhoto,
                              "deliveryNoteClient":deliveryNoteClient,
                              "deliveryNoteCompany":deliveryNoteCompany
                            }, function (uerr, docs) {
                            if (uerr){
                              //console.log(uerr);
                              res.status(200)
                                .send({
                                    error: true,
                                    success: false,
                                    errorMessage: uerr
                                });
                            }else{
                              if(completeStop == 1)
                              {
                                RoutePlansModel.updateOne({ "_id":resultRoute._id }, 
                                {
                                  "status":3
                                }, function (uerr, docs) {
                                  if (uerr){
                                    //console.log(uerr);
                                    res.status(200)
                                      .send({
                                          error: true,
                                          success: false,
                                          errorMessage: uerr
                                      });
                                  }else{
                                    var return_response = {"error":false,errorMessage:"Livré avec succès"};
                                    res.status(200).send(return_response);
                                  }
                                });
                              }else{
                                var return_response = {"error":false,errorMessage:"Livré avec succès"};
                                res.status(200).send(return_response);
                              }
                              
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            }else{
              RoutePlansModel.findOne({_id:resultStop.route_id}).exec((error,resultRoute)=>{
                if(error)
                {
                  res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: error
                  });
                }else{
                  if(resultRoute.completedStop == null || resultRoute.completedStop == "null")
                  {
                    var completedStop = 1;
                  }else{
                    var completedStop = resultRoute.completedStop + 1;
                  }
                  RoutePlansModel.updateOne({ "_id":resultRoute._id }, 
                    {
                      "completedStop":completedStop
                    }, function (uerr, docs) {
                    if (uerr){
                      //console.log(uerr);
                      res.status(200)
                        .send({
                            error: true,
                            success: false,
                            errorMessage: uerr
                        });
                    }else{
                        StopsModel.updateOne({ "_id":stop_id }, 
                        {
                          "status":2,
                          "deliveryText":deliveryText,
                          "deliverySignature":deliverySignature,
                          "deliveryPhoto":deliveryPhoto,
                          "deliveryNoteClient":deliveryNoteClient,
                          "deliveryNoteCompany":deliveryNoteCompany
                        }, function (uerr, docs) {
                        if (uerr){
                          //console.log(uerr);
                          res.status(200)
                            .send({
                                error: true,
                                success: false,
                                errorMessage: uerr
                            });
                        }else{
                          if(completeStop == 1)
                          {
                            RoutePlansModel.updateOne({ "_id":resultRoute._id }, 
                            {
                              "status":3
                            }, function (uerr, docs) {
                              if (uerr){
                                //console.log(uerr);
                                res.status(200)
                                  .send({
                                      error: true,
                                      success: false,
                                      errorMessage: uerr
                                  });
                              }else{
                                var return_response = {"error":false,errorMessage:"Livré avec succès"};
                                res.status(200).send(return_response);
                              }
                            });
                          }else{
                            var return_response = {"error":false,errorMessage:"Livré avec succès"};
                            res.status(200).send(return_response);
                          }
                          
                        }
                      });
                    }
                  });
                }
              });
            }
          }
        }else{
          res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: "L'identifiant de l'arrêt n'est pas valide"
          });
        }
      }
    }catch(error){
      console.log(error);
      res.status(200)
      .send({
        error: true,
        success: false,
        errorMessage: error
      });
    }
  }, 
  skipStop:async function(req,res,next)
  {
    try{
      //console.log(req.body);
      //console.log(req.files);
      var {deliveryText,deliveryNoteClient,deliveryNoteCompany,stop_id,completeStop} = req.body;
      
      if(stop_id == "" || stop_id == null)
      {
        res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: "L'identité est requise"
        });
      }else{
        // StopsModel.findOne({ "_id":stop_id }, 
        //   {
        //     "route_id":1,
        //     "shipment_id":1,"status":1
        //   }, function (err, resultStop) {
        //   if (err){
        //     //console.log(err);
        //     res.status(200)
        //       .send({
        //           error: true,
        //           success: false,
        //           errorMessage: err
        //       });
        //   }else{}
        // });
        var resultStop = await StopsModel.findById(stop_id);
        //console.log("resultStop 332 --------->>>>>>>>> "+resultStop);return false;
        if(resultStop){
          if(resultStop.orderType == 'user_order')
          {
            if(resultStop.stopType == 'Delivery')
            {
              console.log("resultStop 337 first --->>>> "+resultStop);
              let user_order_id_arr = resultStop.user_order_id_arr;
              console.log("user_order_id_arr 339 first --->>>> "+JSON.stringify(user_order_id_arr));
              if(user_order_id_arr.length > 0)
              {
                var less_val_order = user_order_id_arr.length - 1;
                var cc = 0;
                for(let mm=0; mm<user_order_id_arr.length; mm++)
                {
                  console.log("user_order_id_arr 340 first --->>>> "+user_order_id_arr[mm].item_id);
                  var explode_user_order_id = user_order_id_arr[mm].item_id.split(",");
                  if(explode_user_order_id.length > 0)
                  {
                    let o_ID = explode_user_order_id[0];
                    let part_id_string = explode_user_order_id[1];
                    var explode_part_id = part_id_string.split("_");
                    if(explode_part_id.length>1)
                    {
                      let part_id = explode_part_id[1];
                      var response_order_api = await updateorderstatus(OrderModel,o_ID,part_id, "" ,"","Delivery",4)
                      if(response_order_api == true)
                      {
                        cc++;
                      }
                    }
                  }
                }
                if(cc == user_order_id_arr.length)
                {
                  console.log("762 line no");
                  RoutePlansModel.findOne({_id:resultStop.route_id}).exec((error,resultRoute)=>{
                    if(error)
                    {
                      res.status(200)
                      .send({
                          error: true,
                          success: false,
                          errorMessage: error
                      });
                    }else{
                      if(resultRoute.missedStop == null || resultRoute.missedStop == "null")
                      {
                        var missedStop = 1;
                      }else{
                        var missedStop = resultRoute.missedStop + 1;
                      }
                      RoutePlansModel.updateOne({ "_id":resultRoute._id }, 
                        {
                          "missedStop":missedStop
                        }, function (uerr, docs) {
                        if (uerr){
                          //console.log(uerr);
                          res.status(200)
                            .send({
                                error: true,
                                success: false,
                                errorMessage: uerr
                            });
                        }else{
                            StopsModel.updateOne({ "_id":stop_id }, 
                            {
                              "status":3,
                              "deliveryText":deliveryText,
                              "deliveryNoteClient":deliveryNoteClient,
                              "deliveryNoteCompany":deliveryNoteCompany
                            }, function (uerr, docs) {
                            if (uerr){
                              //console.log(uerr);
                              res.status(200)
                                .send({
                                    error: true,
                                    success: false,
                                    errorMessage: uerr
                                });
                            }else{
    
                              if(completeStop == 1)
                              {
                                RoutePlansModel.updateOne({ "_id":resultRoute._id }, 
                                {
                                  "status":3
                                }, function (uerr, docs) {
                                  if (uerr){
                                    //console.log(uerr);
                                    res.status(200)
                                      .send({
                                          error: true,
                                          success: false,
                                          errorMessage: uerr
                                      });
                                  }else{
                                    var return_response = {"error":false,errorMessage:"Saut de livraison réussi"};
                                    res.status(200).send(return_response);
                                  }
                                });
                              }else{
                                var return_response = {"error":false,errorMessage:"Saut de livraison réussi"};
                                res.status(200).send(return_response);
                              }
                            }
                          });
                        }
                      });
                    }
                  });
                }else{
                  console.log("762 line no");
                  var return_response = {"error":true,errorMessage:"Quelque chose n'a pas fonctionné"};
                  res.status(200).send(return_response);
                }

              }
            }else{
              console.log("resultStop 337 --->>>> "+resultStop);
              let user_order_id = resultStop.user_order_id;
              console.log("user_order_id 339 --->>>> "+user_order_id);
              if(user_order_id)
              {
                var explode_user_order_id = user_order_id.split(",");
                //console.log("explode_user_order_id 343 -->>> "+explode_user_order_id);
                if(explode_user_order_id.length > 0)
                {
                  let o_ID = explode_user_order_id[0];
                  let part_id_string = explode_user_order_id[1];
                  var explode_part_id = part_id_string.split("_");
                  if(explode_part_id.length>1)
                  {
                    let part_id = explode_part_id[1];
                    try{

                      //console.log(OrderModel+" - "+ o_ID+" - "+ part_id);
                      var response_modules = await updateorderstatus(OrderModel,o_ID,part_id, "" ,"","Pickup",4);
                      console.log("response_modules "+ response_modules);
                      //console.log("response_modules "+ JSON.stringify(response_modules));
                      if(response_modules ==true)
                      {
                        RoutePlansModel.findOne({_id:resultStop.route_id}).exec((error,resultRoute)=>{
                          if(error)
                          {
                            res.status(200)
                            .send({
                                error: true,
                                success: false,
                                errorMessage: error
                            });
                          }else{
                            if(resultRoute.missedStop == null || resultRoute.missedStop == "null")
                            {
                              var missedStop = 1;
                            }else{
                              var missedStop = resultRoute.missedStop + 1;
                            }
                            RoutePlansModel.updateOne({ "_id":resultRoute._id }, 
                              {
                                "missedStop":missedStop
                              }, function (uerr, docs) {
                              if (uerr){
                                //console.log(uerr);
                                res.status(200)
                                  .send({
                                      error: true,
                                      success: false,
                                      errorMessage: uerr
                                  });
                              }else{
                                  StopsModel.updateOne({ "_id":stop_id }, 
                                  {
                                    "status":3,
                                    "deliveryText":deliveryText,
                                    "deliveryNoteClient":deliveryNoteClient,
                                    "deliveryNoteCompany":deliveryNoteCompany
                                  }, function (uerr, docs) {
                                  if (uerr){
                                    //console.log(uerr);
                                    res.status(200)
                                      .send({
                                          error: true,
                                          success: false,
                                          errorMessage: uerr
                                      });
                                  }else{
          
                                    if(completeStop == 1)
                                    {
                                      RoutePlansModel.updateOne({ "_id":resultRoute._id }, 
                                      {
                                        "status":3
                                      }, function (uerr, docs) {
                                        if (uerr){
                                          //console.log(uerr);
                                          res.status(200)
                                            .send({
                                                error: true,
                                                success: false,
                                                errorMessage: uerr
                                            });
                                        }else{
                                          var return_response = {"error":false,errorMessage:"Saut de livraison réussi"};
                                          res.status(200).send(return_response);
                                        }
                                      });
                                    }else{
                                      var return_response = {"error":false,errorMessage:"Saut de livraison réussi"};
                                      res.status(200).send(return_response);
                                    }
                                  }
                                });
                              }
                            });
                          }
                        });
                      }else{
                        var return_response = {"error":true,errorMessage:"Quelque chose n'a pas fonctionné"};
                        res.status(200).send(return_response);
                      }
                    }catch(error){
                      var return_response = {"error":true,errorMessage:error};
                      res.status(200).send(return_response);
                    }
                    
                  }
                }
              }
            }
          }else{
            if(resultStop.status == 3)
            {
              res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Cet arrêt est déjà livré"
              });
            }else{
              if(resultStop.shipment_id.length > 0)
              {
                ShipmentModel.updateOne({_id:resultStop.shipment_id[0]},{"status":4},function(shipError,shipResult){
                  if(shipError)
                  {
                    res.status(200)
                    .send({
                        error: true,
                        success: false,
                        errorMessage: shipError
                    });
                  }else{
                    RoutePlansModel.findOne({_id:resultStop.route_id}).exec((error,resultRoute)=>{
                      if(error)
                      {
                        res.status(200)
                        .send({
                            error: true,
                            success: false,
                            errorMessage: error
                        });
                      }else{
                        if(resultRoute.missedStop == null || resultRoute.missedStop == "null")
                        {
                          var missedStop = 1;
                        }else{
                          var missedStop = resultRoute.missedStop + 1;
                        }
                        RoutePlansModel.updateOne({ "_id":resultRoute._id }, 
                          {
                            "missedStop":missedStop
                          }, function (uerr, docs) {
                          if (uerr){
                            //console.log(uerr);
                            res.status(200)
                              .send({
                                  error: true,
                                  success: false,
                                  errorMessage: uerr
                              });
                          }else{
                              StopsModel.updateOne({ "_id":stop_id }, 
                              {
                                "status":3,
                                "deliveryText":deliveryText,
                                "deliveryNoteClient":deliveryNoteClient,
                                "deliveryNoteCompany":deliveryNoteCompany
                              }, function (uerr, docs) {
                              if (uerr){
                                //console.log(uerr);
                                res.status(200)
                                  .send({
                                      error: true,
                                      success: false,
                                      errorMessage: uerr
                                  });
                              }else{
      
                                if(completeStop == 1)
                                {
                                  RoutePlansModel.updateOne({ "_id":resultRoute._id }, 
                                  {
                                    "status":3
                                  }, function (uerr, docs) {
                                    if (uerr){
                                      //console.log(uerr);
                                      res.status(200)
                                        .send({
                                            error: true,
                                            success: false,
                                            errorMessage: uerr
                                        });
                                    }else{
                                      var return_response = {"error":false,errorMessage:"Saut de livraison réussi"};
                                      res.status(200).send(return_response);
                                    }
                                  });
                                }else{
                                  var return_response = {"error":false,errorMessage:"Saut de livraison réussi"};
                                  res.status(200).send(return_response);
                                }
                              }
                            });
                          }
                        });
                      }
                    });
                  }
                });
              }else{
                RoutePlansModel.findOne({_id:resultStop.route_id}).exec((error,resultRoute)=>{
                  if(error)
                  {
                    res.status(200)
                    .send({
                        error: true,
                        success: false,
                        errorMessage: error
                    });
                  }else{
                    if(resultRoute.missedStop == null || resultRoute.missedStop == "null")
                    {
                      var missedStop = 1;
                    }else{
                      var missedStop = resultRoute.missedStop + 1;
                    }
                    RoutePlansModel.updateOne({ "_id":resultRoute._id }, 
                      {
                        "missedStop":missedStop
                      }, function (uerr, docs) {
                      if (uerr){
                        //console.log(uerr);
                        res.status(200)
                          .send({
                              error: true,
                              success: false,
                              errorMessage: uerr
                          });
                      }else{
                          StopsModel.updateOne({ "_id":stop_id }, 
                          {
                            "status":3,
                            "deliveryText":deliveryText,
                            "deliveryNoteClient":deliveryNoteClient,
                            "deliveryNoteCompany":deliveryNoteCompany
                          }, function (uerr, docs) {
                          if (uerr){
                            //console.log(uerr);
                            res.status(200)
                              .send({
                                  error: true,
                                  success: false,
                                  errorMessage: uerr
                              });
                          }else{

                            if(completeStop == 1)
                            {
                              RoutePlansModel.updateOne({ "_id":resultRoute._id }, 
                              {
                                "status":3
                              }, function (uerr, docs) {
                                if (uerr){
                                  //console.log(uerr);
                                  res.status(200)
                                    .send({
                                        error: true,
                                        success: false,
                                        errorMessage: uerr
                                    });
                                }else{
                                  var return_response = {"error":false,errorMessage:"Saut de livraison réussi"};
                                  res.status(200).send(return_response);
                                }
                              });
                            }else{
                              var return_response = {"error":false,errorMessage:"Saut de livraison réussi"};
                              res.status(200).send(return_response);
                            }
                          }
                        });
                      }
                    });
                  }
                });
              }
              
            }
          }
        }else{
          res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: "L'identifiant de l'arrêt n'est pas valide"
          });
        }
          
      }
    }catch(error){
      console.log(error);
      res.status(200)
      .send({
        error: true,
        success: false,
        errorMessage: error
      });
    }
  },
  getDriverAllRoutePlan:async function(req,res,next)
  {
    try{
      //console.log(req.body);
      //mongoose.set('debug', true);
      var String_qr = {};  
      var todayDate = new Date();
      todayDate.setHours(0,0,0,0);
      //console.log("todayDate ----> "+todayDate);

      
      var {driver_id,dayValue,status,startDate,endDate} = req.body;
      if(driver_id == "" || driver_id == null)
      {
        res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: "Une carte d'identité de chauffeur est requise"
        });
      }else{
        console.log("startDate "+startDate);
        if(endDate)
        {
          var dd = endDate+" 23:59:59";
          //console.log(dd);return false;
          var newendDatedate = new Date(dd);
          console.log("newendDatedate"+newendDatedate.toISOString("yyyy-MM-dd'T'29:59:59.SSS'Z'"));
        }
        
        if(endDate != "" && endDate != null && startDate != "" && startDate != null )
        {
          String_qr['created_at'] = { $lte: newendDatedate,$gte: startDate };
        }
        String_qr['driver_id'] = driver_id;
        String_qr['status'] = {$lte:4};
        //console.log("String_qr "+ JSON.stringify(String_qr));
        RoutePlansModel.find(String_qr).sort({created_at:-1}).exec((err,result)=>{
          if(err)
          {
            res.status(200)
            .send({
              error: true,
              success: false,
              errorMessage: err
            });
          }else{
            //console.log(result);
            res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Succès",
                record:result
            });
          }
        });
      }
    }catch(error){
      console.log(error);
      res.status(200)
      .send({
        error: true,
        success: false,
        errorMessage: error
      });
    }
  },
  getDeliveredStop:async function(req,res,next)
  {
    try{
      //console.log("heree "+req.body);
      var image_url = customConstant.base_url+"public/uploads/deliveryStop/";
      var {stop_id} = req.body;
      if(stop_id == "" || stop_id == null)
      {
        res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: "L'identité est requise"
        });
      }else{
        StopsModel.findOne({_id:stop_id}).exec((err,result)=>{
          if(err)
          {
            res.status(200)
            .send({
              error: true,
              success: false,
              image_url:image_url,
              errorMessage: err
            });
          }else{
            //console.log(result);
            res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Succès",
                image_url:image_url,
                record:result
            });
          }
        });
      }
    }catch(error){
      console.log(error);
      res.status(200)
      .send({
        error: true,
        success: false,
        errorMessage: error
      });
    }
  },
  getSkippedStop:async function(req,res,next)
  {
    try{
      //console.log("heree "+req.body);
      var {stop_id} = req.body;
      if(stop_id == "" || stop_id == null)
      {
        res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: "L'identité est requise"
        });
      }else{
        StopsModel.findOne({_id:stop_id}).exec((err,result)=>{
          if(err)
          {
            res.status(200)
            .send({
              error: true,
              success: false,
              errorMessage: err
            });
          }else{
            //console.log(result);
            res.status(200)
            .send({
                error: false,
                success: true,
                errorMessage: "Succès",
                record:result
            });
          }
        });
      }
    }catch(error){
      console.log(error);
      res.status(200)
      .send({
        error: true,
        success: false,
        errorMessage: error
      });
    }
  },
  startRoute:async function(req,res,next)
  {
    try{
      //console.log(req.body);
      //console.log(req.files);
      var {route_id} = req.body;
      if(route_id == "" || route_id == null)
      {
        res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: "L'identité est requise"
        });
      }else{
        RoutePlansModel.updateOne({ "_id":route_id }, 
          {
            "status":2
          }, function (uerr, docs) {
          if (uerr){
            //console.log(uerr);
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: uerr
              });
          }else{
            res.status(200)
              .send({
                  error: false,
                  success: true,
                  errorMessage: "Route commencée succès complet"
              });
          }
        });
      }
    }catch(error){
      console.log(error);
      res.status(200)
      .send({
        error: true,
        success: false,
        errorMessage: error
      });
    }
  },
};