var express = require('express');
var router = express.Router();
const userRouter=require("./user/userRouter");
const CommonRouter=require("./common/CommonRouter");
const cartRouter=require("./cart/CartRouter");
const eventRouter=require("./events/EventRouter");
const orderRouter=require("./orders/OrderRouter");
const productRouter=require("./products/ProductRouter");
const salesRouter=require("./sales/SalesRouter");
const InventoryRouter=require("./inventory/InventoryRouter");
const SellerRouter= require("./seller/SellerRouter");
const ShippingRouter=require("./shipping/ShippingRouter");
const subsRouter= require("./subscriptions/SubscriptionRouter");
const AngularAdminRouter= require("./angular-admin/AngularAdminRouter");
const DriverRouter= require("./drivers/DriverRouter");
const MerchantRouter= require("./merchant/MerchantRouter");
const ClientRouter= require("./client/ClientRouter");
const ShipmentRouter= require("./shipment/ShipmentRouter");
const adminRouter=require("./admin/index.js")
const roomcontroller=require("./chat/chatRoom")
router.use("/user",userRouter);
router.use("/cart",cartRouter);
router.use("/event",eventRouter);
router.use("/order",orderRouter);
router.use("/product",productRouter);
router.use("/sales",salesRouter);
router.use("/inventory",InventoryRouter);
router.use("/seller",SellerRouter);
router.use("/common",CommonRouter);
router.use("/shipping",ShippingRouter);
router.use("/subs",subsRouter);
router.use("/admin",adminRouter);
router.use("/chat",roomcontroller);
router.use("",AngularAdminRouter);
router.use("",DriverRouter);
router.use("",MerchantRouter);
router.use("",ClientRouter);
router.use("",ShipmentRouter);


module.exports = router;
