// utils
// const makeValidation = require('@withvoid/make-validation');
// models
const  { CHAT_ROOM_TYPES ,ChatRoom} = require('../../models/chat/ChatRoom');
const {ChatMessage} = require('../../models/chat/ChatMessage.js');
const UserModel = require('../../models/user/User');
const Chat = require('../../models/chat/chat.js');
const ProductChatModel = require('../../models/chat/ProductChatModel');
const ReportChatModel = require('../../models/chat/ReportChatModel');

const mongoose=require("mongoose");
module.exports=  {

  new_initiate_chat:async(req,res)=>
  {
    console.log(req.body);
    var {product_id,seller_id,user_id,message,message_by} = req.body;
    if(product_id == "" || product_id == null || product_id == "null" || product_id == undefined  )
    { 
      return res.send({
        status:false,
        message:"L'identifiant du produit est requis"
      });
    }
    if(seller_id == "" || seller_id == null || seller_id == "null" || seller_id == undefined  )
    { 
      return res.send({
        status:false,
        message:"L'identifiant du vendeur est requis"
      });
    }
    if(user_id == "" || user_id == null || user_id == "null" || user_id == undefined  )
    { 
      return res.send({
        status:false,
        message:"L'identifiant de l'utilisateur est requis"
      });
    }
    if(message == "" || message == null || message == "null" || message == undefined  )
    { 
      return res.send({
        status:false,
        message:"L'identifiant du message est requis"
      });
    }
    if(message_by == "" || message_by == null || message_by == "null" || message_by == undefined  )
    { 
      return res.send({
        status:false,
        message:"Un message par est requis"
      });
    }
    const old_chat = await ProductChatModel.findOne({
      "user_id":user_id,
      "product_id":product_id,
    });
    if(old_chat)
    {
      var room_id = old_chat.room_id;
    }else{
      var room_id = Array.from(Array(30), () => Math.floor(Math.random() * 36).toString(36)).join('');
    }
    const pro_chat = new ProductChatModel();
      pro_chat.room_id = room_id;
      pro_chat.product_id = product_id;
      pro_chat.seller_id = seller_id;
      pro_chat.user_id = user_id;
      pro_chat.message = message;
      pro_chat.message_by = message_by;
      pro_chat.save().then((result)=>{
        return res.send({
          status:true,
          message:"Votre message a été envoyé avec succès",
          room_id:room_id
        })
      }).catch((err)=>{
        return res.send({
            status:false,
            message:err.message
        })
      })
  },

  check_cht_available_or_not:async(req,res)=>{
    var {product_id,seller_id,user_id} = req.body;
    if(product_id == "" || product_id == null || product_id == "null" || product_id == undefined  )
    { 
      return res.send({
        status:false,
        message:"L'identifiant du produit est requis"
      });
    }
    if(seller_id == "" || seller_id == null || seller_id == "null" || seller_id == undefined  )
    { 
      return res.send({
        status:false,
        message:"L'identifiant du vendeur est requis"
      });
    }
    if(user_id == "" || user_id == null || user_id == "null" || user_id == undefined  )
    { 
      return res.send({
        status:false,
        message:"L'identifiant de l'utilisateur est requis"
      });
    }
     const rec = await ProductChatModel.findOne({user_id:user_id,product_id:product_id,seller_id:seller_id});
     //console.log(rec);
     if(rec)
     {
        return res.send({
            status:true,
            message:"Succès",
            data:rec
        })
     }else{
        return res.send({
          status:false,
          message:"Pas d'enregistrement",
          data:[]
      })
     }
    // return res.send({
    //     status:true,
    //     message:"Success",
    //     data:chat_rec
    // })
  },

  get_all_user_chat:async(req,res)=>{
    var user_id = req.params.id;
    console.log(user_id);
    if(user_id == "" || user_id == null || user_id == "null" || user_id == undefined  )
    { 
      return res.send({
        status:false,
        message:"L'identifiant de l'utilisateur est requis"
      });
    }
    //mongoose.set("debug",true);
    //mongoose.set("debug",true);
    const chat_rec = await ProductChatModel.aggregate([
      {
        $match:{
            "user_id":user_id,
        },
        
      },
      {
        $match:{
          "del_flag_user":0
        }
      },
      {
        $addFields:{
          "slr_id":{"$toObjectId":"$seller_id"}
        }
      },
      {
        $lookup:{
          from:"sellers",
          localField:"slr_id",
          foreignField:"_id",
          as:"seller_rec"
        }
      },
      {
        $addFields:{
          "pro_id":{"$toObjectId":"$product_id"}
        }
      },
      {
        $lookup:{
          from:"products",
          localField:"pro_id",
          foreignField:"_id",
          as:"pro_rec"
        }
      },
      {
        $group:{
          _id:"$room_id",
          "seller_id": { "$first": "$seller_id" },
          "product_id": { "$first": "$product_id" },
          "user_id": { "$first": "$user_id" },
          "user_id": { "$first": "$user_id" },
          "message_by": { "$first": "$message_by" },
					"message": { "$first": "$message" },
          "created_at": { "$first": "$created_at" },
          "seller_shopname": { "$first": "$seller_rec.shopname" },
          "pro_rec_title": { "$first": "$pro_rec.title" },
          "pro_rec_description": { "$first": "$pro_rec.description" },
          "pro_rec_images": { "$first": "$pro_rec.images" },
        }
      },
      {
        $sort:{ "created_at": -1 }
      }
      

    ]);
    return res.send({
        status:true,
        message:"Success",
        data:chat_rec
    })
  },
  get_all_seller_chat:async(req,res)=>{
    var seller_id = req.params.id;
    console.log(seller_id);
    if(seller_id == "" || seller_id == null || seller_id == "null" || seller_id == undefined  )
    { 
      return res.send({
        status:false,
        message:"L'identifiant de l'utilisateur est requis"
      });
    }
   // mongoose.set("debug",true);
    const chat_rec = await ProductChatModel.aggregate([
      {
        $match:{
            "seller_id":seller_id,
        }
      },
      {

        $match:{
          "del_flag_seller":0
        }
      },
      {
        $addFields:{
          "slr_id":{"$toObjectId":"$user_id"}
        }
      },
      {
        $lookup:{
          from:"users",
          localField:"slr_id",
          foreignField:"_id",
          as:"seller_rec"
        }
      },
      {
        $addFields:{
          "pro_id":{"$toObjectId":"$product_id"}
        }
      },
      {
        $lookup:{
          from:"products",
          localField:"pro_id",
          foreignField:"_id",
          as:"pro_rec"
        }
      },
      {
        $group:{
          _id:"$room_id",
          "seller_id": { "$first": "$seller_id" },
          "product_id": { "$first": "$product_id" },
          "user_id": { "$first": "$user_id" },
          "message_by": { "$first": "$message_by" },
					"message": { "$first": "$message" },
          "created_at": { "$first": "$created_at" },
          "user_first_name": { "$first": "$seller_rec.first_name" },
          "user_last_name": { "$first": "$seller_rec.last_name" },
          "pro_rec_title": { "$first": "$pro_rec.title" },
          "pro_rec_images": { "$first": "$pro_rec.images" },
        }
      },
      {
        $sort:{ "created_at": -1 }
      }
      

    ]);
    return res.send({
        status:true,
        message:"Success",
        data:chat_rec
    })
  },
  get_all_message_room_seller:async(req,res)=>{
    try{
      //mongoose.set("debug",true);
        // "room_id": "czrufjbt3ekykm9g1yeh2zrfifap00",
        // "seller_id": "63f5c1ec5bf0bda586960971",
        // "user_id": "642d5d64f5a88587ffae0ab5",
        // "message": "hello seller new chat", 
        // "created_at": "2023-04-13T10:32:52.061Z",

      var room_id = req.params.id;
      const pro_chat = await ProductChatModel.aggregate([
            {  
              $match:{
                room_id:room_id
              }
            },
            {
              $match:{
                del_flag_seller:0
              }
            },
            {
              $addFields:{
                "usr_id":{"$toObjectId":"$user_id"}
              }
            },
            {
              $lookup:{
                from:"users",
                localField:"usr_id",
                foreignField:"_id",
                as:"user_rec"
              }
            },
            {
              $unwind:"$user_rec"
            },
            {
              $addFields:{
                "first_name":"$user_rec.first_name",
                "photo":"$user_rec.photo",
                "last_name":"$user_rec.last_name"
              }
            },
            {
              $project:{
                "user_rec":0
              }
            },
            {
              $sort:{"created_at":1}
            }
          ]);
      return res.send({
        status:true,
        message:"Succès",
        data:pro_chat
      })
    }catch(error)
    {
      return res.send({
        status:false,
        message:error.message
      })
    }

  },
  get_all_message_room_user:async(req,res)=>{
    try{
      //mongoose.set("debug",true);
        // "room_id": "czrufjbt3ekykm9g1yeh2zrfifap00",
        // "seller_id": "63f5c1ec5bf0bda586960971",
        // "user_id": "642d5d64f5a88587ffae0ab5",
        // "message": "hello seller new chat", 
        // "created_at": "2023-04-13T10:32:52.061Z",

      var room_id = req.params.id;
      const pro_chat = await ProductChatModel.aggregate([
            {  
              $match:{
                room_id:room_id
              }
            },
            {
              $match:{
                del_flag_user:0
              }
            },
            {
              $addFields:{
                "usr_id":{"$toObjectId":"$seller_id"}
              }
            },
            {
              $lookup:{
                from:"sellers",
                localField:"usr_id",
                foreignField:"_id",
                as:"user_rec"
              }
            },
            {
              $unwind:"$user_rec"
            },
            {
              $addFields:{
                "shopname":"$user_rec.shopname",
                "photo":"$user_rec.photo",
                "fullname":"$user_rec.fullname"
              }
            },
            {
              $project:{
                "user_rec":0
              }
            },
            {
              $sort:{"created_at":-1}
            }
          ]);
      return res.send({
        status:true,
        message:"Succès",
        data:pro_chat
      })
    }catch(error)
    {
      return res.send({
        status:false,
        message:error.message
      })
    }

  },
  
  delete_chat_user_by_room_id:async(req,res)=>{
    try{
      var room_id = req.params.id;
      const pro_chat = await ProductChatModel.updateMany({room_id:room_id},{
        "del_flag_user":1
      }).then((result)=>{
        return res.send({
          status:true,
          message:"Chat supprimé succès complet",
        })
      }).catch((error)=>{
        return res.send({
          status:false,
          message:error.message,
        })
      });      
    }catch(error)
    {
      return res.send({
        status:false,
        message:error.message
      })
    }

  },

  delete_chat_seller_by_room_id:async(req,res)=>{
    try{
      var room_id = req.params.id;
      const pro_chat = await ProductChatModel.updateMany({room_id:room_id},{
        "del_flag_seller":1
      }).then((result)=>{
        return res.send({
          status:true,
          message:"Chat supprimé succès complet",
        })
      }).catch((error)=>{
        return res.send({
          status:false,
          message:error.message,
        })
      });  
    }catch(error)
    {
      return res.send({
        status:false,
        message:error.message
      })
    }

  },
  report_chat_by_seller:async(req,res)=>{
    try{
      var {room_id,reason} = req.body;
      //console.log(req.body)
      const single_chat = await ProductChatModel.findOne({room_id:room_id});
      //console.log(single_chat);
      if(!single_chat)
      {
        return res.send({
          status:false,
          message:"Identifiant de salle invalide",
        })
      }
      let product_id = single_chat.product_id;
      let seller_id = single_chat.seller_id;
      let user_id = single_chat.user_id;
      var re =  await ProductChatModel.updateMany({room_id:room_id},{report_seller:1});
      const report_chat = new  ReportChatModel();
      report_chat.room_id = room_id;
      report_chat.product_id = product_id;
      report_chat.seller_id = seller_id;
      report_chat.user_id = user_id;
      report_chat.reported_by = 2;
      report_chat.reason = reason;
      report_chat.save().then((result)=>{
        return res.send({
          status:true,
          message:"Drapeau de rapport soumis",
        })
      }).catch((error)=>{
        return res.send({
          status:false,
          message:error.message,
        })
      });
    }catch(error)
    {
      return res.send({
        status:false,
        message:error.message
      })
    }
  },
  report_chat_by_user:async(req,res)=>{
    try{
      var {room_id,reason} = req.body;
      //console.log(req.body)
      const single_chat = await ProductChatModel.findOne({room_id:room_id});
      //console.log(single_chat);
      if(!single_chat)
      {
        return res.send({
          status:false,
          message:"Identifiant de salle invalide",
        })
      }
      let product_id = single_chat.product_id;
      let seller_id = single_chat.seller_id;
      let user_id = single_chat.user_id;
      var re =  await ProductChatModel.updateMany({room_id:room_id},{report_user:1});
      const report_chat = new  ReportChatModel();
      report_chat.room_id = room_id;
      report_chat.product_id = product_id;
      report_chat.seller_id = seller_id;
      report_chat.user_id = user_id;
      report_chat.reported_by = 1;
      report_chat.reason = reason;
      report_chat.save().then((result)=>{
        return res.send({
          status:true,
          message:"Drapeau de rapport soumis",
        })
      }).catch((error)=>{
        return res.send({
          status:false,
          message:error.message,
        })
      });
    }catch(error)
    {
      return res.send({
        status:false,
        message:error.message
      })
    }
  }
};
