const express =require('express');
const DeliveryController = require('./DeliveryController');
const router = express.Router();
const multer=require("multer");
const path=require("path");
const PRODUCT_PATH=process.env.PRODUCT_PATH;
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.resolve(PRODUCT_PATH))
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
  });
const uploadTo = multer({ storage: storage });
router.post('/createdelivery',uploadTo.fields([
    { 
      name: 'photo', 
      maxCount: 10
    }
  ]
  ),DeliveryController.createproduct);

module.exports=router