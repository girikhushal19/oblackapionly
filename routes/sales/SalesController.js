const sales = require("../../models/sales/Sales");
module.exports = {
    createSales: async (req, res) => {
        const {
            product_id,
            quantity,
            userid
        } = req.body;
        const oldsales=await sales.findOne({product_id:product_id});
        if(oldsales){
            oldsales.totalsales=oldsales.totalsales+quantity;
            oldsales.userIds=oldsales.userIds.concat([userid]);
            oldsales.save().then((result)=>{
                return res.send({
                    status:true,
                    message:"updated successfully"
                })
            })
        }else{
          const newSales=await sales();
          newSales.product_id=product_id;
          newSales.totalsales=quantity;
          newSales.userIds=userid;
          newSales.save().then((result)=>{
            return res.send({
                status:true,
                message:"inventory created successfully"
            })
          })
        }
        
    },
    deleteSales: async (req, res) => {
        const id = req.body.id;
        await sales.findByIdAndDelete(id)
            .then((result) => {
                return res.send({
                    status: true,
                    message: "deleted successfully"
                })
            });
    },
}