const RefundModel=require("../../models/admin/Refund");
const orderModel=require("../../models/orders/order")
module.exports={
    addOrupdate:async(req,res)=>{
      const {
        orderId,
        vendor,
        vendorId,
        amount,
        reason,
        paymentgateway,
        id,
        requestById,
        requestByName
      }=req.body;
      
      const order=await orderModel.findById(orderId);
      console.log("order",order)
      let products=order?.products||[]
      let productsArray=[]
      products.map((product)=>{
        productsArray.push(product.product._id)
      });
      const datatoupdate={
        ...(id&&{id:id}),
        ...(orderId&&{orderId:orderId}),
        ...(requestById&&{requestById:requestById}),
        ...(requestByName&&{requestByName:requestByName}),
        ...(vendor&&{vendor:vendor}),
        ...(vendorId&&{vendorId:vendorId}),
        ...(amount&&{amount:amount}),
        ...(reason&&{reason:reason}),
        ...(paymentgateway&&{paymentgateway:paymentgateway}),
        ...(productsArray.length&&{products:productsArray})
      }
      console.log("datatoupdate",datatoupdate)
       await RefundModel.addOrupdate(datatoupdate).then((result)=>{
        return res.send({
            status:true,
            message:"added/updated succesfully"
        })
       })
    },
    delete:async(req,res)=>{
    const id=req.params.id;
    await RefundModel.findByIdAndDelete(id).then((result)=>{
        return res.send({
            status:true,
            message:"deleted successfully"
        })
    })
    },
    markAsdone:async(req,res)=>{
        const id=req.params.id;
        await RefundModel.findByIdAndUpdate(id,{status:true}).then((result)=>{
            return res.send({
                status:true,
                message:"marked as done"
            })
        })
    },
    markAsUndone:async(req,res)=>{
        const id=req.params.id;
        await RefundModel.findByIdAndUpdate(id,{status:false}).then((result)=>{
            return res.send({
                status:true,
                message:"marked as undone"
            })
        })
    },
    markAsCancelled:async(req,res)=>{
        const id=req.params.id;
        await RefundModel.findByIdAndUpdate(id,{is_rejected:true}).then((result)=>{
            return res.send({
                status:true,
                message:"marked as cancelled"
            })
        })
    },
    getallrefunds:async(req,res)=>{
        const id=req.body.id;
        const approved=req.body.approved;
        const query={};
        const pending=req.body.pending;
        const cancelled=req.body.cancelled;
        if(approved){
            query["status"]=true
        }
        if(pending){
            query["status"]=false
        }
        if(cancelled){
            query["is_rejected"]=true
        }
        const pageno=req.body.pageno
        const limit=10;
        
        
        const skip=limit*pageno;
        if(pageno==null||pageno=="null"){
            const count=(await RefundModel.aggregate([
                {$match:query},
                {$addFields:{
                    orderIdIDOBJ:{"$toObjectId":"$orderId"},
                    userIDOBJ:{"$toObjectId":"$requestById"},
                    vendorIDOBJ:{"$toObjectId":"$vendorId"},
                }},
                {
                    $lookup:{
                        from:"orders",
                        localField:"orderIdIDOBJ",
                        foreignField:"_id",
                        as:"order"
                    }
                },
                {
                    $lookup:{
                        from:"users",
                        localField:"userIDOBJ",
                        foreignField:"_id",
                        as:"requestedBy"
                    }
                },
                {
                    $lookup:{
                        from:"sellers",
                        localField:"vendorIDOBJ",
                        foreignField:"_id",
                        as:"vendor"
                    }
                }
            ]))?.length
    
            const pages=Math.ceil(count/limit)
            return res.send({
                status:true,
                pages:pages
            })
        }
        const refunds=await RefundModel.aggregate([
            {$match:query},
            {$addFields:{
                orderIdIDOBJ:{"$toObjectId":"$orderId"},
                userIDOBJ:{"$toObjectId":"$requestById"},
                vendorIDOBJ:{"$toObjectId":"$vendorId"},
            }},
            {
                $lookup:{
                    from:"orders",
                    localField:"orderIdIDOBJ",
                    foreignField:"_id",
                    as:"order"
                }
            },
            {
                $lookup:{
                    from:"users",
                    localField:"userIDOBJ",
                    foreignField:"_id",
                    as:"requestedBy"
                }
            },
            {
                $lookup:{
                    from:"sellers",
                    localField:"vendorIDOBJ",
                    foreignField:"_id",
                    as:"vendor"
                }
            }
        ]);
        return res.send({
            status:true,
            message:"fetched",
            data:refunds
        })
    }
}