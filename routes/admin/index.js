const express =require('express');
const AbuseReportController = require('./AbuseReport.controller');
const RefundController = require('./Refund.controller');
const SellerRatingsController=require("./SellerRatings.controller");
const AnnouncementController=require("./Announcement.controller");
const GlobalSettingsController=require("./GlobalSettings.controller");
const UserAppMiscController=require("./UserAppMisc.controller");
const router = express.Router();
const multer=require("multer");
const path=require("path");
const user_path=process.env.USER_PATH;
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      console.log(user_path);
      cb(null, path.resolve("views/admin-panel/public/media"))
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
  });
const uploadTo = multer({ storage: storage });
router.post('/addOrupdate',uploadTo.fields([
    { 
      name: 'photo', 
      maxCount: 10
    }
  ]
  ),AbuseReportController.addOrupdate);
router.get('/deletereport/:id',AbuseReportController.delete)
router.get('/markAsresolved/:id',AbuseReportController.markAsresolved)
router.get('/markAsUnresolved/:id',AbuseReportController.markAsUnresolved)
router.post('/getallreports',AbuseReportController.getallreports)
router.post('/addOrupdatereason',AbuseReportController.addOrupdatereason)
router.get('/getallreasons',AbuseReportController.getallreasons)


//refund 
router.post('/addOrupdaterefund',uploadTo.fields([
    { 
      name: 'photo', 
      maxCount: 10
    }
  ]
  ),RefundController.addOrupdate);
router.get('/deleterefund/:id',RefundController.delete)
router.get('/markAsdone/:id',RefundController.markAsdone)
router.get('/markAsUndone/:id',RefundController.markAsUndone)
router.post('/getallrefunds',RefundController.getallrefunds)

//seller ratings
router.get('/addOrupdaterating',SellerRatingsController.addOrupdaterating)
router.get('/deletestorerating/:id',SellerRatingsController.deleterating)
router.get('/getallratingsbystoreid/:id',SellerRatingsController.getallratingsbystoreid)
router.get('/getallstoresratings',SellerRatingsController.getallstoresratings)

//announcements
router.post('/makenewannouncements',AnnouncementController.newannouncement);
router.get('/getallannouncements',AnnouncementController.getallannouncements);

//global settings
router.post("/updateoraddSettings",GlobalSettingsController.updateoraddSettings);
router.get("/getglobalsettings",GlobalSettingsController.getglobalsettings);

//banners
router.post("/addorupdatebannerimage",uploadTo.fields([
  { 
    name: 'image', 
    maxCount: 10
  }
]
),UserAppMiscController.addorupdatebannerimage);

router.post("/addMiddleBannerImageSubmit",uploadTo.fields([
  { 
    name: 'file', 
    maxCount: 1
  }
]
),UserAppMiscController.addMiddleBannerImageSubmit);


router.get("/getMiddlebannerimages",UserAppMiscController.getMiddlebannerimages); 

router.get("/getSinglebannerimage/:id",UserAppMiscController.getSinglebannerimage);
router.get("/getallbannerimage",UserAppMiscController.getallbannerimage); 
router.get("/deletebannerimage/:id",UserAppMiscController.deletebannerimage);
router.post("/addorupdatetopcatagory",UserAppMiscController.addorupdatetopcatagory);
router.get("/deletetopcatagory",UserAppMiscController.deletetopcatagory);
router.get("/gettopcatagories",UserAppMiscController.gettopcatagories);
router.post("/addorupdateFeatruedProductcatagory",UserAppMiscController.addorupdateFeatruedProductcatagory);
router.get("/deleteFeatruedProductcatagory",UserAppMiscController.deleteFeatruedProductcatagory);
router.get("/getFeatruedProductcatagory",UserAppMiscController.getFeatruedProductcatagory);
router.get("/deleteFeatruedProductcatagory/:id",UserAppMiscController.deleteFeatruedProductcatagory);
router.post("/getFeatruedProductcatagory",UserAppMiscController.getFeatruedProductcatagory);




module.exports=router