const bannerImageModel=require("../../models/admin/Banners");
const TopCatagoriesModel=require("../../models/admin/TopCatagories");
const FeatruedProductcatagories=require("../../models/admin/FeatruedProductcatagories");
const mongoose = require("mongoose");
const Catagorymodel = require("../../models/products/Catagory");
module.exports={
    addorupdatebannerimage:async(req,res)=>{
        const {
            id,
            url,
            title,
            text,
           
            location
        }=req.body;
        let image;
        if(req.files?.image?.length){
            image="media/"+req.files.image[0].filename;
            console.log(req.files?.image)
        }
        if(id){
            await bannerImageModel.findByIdAndUpdate(id,{
                ...({url:url}),
                ...(location&&{location:location}),
                ...(title&&{title:title}),
                ...(text&&{text:text}),
                ...(image&&{image:image})
            }).then((result)=>{
                return res.send({
                    status:true,
                    message:"L'enregistrement de la bannière a été mis à jour avec succès"
                })
            })
        }else{
            await bannerImageModel.create({
                ...(url&&{url:url}),
                ...(title&&{title:title}),
                ...(location&&{location:location}),
                ...(text&&{text:text}),
                ...(image&&{image:image})
            }).then((result)=>{
                return res.send({
                    status:true,
                    message:"Enregistrement de la bannière ajouté avec succès"
                })
            })
        }
    },
    getallbannerimage:async(req,res)=>{
        await bannerImageModel
        .find({})
        .then((result)=>{
            return res.send({
                status:true,
                data:result,
                message:"created success"
            })
        })
    },
    getSinglebannerimage:async(req,res)=>{
        await bannerImageModel
        .findOne({_id:req.params.id})
        .then((result)=>{
            return res.send({
                status:true,
                data:result,
                message:"created success"
            })
        })
    },
    
    getMiddlebannerimages:async(req,res)=>{
        await bannerImageModel
        .find({ location: 'user_home_middle_block' })
        .then((result)=>{
            return res.send({
                status:true,
                data:result,
                message:"Succès"
            })
        })
    },
    deletebannerimage:async(req,res)=>{
        const id=req.params.id;
        await bannerImageModel
        .findByIdAndDelete(id)
        .then((result)=>{
            return res.send({
                status:true,
                message:"deleted success"
            })
        })
    },

    addorupdatetopcatagory:async(req,res)=>{
        const {
            id,
            catagory,
            catagory_id
        }=req.body;
        if(id){
            await TopCatagoriesModel.findByIdAndUpdate(id,{
                ...(catagory&&{catagory:catagory}),
                ...(catagory_id&&{catagory_id:catagory_id}),
                
            }).then((result)=>{
                return res.send({
                    status:true,
                    message:"updated success"
                })
            })
        }else{
            await TopCatagoriesModel.create({
                ...(catagory&&{catagory:catagory}),
                ...(catagory_id&&{catagory_id:catagory_id})
            }).then((result)=>{
                return res.send({
                    status:true,
                    message:"created success"
                })
            })
        }
    },
    deletetopcatagory:async(req,res)=>{
        const id=req.params.id;
        await TopCatagoriesModel
        .findByIdAndDelete(id)
        .then((result)=>{
            return res.send({
                status:true,
                message:"deleted success"
            })
        })
    },
    gettopcatagories:async(req,res)=>{
        await TopCatagoriesModel
        .find({})
        .then((result)=>{
            return res.send({
                status:true,
                data:result,
                message:"created success"
            })
        })
    },

    addorupdateFeatruedProductcatagory:async(req,res)=>{
        const {
            id,
            catagory,
            catagory_id
        }=req.body;
        if(id){
            await FeatruedProductcatagories.findByIdAndUpdate(id,{
                ...(catagory&&{catagory:catagory}),
                ...(catagory_id&&{catagory_id:catagory_id}),
                
            }).then((result)=>{
                return res.send({
                    status:true,
                    message:"updated success"
                })
            })
        }else{
            await FeatruedProductcatagories.create({
                ...(catagory&&{catagory:catagory}),
                ...(catagory_id&&{catagory_id:catagory_id})
            }).then((result)=>{
                return res.send({
                    status:true,
                    message:"created success"
                })
            })
        }
    },
    deleteFeatruedProductcatagory:async(req,res)=>{
        const id=req.params.id;
        await FeatruedProductcatagories
        .findByIdAndDelete(id)
        .then((result)=>{
            return res.send({
                status:true,
                message:"deleted success"
            })
        })
    },
    getFeatruedProductcatagory:async(req,res)=>{
        //console.log("this is calling 164");
        //mongoose.set("debug",true);
        try{
            var user_id = req.body.user_id;
            
            
            await Catagorymodel
            .aggregate([
                { $match: { "is_featured":true  } },
               
                {
                    $lookup: {
                        from: "products",
                        let:{"prod_id":{"$toString":"$_id"}},
                        pipeline: [
                            { 
                                "$match":{ 
                                    "$expr":{ 
                                        "$in": [ "$$prod_id", "$category_id"] 
                                    } 
                                } 
                            },
                            { 
                                "$match":{ 
                                    "$expr":{ 
                                        $and: [ 
                                            { $eq: ['$is_deleted', false] },
                                            { $eq: ['$is_active', true] }
                                          ]
                                    } 
                                } 
                            },
                            {
                                $addFields:{
                                    "p_id":"$_id"
                                }
                            },
                            {
                                $lookup: {
                                    from: "ratingnreviews",
                                    let: { "pro_id": { "$toString": "$p_id" } },
                                    pipeline: [
                                        {
                                            $match:{
                                                $expr:{
                                                    $eq:["$product_id","$$pro_id"]
                                                }
                                            }
                                        }
                                        //{ $addFields: { "userIDOBJ": { "$toObjectId": "$user_id" } } },
                                        // {
                                        //     $lookup: {
                                        //         from: "users",
                                        //         localField: "userIDOBJ",
                                        //         foreignField: "_id",
                                        //         as: "userwhorated"
                                        //     }
                                        // }
                                    ],
                                    as: "reviews"
                                }
                            }, 
                            {
                                $project:{
                                    "reviews.media":0,
                                    "reviews.replies":0,
                                    "reviews.review":0,
                                    "reviews.createdAt":0,
                                    "reviews.updatedAt":0,
                                    "reviews.__v":0

                                }
                            },
                            {
                                $addFields: {
                                    avgRating:{
                                        $avg: "$reviews.rating"
                                    },
                                    provideridObj: {
                                        "$toObjectId": "$provider_id"
                                    }
                                }
                            },
                            {
                                $lookup:{
                                    from:"subscriptions",
                                    let:{"slr_ID":{"$toString":"$provideridObj"} },
                                    pipeline:[
                                        {
                                            $match:{
                                                $expr:{
                                                    $and:[
                                                        {$eq:["$provider_id","$$slr_ID"]},
                                                        {$eq:["$payment_status", true]}
                                                    ]
                                                    
                                                    
                                                }
                                            }
                                        },
                                        {
                                            $sort:{
                                                "date_of_transaction":-1
                                            }
                                        },
                                        {
                                            $limit:1
                                        }
                                    ],
                                    as:"slr_rec"
                                }
                            },
                            {
                                $addFields: {
                                    "vendor_type":"$slr_rec.name_of_package"
                                }
                            },
                            {
                                $project:{
                                    "slr_rec":0
                                }
                            },
                            // {
                            //     $lookup:{
                            //         from:"productfavorites",
                            //         let:{"pro_id":{"$toString":"$_id"}},
                            //         pipeline:[
                            //              {
                            //                 $match:{
                            //                     $expr:{
                            //                         $and:[
                            //                             {$eq:["$product_id","$$pro_id"]},
                            //                             {$eq:["$user_id","$user_id"]}
                            //                         ]
                            //                     }
                            //                 }
                            //              }
                            //         ],
                            //         as:"favProduct"
                            //     }
                            // },
                            {
                                $addFields:{
                                    "usr_id":user_id
                                }
                            },
                            {
                                $lookup:{
                                    from:"productfavorites",
                                    let:{"pro_id":{"$toString":"$_id"}},
                                    pipeline:[
                                        {
                                            $match:{
                                                $expr:{
                                                    $and:[
                                                        {$eq:["$user_id",user_id]},
                                                        {$eq:["$product_id","$$pro_id"]}
                                                    ]
                                                }
                                            }
                                        }
                                    ],
                                    as:"favProduct"
                                }
                            },

                            {
                                $addFields:{
                                    "isFav":{$size:"$favProduct"}
                                }
                            },
                            // {
                            //     $project:{
                            //         "reviews.userwhorated":0
                            //     }
                            // },
                            // {
                            //     $project:{
                            //         "favProduct":0
                            //     }
                            // }
                        ],
                        as:"products"
                    }
                    
                }
            ])
            .then((result) => {
                return res.send({
                    status: true,
                    data: result,
                    message: "Succès"
                })
            }).catch((error)=>{
                return res.send({
                    status: false,
                    data: [],
                    message: error.message
                })
            });
        }catch(error)
        {
            return res.send({
                status: false,
                data: [],
                message: error.message
            })
        }
        
    },
    addMiddleBannerImageSubmit:async(req,res)=>{
        try{
            console.log(req.body);
            console.log(req.files);
            if(!req.files)
            {
                return res.send({
                    error:true,
                    errorMessage:"L'image est requise"
                })
            }
            if(req.files.file.length == 0)
            {
                return res.send({
                    error:true,
                    errorMessage:"L'image est requise"
                })
            }
            //req.files.file[0].filename
        }catch(error)
        {
            return res.send({
                error:true,
                errorMessage:error.message
            })
        }
    },
}
