const express =require('express');
const ProductController = require('./ProductController');
const ProductSubController = require('./ProductSubController');
const router = express.Router();
const multer=require("multer");
const path=require("path");
const SellerController = require('../seller/SellerController');
const PRODUCT_PATH=process.env.PHOTOS_PATH;
const storage = multer.diskStorage({
  
    destination: function (req, file, cb) {
      cb(null, path.resolve(PRODUCT_PATH))
    },
    filename: function (req, file, cb) {
      cb(null, Date.now()+"_"+Array.from(Array(6), () => Math.floor(Math.random() * 36).toString(36)).join('')+ path.extname(file.originalname)) //Appending extension
    }
  });
const uploadTo = multer({ storage: storage });
router.post('/createproduct',uploadTo.fields([
    {
      name: 'photo', 
      maxCount: 9
    },
    {
      name: 'video', 
      maxCount: 1
    }
  ]
  ),ProductController.createproduct);

  router.post('/createproductTest',uploadTo.fields([
    {
      name: 'photo', 
      maxCount: 9
    },
    {
      name: 'video', 
      maxCount: 1
    }
  ]
  ),ProductController.createproductTest);

  
  router.post('/createAttribute',uploadTo.fields([
    
    {
      name: 'sub_attr_image_1', 
      maxCount: 6
    },
    {
      name: 'sub_attr_image_2', 
      maxCount: 6
    },
    {
      name: 'sub_attr_image_3', 
      maxCount: 6
    },
    {
      name: 'sub_attr_image_4', 
      maxCount: 6
    },
    {
      name: 'sub_attr_image_5', 
      maxCount: 6
    },
    {
      name: 'sub_attr_image_6', 
      maxCount: 6
    },
    {
      name: 'sub_attr_image_7', 
      maxCount: 6
    },
    {
      name: 'sub_attr_image_8', 
      maxCount: 6
    },
    {
      name: 'sub_attr_image_9', 
      maxCount: 6
    },
    {
      name: 'sub_attr_image_10', 
      maxCount: 6
    }
  ]
  ),ProductController.createAttribute);
  
  router.post('/tempImageUpload',uploadTo.fields([
    { 
      name: 'photo', 
      maxCount: 10
    }
  ]
  ),ProductController.tempImageUpload);

  router.post('/editVariation',ProductController.editVariation);
  
router.get('/deleteproduct/:id',ProductController.deleteproduct);
router.post('/getallproducts',ProductController.getallproductsforuser);
router.post('/listProducts',ProductSubController.listProducts);
router.post('/listProductsTotalCount',ProductSubController.listProductsTotalCount);

router.post('/autoSuggestionProductList',ProductSubController.autoSuggestionProductList);
router.post('/getrecommendedproducts',ProductController.getrecommendedproducts);
router.post('/getallproductsforadmin',ProductController.getallproductsforadmin);
router.get('/getproductbyid/:id',ProductController.getproductbyid);

router.get('/getproductbyidtoedit/:id',ProductController.getproductbyidtoedit);
router.get("/getallproductjustforfilter",ProductController.getallproductjustforfilter);
router.get("/getSingleCatEditTime/:id",ProductController.getSingleCatEditTime);

router.post("/getAutoSuggestionProductList",ProductController.getAutoSuggestionProductList);
router.post("/addFavProduct",ProductSubController.addFavProduct);
router.get("/getFavProduct/:id",ProductSubController.getFavProduct);
router.get("/deleteFavProduct/:id",ProductSubController.deleteFavProduct);
router.get("/getAllPromoCode",ProductSubController.getAllPromoCode);

router.post("/saveUserViewProduct",ProductSubController.saveUserViewProduct);
router.get("/getUserHistoryProduct/:user_id",ProductSubController.getUserHistoryProduct);

router.post("/deleteProImg",ProductSubController.deleteProImg);
router.post("/getVariationPriceStock",ProductSubController.getVariationPriceStock);
router.post("/groupRating",ProductSubController.groupRating);


router.post('/savecatagory',uploadTo.fields([
  { 
    name: 'image', 
    maxCount: 10
  },
]
),ProductController.savecatagoryonline);
router.get('/getOnlyFeatureCategory',ProductController.getOnlyFeatureCategory);
router.get('/getOnlyParentCategory',ProductController.getOnlyParentCategory);
router.get('/getOnlyChildCategory/:id',ProductController.getOnlyChildCategory);
router.get('/getAllParentCategory/:id',ProductController.getAllParentCategory);

router.post("/updateCatStatus",ProductController.updateCatStatus);

router.get('/getallcatagories',ProductController.getallcatagories)
router.get('/getallcatagoriesflat',ProductController.getallcatagoriesflat);
router.get('/getAllCatagoriesCount',ProductController.getAllCatagoriesCount);
router.post('/getAllCatagoriesPagi',ProductController.getAllCatagoriesPagi);
router.post('/makeFeatureApi',ProductController.makeFeatureApi);

router.get('/getallcatagoriesFilter',ProductController.getallcatagoriesFilter);
router.get('/makeProductasactive/:id',ProductController.makeProductasactive)
router.get('/makeProductasInactive/:id',ProductController.makeProductasInactive)
router.post('/ProductfilterwithAttributes',ProductController.ProductfilterwithAttributes)

router.post('/webSubmitAttribute',uploadTo.fields([
  { name: 'image0', maxCount: 6 },{ name:'image1', maxCount: 6 },
  { name: 'image2', maxCount: 6 },{ name:'image3', maxCount: 6 },
  { name: 'image4', maxCount: 6 },{ name:'image5', maxCount: 6 },
  { name: 'image6', maxCount: 6 },{ name:'image7', maxCount: 6 },
  { name: 'image8', maxCount: 6 },{ name:'image9', maxCount: 6 },
  { name: 'image10', maxCount: 6 },{ name:'image11', maxCount: 6 },
  { name: 'image12', maxCount: 6 },{ name:'image13', maxCount: 6 },
  { name: 'image14', maxCount: 6 },{ name:'image15', maxCount: 6 },
  { name: 'image16', maxCount: 6 },{ name:'image17', maxCount: 6 },
  { name: 'image18', maxCount: 6 },{ name:'image19', maxCount: 6 }
]
),ProductSubController.webSubmitAttribute);


router.post('/deleteAttributeIndex',ProductSubController.deleteAttributeIndex);
router.post('/deleteAttributeIndexImage',ProductSubController.deleteAttributeIndexImage);

router.get('/getSingleVariation/:id',ProductSubController.getSingleVariation);

router.get('/get2level_category',ProductController.get2level_category)

//matcat
router.get('/getparentcatagoriesonly',ProductController.getparentcatagoriesonly)
router.post('/getchildcatagories',ProductController.getchildcatagories)
router.get('/getallmatcatagories',ProductController.getallmatcatagories)
router.get('/deletematcatagory/:id',ProductController.deletematcatagory)
router.post('/updatematcatagory',uploadTo.fields([
  { 
    name: 'image', 
    maxCount: 10
  },
]
),ProductController.updatematcatagory)
router.post('/savematcatagory',uploadTo.fields([
  { 
    name: 'image', 
    maxCount: 10
  },
]
),ProductController.savematcatagory);
// router.post('/savevariationpricetoproduct',uploadTo.fields([
//   { 
//     name: 'image', 
//     maxCount: 10
//   },
// ]
// ),ProductController.addVariationsPrice);
router.post('/getallproductsforseller',ProductController.getallproductsforseller)
router.post('/markProductasViolated',ProductController.markProductasViolated)

//variations
router.post('/addVariations',uploadTo.fields([
  { 
    name: 'images', 
    maxCount: 10
  }
  
]
),ProductController.addVariations)
router.post('/updateVariations',ProductController.updateVariations)
router.post('/deleteVariation',ProductController.deleteVariations)
router.post('/deleteVariationoption',ProductController.deleteVariationoption)
router.get('/getVariations/:id',ProductController.getVariations)


// variation price
router.post('/addVariationsPrice',ProductController.addVariationsPrice)
router.post('/updateVariationsPrice',ProductController.updateVariationsPrice)
router.get('/deleteVariationsPrice/:id',ProductController.deleteVariationsPrice)
router.get('/getVariationsPrice/:id',ProductController.getVariationsPrice)
router.post('/getPriceofVariations',ProductController.getPriceofVariations);
router.post('/updateAdViewCount',ProductController.updateAdViewCount);


//page views
router.post('/addPageView',ProductController.addPageView)
router.get('/getPageViews/:id',ProductController.getPageViews)
router.get('/getMaximumReturnDay',ProductSubController.getMaximumReturnDay);

router.get("/getSellerNameForFilter",SellerController.getSellerNameForFilter);

router.get('/restoreproduct/:id',ProductController.restoreproduct);


module.exports=router