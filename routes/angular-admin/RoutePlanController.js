const Seller = require("../../models/seller/Seller");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const async = require("async");
const base_url = process.env.BASE_URL;
const path = require("path");
const AdminModel = require("../../models/admin/AdminModel");
const DriversModel = require("../../models/admin/DriversModel");
const RoutePlansModel = require("../../models/admin/RoutePlansModel");
const StopsModel = require("../../models/admin/StopsModel");
const DriverNotificationModel = require("../../models/admin/DriverNotificationModel");
const fs = require('fs');

const mongoose = require("mongoose");
const { parse } = require("path");
//var bcrypt = require('bcryptjs');
//const jwt = require("jsonwebtoken");
var FCM = require('fcm-node');
var serverKey = process.env.FCM_SERVER_KEY; //put your server key here
var fcm = new FCM(serverKey); 

module.exports = {


  addRouteSubmit: async function(req, res,next)
  {
    try{
      //console.log("req.body ", req.body);return false;
      var {fullName,notes,email,mobileNumber,priority,serviceTime,address,businessName,stopType,parcelFront,parcelLeft,parcelFloor,parcelCount,latitude,longitude,route_id,shipment_id,orderType,user_order_id,stopType2,user_order_id_arr} = req.body;
      if(!stopType)
      {
        if(stopType2)
        {
          stopType = stopType2;
        }
      }
      

      // console.log("stopType ", stopType);
      // return false;
      if(user_order_id_arr)
      {
        if(user_order_id_arr.length == 0)
        {
          user_order_id_arr = [];
        }
      }else{
        user_order_id_arr = [];
      }
      if(shipment_id == '')
      {
        shipment_id = null
      }
      var todayDate = new Date();
      if(latitude == "" || latitude == null || longitude == "" || longitude == null)
      {
        var return_response = {"error":true,success: false,errorMessage:"l'adresse de départ doit être une adresse google"};
        res.status(200)
        .send(return_response);
      }else{

        latitude = parseFloat(latitude);
        longitude = parseFloat(longitude);
        //routeName,totalStop
        if(route_id == "" || route_id == null || route_id == undefined || route_id == 'undefined')
        {
          RoutePlansModel.countDocuments({ }).exec((errCount, totalcount)=>
          {
            if(errCount)
            {
              var return_response = {"error":true,success: false,errorMessage:errCount};
                res.status(200)
                .send(return_response);
            }else{
              var newRouteName = totalcount + 1;
              var routeName = "Route plan -"+newRouteName;
              RoutePlansModel.create({
                routeDate:todayDate,
                routeName:routeName,
                totalStop:1
              },function(err,result){
                if(err)
                {
                  //console.log(err);
                  var return_response = {"error":true,success: false,errorMessage:err};
                    res.status(200)
                    .send(return_response);
                }else{
                  var last_insertId = result._id;
                  StopsModel.create({
                    orderType:orderType,
                    user_order_id:user_order_id,
                    user_order_id_arr:user_order_id_arr,
                    shipment_id:shipment_id,
                    route_id:last_insertId,
                    fullName:fullName,
                    notes:notes,
                    email:email,
                    mobileNumber:mobileNumber,
                    priority:priority,
                    serviceTime:serviceTime,
                    businessName:businessName,
                    stopType:stopType,
                    parcelFront:parcelFront,
                    parcelLeft:parcelLeft,
                    parcelFloor:parcelFloor,
                    parcelCount:parcelCount,
                    address:address,
                    location: {
                      type: "Point",
                      coordinates: [latitude,longitude]
                    },
                  },function(err2,result2){
                    if(err2)
                    {
                      //console.log(err);
                      var return_response = {"error":true,success: false,errorMessage:err2};
                        res.status(200)
                        .send(return_response);
                    }else{
                      var return_response = {"error":false,success: true,errorMessage:"Arrêter de créer le succès pleinement",lastInsertId:last_insertId};
                        res.status(200)
                        .send(return_response);
                    }
                  });
                }
              });
            }
          });
        }else{
          console.log("hereee");
          var last_insertId = route_id;
          StopsModel.create({

            orderType:orderType,
            user_order_id:user_order_id,
            user_order_id_arr:user_order_id_arr,
            shipment_id:shipment_id,
            route_id:last_insertId,
            fullName:fullName,
            notes:notes,
            email:email,
            mobileNumber:mobileNumber,
            priority:priority,
            serviceTime:serviceTime,
            businessName:businessName,
            stopType:stopType,
            parcelFront:parcelFront,
            parcelLeft:parcelLeft,
            parcelFloor:parcelFloor,
            parcelCount:parcelCount,
            address:address,
            location: {
              type: "Point",
              coordinates: [latitude,longitude]
            },
          },function(err2,result2){
            if(err2)
            {
              //console.log(err);
              var return_response = {"error":true,success: false,errorMessage:err2};
                res.status(200)
                .send(return_response);
            }else{
              StopsModel.countDocuments({ route_id:last_insertId,routeType: 'stop' }).exec((errCount, totalcount)=>
              {
                if(errCount)
                {
                  var return_response = {"error":true,success: false,errorMessage:errCount};
                    res.status(200)
                    .send(return_response);
                }else{
                  RoutePlansModel.updateOne({ "_id":last_insertId }, 
                  {totalStop:totalcount}, function (uerr, docs) {
                  if (uerr){
                    //console.log(uerr);
                    res.status(200)
                      .send({
                          error: true,
                          success: false,
                          errorMessage: uerr
                      });
                  }else{
                      //console.log("here");return false;
                      var return_response = {"error":false,success: true,errorMessage:"Arrêter de créer le succès pleinement",lastInsertId:last_insertId};
                      res.status(200)
                      .send(return_response);
                    }
                  });
                }
              });
              
            }
          });
        }
      }
    }catch(error){
      console.log(error);
      var return_response = {"error":true,errorMessage:error };
      res.status(200).send(return_response);
    }
  },
  addRouteFileSubmit: async function(req, res,next)
  {
    try{
      // console.log(req.body);
      // console.log(req.files.file);
      // return false;

      var userImage = null;
      if(req.files)
      {
        if(req.files)
        {
          var p_r_i = req.files.file;
          for(let m=0; m<p_r_i.length; m++)
          {
            //let images_name_obj = { "filename":req.files[m].filename,"path":req.files[m].path  };
            //images.push(images_name_obj);
            userImage = req.files.file[m].filename;
          }
          //console.log("images");
          //console.log(userImage);
        }else{
          userImage = null;
        }
      }else{
        userImage = null;
      }
      //console.log("userImage"+userImage);return false;
      var {fullName,notes,email,mobileNumber,priority,serviceTime,address,businessName,stopType,parcelFront,parcelLeft,parcelFloor,parcelCount,latitude,longitude,route_id,shipment_id} = req.body;
      if( shipment_id == "" || shipment_id == null )
      {
          shipment_id = [];
      }
      var todayDate = new Date();
      if(latitude == "" || latitude == null || longitude == "" || longitude == null)
      {
        var return_response = {"error":true,success: false,errorMessage:"l'adresse de départ doit être une adresse google"};
        res.status(200)
        .send(return_response);
      }else{
        latitude = parseFloat(latitude);
        longitude = parseFloat(longitude);
        //routeName,totalStop
        if(route_id == "" || route_id == null || route_id == undefined || route_id == 'undefined')
        {
          RoutePlansModel.countDocuments({ }).exec((errCount, totalcount)=>
          {
            if(errCount)
            {
              var return_response = {"error":true,success: false,errorMessage:errCount};
                res.status(200)
                .send(return_response);
            }else{
              var newRouteName = totalcount + 1;
              var routeName = "Route plan -"+newRouteName;
              RoutePlansModel.create({
                routeDate:todayDate,
                routeName:routeName,
                totalStop:1
              },function(err,result){
                if(err)
                {
                  //console.log(err);
                  var return_response = {"error":true,success: false,errorMessage:err};
                    res.status(200)
                    .send(return_response);
                }else{
                  //console.log(result);
                  //console.log(result._id);
                  var last_insertId = result._id;
                  StopsModel.create({
                    shipment_id:shipment_id,
                    route_id:last_insertId,
                    fullName:fullName,
                    notes:notes,
                    email:email,
                    mobileNumber:mobileNumber,
                    priority:priority,
                    serviceTime:serviceTime,
                    businessName:businessName,
                    stopType:stopType,
                    parcelFront:parcelFront,
                    parcelLeft:parcelLeft,
                    parcelFloor:parcelFloor,
                    parcelCount:parcelCount,
                    address:address,
                    location: {
                      type: "Point",
                      coordinates: [latitude,longitude]
                    },
                    image:userImage,
                  },function(err2,result2){
                    if(err2)
                    {
                      //console.log(err2);
                      var return_response = {"error":true,success: false,errorMessage:err2};
                        res.status(200)
                        .send(return_response);
                    }else{
                      var return_response = {"error":false,success: true,errorMessage:"Arrêter de créer le succès pleinement",lastInsertId:last_insertId};
                        res.status(200)
                        .send(return_response);
                    }
                  });
                }
              });
            }
          });
        }else{
          console.log("hereee");
          var last_insertId = route_id;
          StopsModel.create({
            shipment_id:shipment_id,
            route_id:last_insertId,
            fullName:fullName,
            notes:notes,
            email:email,
            mobileNumber:mobileNumber,
            priority:priority,
            serviceTime:serviceTime,
            businessName:businessName,
            stopType:stopType,
            parcelFront:parcelFront,
            parcelLeft:parcelLeft,
            parcelFloor:parcelFloor,
            parcelCount:parcelCount,
            address:address,
            location: {
              type: "Point",
              coordinates: [latitude,longitude]
            },
            image:userImage,
          },function(err2,result2){
            if(err2)
            {
              //console.log(err2);
              var return_response = {"error":true,success: false,errorMessage:err2};
                res.status(200)
                .send(return_response);
            }else{
              StopsModel.countDocuments({ route_id:last_insertId,routeType: 'stop' }).exec((errCount, totalcount)=>
              {
                if(errCount)
                {
                  var return_response = {"error":true,success: false,errorMessage:errCount};
                    res.status(200)
                    .send(return_response);
                }else{
                  RoutePlansModel.updateOne({ "_id":last_insertId }, 
                  {totalStop:totalcount}, function (uerr, docs) {
                  if (uerr){
                    //console.log(uerr);
                    res.status(200)
                      .send({
                          error: true,
                          success: false,
                          errorMessage: uerr
                      });
                  }else{
                      //console.log("here");return false;
                      var return_response = {"error":false,success: true,errorMessage:"Arrêter de créer le succès pleinement",lastInsertId:last_insertId};
                      res.status(200)
                      .send(return_response);
                    }
                  });
                }
              });
            }
          });
        }
      }
    }catch(error){
      console.log(error);
      var return_response = {"error":true,errorMessage:error };
      res.status(200).send(return_response);
    }
  },

  editRouteSubmit: async function(req, res,next)
  {
    try{
      //console.log(req.body);
      var {fullName,notes,email,mobileNumber,priority,serviceTime,address,businessName,stopType,parcelFront,parcelLeft,parcelFloor,parcelCount,latitude,longitude,route_id} = req.body;
      if(latitude == "" || latitude == null || longitude == "" || longitude == null)
      {
        var return_response = {"error":true,success: false,errorMessage:"l'adresse de départ doit être une adresse google"};
        res.status(200)
        .send(return_response);
      }else{
        StopsModel.findOne( {_id:route_id}, {}).exec((errRoute, resultRoute)=>
        {
          if(errRoute)
          {
            //console.log(errRoute);
            var return_response = {"error":true,errorMessage:errRoute};
              res.status(200).send(return_response);
          }else{
            if(stopType == "" || stopType == null)
            {
              stopType = resultRoute.stopType;
            }
            if(parcelFront == "" || parcelFront == null)
            {
              parcelFront = resultRoute.parcelFront;
            }
            if(parcelLeft == "" || parcelLeft == null)
            {
              parcelLeft = resultRoute.parcelLeft;
            }
            if(parcelFloor == "" || parcelFloor == null)
            {
              parcelFloor = resultRoute.parcelFloor;
            }
            latitude = parseFloat(latitude);
            longitude = parseFloat(longitude);
            //routeName,totalStop
            var last_insertId = route_id;
            StopsModel.findByIdAndUpdate({ _id: route_id },{
              fullName:fullName,
              notes:notes,
              email:email,
              mobileNumber:mobileNumber,
              priority:priority,
              serviceTime:serviceTime,
              businessName:businessName,
              stopType:stopType,
              parcelFront:parcelFront,
              parcelLeft:parcelLeft,
              parcelFloor:parcelFloor,
              parcelCount:parcelCount,
              address:address,
              location: {
                type: "Point",
                coordinates: [latitude,longitude]
              },
            },function(err2,result2){
              if(err2)
              {
                //console.log(err);
                var return_response = {"error":true,success: false,errorMessage:err2};
                  res.status(200)
                  .send(return_response);
              }else{
                var return_response = {"error":false,success: true,errorMessage:"Arrêter de créer le succès pleinement",lastInsertId:resultRoute.route_id};
                        res.status(200)
                        .send(return_response);
                
              }
            });
          }
        });
        
      }
    }catch(error){
      console.log(error);
      var return_response = {"error":true,errorMessage:error };
      res.status(200).send(return_response);
    }
  },
  editRouteFileSubmit: async function(req, res,next)
  {
    try{
      //console.log(req.body);
      //console.log(req.files.file);
      var userImage = null;
      
      //console.log("userImage"+userImage);return false;
      var {fullName,notes,email,mobileNumber,priority,serviceTime,address,businessName,stopType,parcelFront,parcelLeft,parcelFloor,parcelCount,latitude,longitude,route_id} = req.body;
      if(latitude == "" || latitude == null || longitude == "" || longitude == null)
      {
        var return_response = {"error":true,success: false,errorMessage:"l'adresse de départ doit être une adresse google"};
        res.status(200)
        .send(return_response);
      }else{
        StopsModel.findOne( {_id:route_id}, {}).exec((errRoute, resultRoute)=>
        {
          if(errRoute)
          {
            //console.log(errRoute);
            var return_response = {"error":true,errorMessage:errRoute};
              res.status(200).send(return_response);
          }else{
            if(req.files)
            {
              if(req.files)
              {
                var p_r_i = req.files.file;
                for(let m=0; m<p_r_i.length; m++)
                {
                  //let images_name_obj = { "filename":req.files[m].filename,"path":req.files[m].path  };
                  //images.push(images_name_obj);
                  userImage = req.files.file[m].filename;
                }

                var oldFileName = resultRoute.image;
                var uploadDir = './public/uploads/routePlan/';
                let fileNameWithPath = uploadDir + oldFileName;
                //console.log(fileNameWithPath);
                //console.log(userImage);
                if (fs.existsSync(fileNameWithPath))
                {
                  fs.unlink(uploadDir + oldFileName, (err) => 
                  {
                    console.log("unlink file error "+err);
                  });
                }

                //console.log("images");
                //console.log(userImage);
              }else{
                userImage = resultRoute.image;
              }
            }else{
              userImage = resultRoute.image;
            }

              if(stopType == "" || stopType == null)
              {
                stopType = resultRoute.stopType;
              }
              if(parcelFront == "" || parcelFront == null)
              {
                parcelFront = resultRoute.parcelFront;
              }
              if(parcelLeft == "" || parcelLeft == null)
              {
                parcelLeft = resultRoute.parcelLeft;
              }
              if(parcelFloor == "" || parcelFloor == null)
              {
                parcelFloor = resultRoute.parcelFloor;
              }
              if(mobileNumber == "" || mobileNumber == 'null')
              {
                mobileNumber = null;
              }
              console.log("mobileNumber "+mobileNumber);
              latitude = parseFloat(latitude);
              longitude = parseFloat(longitude);
              //routeName,totalStop
              var last_insertId = route_id;
              StopsModel.findByIdAndUpdate({ _id: route_id },{
              fullName:fullName,
              notes:notes,
              email:email,
              mobileNumber:mobileNumber,
              priority:priority,
              serviceTime:serviceTime,
              businessName:businessName,
              stopType:stopType,
              parcelFront:parcelFront,
              parcelLeft:parcelLeft,
              parcelFloor:parcelFloor,
              parcelCount:parcelCount,
              address:address,
              location: {
                type: "Point",
                coordinates: [latitude,longitude]
              },
              image:userImage,
            },function(err2,result2){
              if(err2)
              {
                console.log(err2);
                var return_response = {"error":true,success: false,errorMessage:err2};
                  res.status(200)
                  .send(return_response);
              }else{
                //console.log("here");return false;
                var return_response = {"error":false,success: true,errorMessage:"Arrêter de créer le succès pleinement",lastInsertId:resultRoute.route_id};
                res.status(200)
                .send(return_response);
              }
            });
          }
        });
        
        
      }
    }catch(error){
      console.log(error);
      var return_response = {"error":true,errorMessage:error };
      res.status(200).send(return_response);
    }
  },
  allRoutePlan:async function(req,res,next)
	{
		var { fullName,lastName,mobileNumber,email,city,status } = req.body;
  	/*console.log(fullName);
  	console.log(lastName);
  	console.log(email);*/

	  	var String_qr = {}; 
	    //String_qr['fullName'] = fullName;
	    if(fullName && fullName != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['fullName'] = { $regex: '.*' + fullName + '.*' };
	    }
	    if(lastName && lastName != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['lastName'] = { $regex: '.*' + lastName + '.*' };
	    }
	    if(email && email != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['email'] = { $regex: '.*' + email + '.*' };
	    }
	    if(mobileNumber && mobileNumber != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['mobileNumber'] = { $regex: '.*' + mobileNumber + '.*' };
	    }
	    if(city && city != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['city'] = { $regex: '.*' + city + '.*' };
	    }
	    if(status && status != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['status'] = status;
	    }
      String_qr['deleted_at'] = 0;
       
	  	//console.log(String_qr);
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
	    	//mongoose.set('debug', true);
        const queryJoin = [
            {
              path :"driver_id",
              select:['fullName','mobileNumber']
            }
          ];
	    	
	      RoutePlansModel.find( String_qr, {}).populate(queryJoin).skip(fromindex).limit(perPageRecord).sort({"created_at":-1}).lean(true).exec((err, result)=>
	      {
	        if(err)
	        {
	          console.log(err);
	        }else{
	          //console.log("result ");
	          //console.log(typeof result);
	          //console.log("result"+result);
	          if(result.length > 0)
	          {
	             var return_response = {"error":false,errorMessage:"Succès","record":result};
	                  res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	      });

	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
  getRouteAssignCheck:async function(req,res,next)
  {
    try{
      var { route_id } = req.body;
      /*console.log(fullName);
      console.log(lastName);
      console.log(email);*/
      if(route_id != "" && route_id != undefined  && route_id != 'undefined')
      {
        RoutePlansModel.findOne( {_id:route_id}, {driver_id:1}).exec((err, result)=>
        {
          if(err)
          {
            console.log(err);
          }else{
            //console.log("result ");
            //console.log(typeof result);
            //console.log("result"+result);
            if(result)
            {
               var return_response = {"error":false,errorMessage:"Succès","record":result};
                    res.status(200).send(return_response);
            }else{
              var return_response = {"error":true,errorMessage:"Pas d'enregistrement","record":result};
              res.status(200).send(return_response);
            }

          }
        });
      }else{
        var return_response = {"error":true,errorMessage:"Pas d'enregistrement"};
              res.status(200).send(return_response);
      }
      
    }catch(err){
      console.log(err);
      var return_response = {"error":true,errorMessage:err };
      res.status(200).send(return_response);
    }
  },
  allRoutePlanCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);
	    try{
        RoutePlansModel.countDocuments({ deleted_at:0 }).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
  getStopListWithDriver:async function(req,res,next)
  {
    try{
      //console.log(req.body);
      var finalArray = [];
      var {driver_id,route_id} = req.body;
      var resultDriverObj = {};
      DriversModel.findOne( {_id:driver_id}, {}).exec((errDriver, resultDriver)=>
      {
        if(errDriver)
        {
          //console.log(errDriver);
          var return_response = {"error":true,errorMessage:errDriver};
            res.status(200).send(return_response);
        }else{
          resultDriverObj = resultDriver;
          StopsModel.find( {route_id:route_id,routeType: 'start' }, {}).exec((errRoute, resultRoute)=>
          {
            if(errRoute)
            {
              //console.log(errRoute);
              var return_response = {"error":true,errorMessage:errRoute};
                res.status(200).send(return_response);
            }else{
              finalArray.push(resultRoute[0]);
              StopsModel.find( {route_id:route_id,routeType: 'end' }, {}).exec((errRoute, resultRoute)=>
              {
                if(errRoute)
                {
                  //console.log(errRoute);
                  var return_response = {"error":true,errorMessage:errRoute};
                    res.status(200).send(return_response);
                }else{
                  finalArray.push(resultRoute[0]);
                  StopsModel.find( {route_id:route_id,routeType:{$nin : ["start", "end"]} }, {}).exec((errRoute, resultRoute)=>
                  {
                    if(errRoute)
                    {
                      //console.log(errRoute);
                      var return_response = {"error":true,errorMessage:errRoute};
                        res.status(200).send(return_response);
                    }else{
                      var lessVal = resultRoute.length -1;
                      var return_response = {"error":false,errorMessage:"Succès","driverRecord":resultDriverObj,startEndRoute:finalArray,routeRecord:resultRoute};
                          res.status(200).send(return_response);
                      //console.log(resultRoute);
                      
                    }
                  });
                }
              });
            }
          });
        }
      });
      
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,errorMessage:error };
      res.status(200).send(return_response);
    }
  },
  getAllRouteStop:async function(req,res,next)
  {
    try{
      //console.log(req.body);
      var {route_id} = req.body;
      StopsModel.find( {route_id:route_id,  routeType: { $nin: [ 'start','end']} }, {}).exec((errRoute, resultRoute)=>
      {
        if(errRoute)
        {
          //console.log(errRoute);
          var return_response = {"error":true,errorMessage:errRoute};
            res.status(200).send(return_response);
        }else{
          //console.log(resultRoute);
          var return_response = {"error":false,errorMessage:"Succès",routeRecord:resultRoute};
            res.status(200).send(return_response);
        }
      });
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,errorMessage:error };
      res.status(200).send(return_response);
    }
  },
  getSingleStopApi:async function(req,res,next)
  {
    try{
      //console.log(req.body);
      var {route_id} = req.body;
      StopsModel.findOne( {_id:route_id}, {}).exec((errRoute, resultRoute)=>
      {
        if(errRoute)
        {
          //console.log(errRoute);
          var return_response = {"error":true,errorMessage:errRoute};
            res.status(200).send(return_response);
        }else{
          //console.log(resultRoute);
          var return_response = {"error":false,errorMessage:"Succès",record:resultRoute};
            res.status(200).send(return_response);
        }
      });
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,errorMessage:error };
      res.status(200).send(return_response);
    }
  },
  deleteStopApi:async function(req,res,next)
  {
    try{
      //console.log(req.body);
      var {id} = req.body;
      var totalcounts = 0;
      StopsModel.findOne( {_id:id}, {route_id:1}).exec((errRoute, resultRoute)=>
      {
        if(errRoute)
        {
          //console.log(errRoute);
          var return_response = {"error":true,errorMessage:errRoute};
            res.status(200).send(return_response);
        }else{
          StopsModel.countDocuments({ route_id:resultRoute.route_id }).exec((errCount, totalcount)=>
          {
            if(errCount)
            {
              var return_response = {"error":true,success: false,errorMessage:errCount};
                res.status(200)
                .send(return_response);
            }else{
              totalcounts = totalcount;
              if(totalcount >0)
              {
                totalcounts = totalcount - 1;
              }
              RoutePlansModel.updateOne({ "_id":resultRoute.route_id }, 
                {totalStop:totalcounts}, function (uerr, docs) {
                if (uerr){
                  //console.log(uerr);
                  res.status(200)
                    .send({
                        error: true,
                        success: false,
                        errorMessage: uerr
                    });
                }else{
                  StopsModel.deleteOne( {_id:id}, {}).exec((errRoute, resultRoute)=>
                  {
                    if(errRoute)
                    {
                      //console.log(errRoute);
                      var return_response = {"error":true,errorMessage:errRoute};
                        res.status(200).send(return_response);
                    }else{
                      //console.log(resultRoute);
                      var return_response = {"error":false,errorMessage:"Succès",record:resultRoute};
                        res.status(200).send(return_response);
                    }
                  });
                }
              });
            }
          });
        }
      });
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,errorMessage:error };
      res.status(200).send(return_response);
    }
  },
  updatePositionRoute:async function(req,res,next)
  {
    try{
      //console.log(req.body);
      var {id,position,distance,distanceTime} = req.body;
      StopsModel.updateOne({ "_id":id }, 
        {"position":position,"distance":distance,"distanceTime":distanceTime,"status":1}, function (uerr, docs) {
        if (uerr){
          //console.log(uerr);
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: uerr
            });
        }else{
          var return_response = {"error":false,errorMessage:"Succès"};
          res.status(200).send(return_response);
        }
      });
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,errorMessage:error };
      res.status(200).send(return_response);
    }
  },
  updatePositionRouteLast:async function(req,res,next)
  {
    try{
      //console.log(req.body);
      var {route_id,position,distance,distanceTime} = req.body;
      StopsModel.updateOne({ "route_id":route_id,routeType:"end" }, 
        {"position":position,"distance":distance,"distanceTime":distanceTime}, function (uerr, docs) {
        if (uerr){
          //console.log(uerr);
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: uerr
            });
        }else{
          var return_response = {"error":false,errorMessage:"Succès"};
          res.status(200).send(return_response);
        }
      });
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,errorMessage:error };
      res.status(200).send(return_response);
    }
  },
  updateTotalTimeDistance:async function(req,res,next)
  {
    try{
      //console.log(req.body);
      var {route_id,totalHour,totalMin,totalDistance} = req.body;
      var totalTime = totalHour+"."+totalMin;
      totalTime = parseFloat(totalTime);
      RoutePlansModel.updateOne({ "_id":route_id }, 
        {
          "trip_status":1,
          "totalTime":totalTime,
          "totalHour":totalHour,
          "totalMin":totalMin,
          "totalDistance":totalDistance
        }, function (uerr, docs) {
        if (uerr){
          //console.log(uerr);
          res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: uerr
            });
        }else{
          var return_response = {"error":false,errorMessage:"Succès"};
          res.status(200).send(return_response);
        }
      });
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,errorMessage:error };
      res.status(200).send(return_response);
    }
  },
  finalRouteAssignApi:async function(req,res,next)
  {
    try{
      //console.log(req.body);
      var {route_id,driver_id} = req.body;

      DriversModel.findOne( {_id:driver_id}, {}).exec((errDriver, resultDriver)=>
      {
        if(errDriver)
        {
          //console.log(errDriver);
          var return_response = {"error":true,errorMessage:errDriver};
            res.status(200).send(return_response);
        }else{
          if(resultDriver)
          {
            // console.log(resultDriver.startAddress);
            // console.log(resultDriver.endAddress);
            // console.log(resultDriver.startLocation);
            // console.log(resultDriver.endLocation);
            StopsModel.findOne( {route_id:route_id,routeType:"start"}, {}).exec((errRoute, resultRoute)=>
            {
              if(errRoute)
              {
                //console.log(errRoute);
                var return_response = {"error":true,errorMessage:errRoute};
                  res.status(200).send(return_response);
              }else{
                if(resultRoute)
                {
                  StopsModel.updateOne({ "_id":resultRoute._id }, {
                    route_id:route_id,
                    address:resultDriver.startAddress,
                    location: resultDriver.startLocation,
                  },function(err2,result2){
                    if(err2)
                    {
                      //console.log(err);
                      var return_response = {"error":true,success: false,errorMessage:err2};
                        res.status(200)
                        .send(return_response);
                    }else{
                      RoutePlansModel.updateOne({ "_id":route_id }, 
                        {"driver_id":driver_id,"status":1}, function (uerr, docs) {
                        if (uerr){
                          //console.log(uerr);
                          res.status(200)
                            .send({
                                error: true,
                                success: false,
                                errorMessage: uerr
                            });
                        }else{
                          var return_response = {"error":false,errorMessage:"Route assignée succès complet"};
                          res.status(200).send(return_response);
                        }
                      });
                    }
                  });
                }else{
                  StopsModel.create({
                    route_id:route_id,
                    routeType:"start",
                    address:resultDriver.startAddress,
                    location: resultDriver.startLocation,
                  },function(err2,result2){
                    if(err2)
                    {
                      //console.log(err);
                      var return_response = {"error":true,success: false,errorMessage:err2};
                        res.status(200)
                        .send(return_response);
                    }else{
                      RoutePlansModel.updateOne({ "_id":route_id }, 
                        {"driver_id":driver_id,"status":1}, function (uerr, docs) {
                        if (uerr){
                          //console.log(uerr);
                          res.status(200)
                            .send({
                                error: true,
                                success: false,
                                errorMessage: uerr
                            });
                        }else{
                          var return_response = {"error":false,errorMessage:"Route assignée succès complet"};
                          res.status(200).send(return_response);
                        }
                      });
                    }
                  });
                }
              }
            });

          }
        }
      });
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,errorMessage:error };
      res.status(200).send(return_response);
    }
  },
  finalRouteAssignApi2:async function(req,res,next)
  {
    try{
      //console.log(req.body);
      var {route_id,driver_id} = req.body;

      DriversModel.findOne( {_id:driver_id}, {}).exec((errDriver, resultDriver)=>
      {
        if(errDriver)
        {
          //console.log(errDriver);
          var return_response = {"error":true,errorMessage:errDriver};
            res.status(200).send(return_response);
        }else{
          if(resultDriver)
          {
            // console.log(resultDriver.startAddress);
            // console.log(resultDriver.endAddress);
            // console.log(resultDriver.startLocation);
            // console.log(resultDriver.endLocation);
            StopsModel.findOne( {route_id:route_id,routeType:"end"}, {}).exec((errRoute, resultRoute)=>
            {
              if(errRoute)
              {
                //console.log(errRoute);
                var return_response = {"error":true,errorMessage:errRoute};
                  res.status(200).send(return_response);
              }else{
                if(resultRoute)
                {
                  StopsModel.updateOne({ "_id":resultRoute._id }, {
                    route_id:route_id,
                    address:resultDriver.endAddress,
                    location: resultDriver.endLocation,
                  },function(err2,result2){
                    if(err2)
                    {
                      //console.log(err);
                      var return_response = {"error":true,success: false,errorMessage:err2};
                        res.status(200)
                        .send(return_response);
                    }else{
                      RoutePlansModel.updateOne({ "_id":route_id }, 
                        {"driver_id":driver_id,"status":1}, function (uerr, docs) {
                        if (uerr){
                          //console.log(uerr);
                          res.status(200)
                            .send({
                                error: true,
                                success: false,
                                errorMessage: uerr
                            });
                        }else{
                          var return_response = {"error":false,errorMessage:"Route assignée succès complet"};
                          res.status(200).send(return_response);
                        }
                      });
                    }
                  });
                }else{
                  StopsModel.create({
                    route_id:route_id,
                    routeType:"end",
                    address:resultDriver.endAddress,
                    location: resultDriver.endLocation,
                  },function(err2,result2){
                    if(err2)
                    {
                      //console.log(err);
                      var return_response = {"error":true,success: false,errorMessage:err2};
                        res.status(200)
                        .send(return_response);
                    }else{
                      RoutePlansModel.updateOne({ "_id":route_id }, 
                        {"driver_id":driver_id,"status":1}, function (uerr, docs) {
                        if (uerr){
                          //console.log(uerr);
                          res.status(200)
                            .send({
                                error: true,
                                success: false,
                                errorMessage: uerr
                            });
                        }else{
                          var return_response = {"error":false,errorMessage:"Route assignée succès complet"};
                          res.status(200).send(return_response);
                        }
                      });
                    }
                  });
                }
              }
            });

          }
        }
      });
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,errorMessage:error };
      res.status(200).send(return_response);
    }
  },

  assignToDriverApi:async function(req,res,next)
  {
    try{
      // console.log(req.body);
      // return false;
      //console.log(req.files);
      var {route_id,driver_id,start_date,start_time,start_date_time} = req.body;
      var message = "";var notification_id = "";
      if(route_id == "" || route_id == null)
      {
        res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: "L'identité est requise"
        });
      }else{
        //"Adminstrateur vous a attribué un itineraire "+Route_plan_name+" qui commence le "plan_date
        RoutePlansModel.findOne( {_id:route_id}, {}).exec((err, resultRoute)=>
	      {
	        if(err)
	        {
	          console.log(err);
	        }else{
            RoutePlansModel.updateOne({ "_id":route_id }, 
              {
                "trip_status":1,
                "start_date":start_date,
                "start_time":start_time,
                "start_date_time":start_date_time
              }, function (uerr, docs) {
              if (uerr){
                //console.log(uerr);
                res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: uerr
                  });
              }else{
                var plan_cre_date =   resultRoute.created_at;
                var new_date = plan_cre_date.getMonth()+1+"-"+plan_cre_date.getDate()+"-"+plan_cre_date.getFullYear()+" "+plan_cre_date.getHours()+":"+plan_cre_date.getMinutes();
                 console.log(new_date);
                message = "Adminstrateur vous a attribué un itineraire "+resultRoute.routeName;
                //+" qui commence le "+new_date;
                DriverNotificationModel.create({
                  route_id:route_id,
                  driver_id:driver_id,
                  message:message
                },function(err,result){
                  if(err)
                  {
                    //console.log(err);
                    var return_response = {"error":true,success: false,errorMessage:err};
                      res.status(200)
                      .send(return_response);
                  }else{
                    notification_id = result._id;
                    DriversModel.findOne( {_id:driver_id}, {deviceToken:1}).exec((errDriver, resultDriver)=>
                    {
                      if(errDriver)
                      {
                        //console.log(errDriver);
                        var return_response = {"error":true,errorMessage:errDriver};
                          res.status(200).send(return_response);
                      }else{
                        //console.log("resultDriver ", resultDriver);
                        if(resultDriver)
                        {
                          if(resultDriver.deviceToken && resultDriver.deviceToken != null)
                          {
                            var message = {
                              to: resultDriver.deviceToken, 
                              collapse_key: 'your_collapse_key',
                              
                              notification: {
                                  title: 'Oblack', 
                                  body: message,
                              },
                              
                              data: {  //you can send only notification or only data(or include both)
                                  my_key: notification_id,
                                  my_another_key: 'new_route_assigned'
                              }
                            };
                            fcm.send(message, function(err, response){
                              if(err){
                                console.log("Something has gone wrong! err in fcm push");
                                console.log(err);
                                res.status(200)
                                .send({
                                    error: false,
                                    success: true,
                                    errorMessage: "Itinéraire assigné et envoyé au chauffeur"
                                });
                              }else{
                                console.log("Successfully sent with response: FCM PUSH");
                                res.status(200)
                                .send({
                                    error: false,
                                    success: true,
                                    errorMessage: "Itinéraire assigné et envoyé au chauffeur"
                                });
                              }
                            });
                          }else{
                            res.status(200)
                            .send({
                                error: false,
                                success: true,
                                errorMessage: "Itinéraire assigné et envoyé au chauffeur"
                            });
                          }
                        }else{
                          res.status(200)
                          .send({
                              error: false,
                              success: true,
                              errorMessage: "Itinéraire assigné et envoyé au chauffeur"
                          });
                        }
                      }
                    });

                    
                  }
                });

                
              }
            });
          }
        });
        
      }
    }catch(error){
      console.log(error);
      res.status(200)
      .send({
        error: true,
        success: false,
        errorMessage: error
      });
    }
  },

  allRouteStop:async(req,res)=>{
    try{
      //console.log("req.params kkk ",req.params);
      var id = req.params.id;
      //console.log("idddddddddddddd ", id);return false;
      let route_record = await RoutePlansModel.find({ "_id": id });
      let stop_record = await StopsModel.find({route_id:id});
      return res.status(200)
        .send({
            error: false,
            status: true,
            errorMessage: "Succès",
            route_record:route_record,
            stop_record:stop_record
        });
    }catch(e){
      res.status(200)
      .send({
        error: true,
        status: false,
        errorMessage: e.message
      });
    }
  }
};