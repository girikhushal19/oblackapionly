const Seller = require("../../models/seller/Seller");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const async = require("async");
const base_url = process.env.BASE_URL;
const path = require("path");
const AdminModel = require("../../models/admin/AdminModel");
const DriversModel = require("../../models/admin/DriversModel");
const RoutePlansModel = require("../../models/admin/RoutePlansModel");
const UsersModel = require("../../models/user/User");
const ClientsModel = require("../../models/admin/ClientsModel");
const ShipmentModel = require("../../models/admin/ShipmentModel");
const SettingModel = require("../../models/admin/SettingModel");
const InvoicesModel = require("../../models/admin/InvoicesModel");

const fs = require('fs');
const mongoose = require("mongoose");
//var bcrypt = require('bcryptjs');
//const jwt = require("jsonwebtoken");

module.exports = {
  allAdminShipment:async function(req,res,next)
	{
		
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
	    	//mongoose.set('debug', true);
        //console.log(String_qr);	    	
	      ShipmentModel.find( {}, {}).skip(fromindex).limit(perPageRecord).sort({created_at:-1}).lean(true).exec((err, result)=>
	      {
	        if(err)
	        {
	          console.log(err);
	        }else{
	          //console.log("result ");
	          //console.log(typeof result);
	          //console.log("result"+result);
	          if(result.length > 0)
	          {
	             var return_response = {"error":false,errorMessage:"Succès","record":result};
	                  res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	      });

	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
  allAdminShipmentCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);

	    try{
        
        ShipmentModel.countDocuments().exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
	allLatestShipmentCount:async(req,res)=>{
		try{
			let total_count = await ShipmentModel.count({status:1});
			let totalPageNumber = total_count / 10;
			totalPageNumber = Math.ceil(totalPageNumber);
			return res.send({
				error:false,
				errorMessage:"Succès",
				totalPageNumber:totalPageNumber
			})
		}catch(error)
		{
			return res.send({
				error:true,
				errorMessage:error.message
			})
		}
	},
	allLatestShipment:async(req,res)=>{
		try{
			let pageNo = req.body.pageNo;
			if(!pageNo)
			{
				pageNo = 0;
			}
			let fromindex = pageNo * 10;
			await ShipmentModel.find({status:1}).skip(fromindex).limit(10).sort({created_at:-1}).then((result)=>{
				return res.send({
					error:false,
					errorMessage:"Succès",
					record:result
				})
			}).catch((error)=>{
				return res.send({
					error:true,
					errorMessage:error.message
				})
			})
		}catch(error){
			return res.send({
				error:true,
				errorMessage:error.message
			})
		}
	},
	allCompletedShipment:async function(req,res,next)
	{
	    try{
	    	//mongoose.set('debug', true);
        //console.log(String_qr);	 
				var xx = 0;
				var total = 0;
				var perPageRecord = 10;
				var fromindex = 0;
				if(req.body.numofpage == 0)
				{
					fromindex = 0;
				}else{
					fromindex = perPageRecord * req.body.numofpage
				}
				var {user_id} = req.body;
				//console.log(req.body);return false;   	,status:3
				if(user_id)
				{
					ShipmentModel.find( {user_id:user_id,status:3,is_invoiced:0}, {}).skip(fromindex).limit(perPageRecord).exec((err, result)=>
					{
						if(err)
						{
							console.log(err);
						}else{
							//console.log("result ");
							//console.log(typeof result);
							//console.log("result"+result);
							if(result.length > 0)
							{
								var return_response = {"error":false,errorMessage:"Succès","record":result};
											res.status(200).send(return_response);
							}else{
								var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
								res.status(200).send(return_response);
							}

						}
					});
				}else{
					var return_response = {"error":true,errorMessage:"Identifiant d'utilisateur non valide" };
        	res.status(200).send(return_response);
				}
	      
	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
  getAdminShipmentDetail:async function(req,res,next)
	{
    try{
      //mongoose.set('debug', true);
      var { order_id } = req.body;
      var String_qr = {}; 
      String_qr['_id'] = order_id;
      //console.log(String_qr);	    	
      ShipmentModel.find( String_qr, {}).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
            res.status(200).send(return_response);
          }else{
            var return_response = {"error":true,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }
        }
      });
    }catch(err){
      console.log(err);
      var return_response = {"error":true,errorMessage:err };
      res.status(200).send(return_response);
    }
	},
	getAdminAllShipmentId:async function(req,res,next)
	{
		try{
			//mongoose.set('debug', true);
			//console.log(String_qr);	    	
			ShipmentModel.find( {status:1},{order_id:1,pickup_address:1,pickup_location:1,receiver_address:1,receiver_location:1}).exec((err, result)=>
			{
				if(err)
				{
					console.log(err);
				}else{
					//console.log("result ");
					//console.log(typeof result);
					//console.log("result"+result);
					if(result.length > 0)
					{
							var return_response = {"error":false,errorMessage:"Succès","record":result};
									res.status(200).send(return_response);
					}else{
						var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
						res.status(200).send(return_response);
					}
				}
			});
		}catch(err){
			console.log(err);
			var return_response = {"error":true,errorMessage:err };
			res.status(200).send(return_response);
		}
	},
	getAdminShipmentAddress:async function(req,res,next)
	{
		try{
			//mongoose.set('debug', true);
			//console.log(String_qr);	 
			var {stopType,shipment_id} = req.body;
			if(stopType != '' && stopType !="null" && stopType !=null  && stopType !=undefined  && stopType !='undefined')
			{
				ShipmentModel.findOne( {_id:shipment_id},{order_id:1,pickup_address:1,pickup_location:1,receiver_address:1,receiver_location:1}).exec((err, result)=>
				{
					if(err)
					{
						console.log(err);
					}else{
						//console.log("result ");
						//console.log(typeof result);
						//console.log("result"+result);
						if(result.length > 0)
						{
								var return_response = {"error":false,errorMessage:"Succès","record":result};
										res.status(200).send(return_response);
						}else{
							var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
							res.status(200).send(return_response);
						}
					}
				});
			}else{
				var return_response = {"error":true,errorMessage:"L'expédition et le type d'arrêt sont tous deux requis" };
				res.status(200).send(return_response);
			}			
		}catch(err){
			console.log(err);
			var return_response = {"error":true,errorMessage:err };
			res.status(200).send(return_response);
		}
	},
	generateInvoiceApi:async function(req,res,next)
	{
		try{
			var tax_percent = 0;
			var total_price = 0;
			var tax_value = 0;
			var {shipment_id} = req.body;
			var shipment_price = 0;
			//console.log(req.body);
			SettingModel.find({
        attribute_key: "tax_percent"
      }, {}).exec((errTax, resultTax) => {
        if (errTax) {
          //console.log(errTax);
					var return_response = {"error":true,errorMessage:errTax };
					res.status(200).send(return_response);
        } else {
          //console.log(resultTax);
          if (resultTax.length > 0)
					{
            //console.log(resultTax[0].attribute_value);
            tax_percent = resultTax[0].attribute_value;
          }
					if(shipment_id.length > 0)
					{
						//console.log("hereeee "+shipment_id.length);
						var invoice_id_random = Math.floor(10000000 + Math.random() * 90000000);
						//console.log("shipment_id "+shipment_id);
						var less_val = 0;
						less_val = shipment_id.length - 1;
						var m=0;
						for(x=0; x < shipment_id.length; x++)
						{
							//console.log("hereeee");
							//console.log(shipment_id[x]);
							ShipmentModel.findOne( {_id:shipment_id[x]}, {total_shipping_price:1,user_id:1}).exec((err, result)=>
							{
								if(err)
								{
									console.log(err);
									var return_response = {"error":true,errorMessage:err };
									res.status(200).send(return_response);
								}else{
									if(result)
									{
										shipment_price = parseFloat(shipment_price) + parseFloat(result.total_shipping_price);
										var user_id = result.user_id;
										// console.log("shipment_price "+shipment_price);
										// console.log("less_val "+less_val);
										// console.log("m -->"+m);
										if(less_val == m)
										{
											tax_value = parseFloat(shipment_price)* parseFloat(tax_percent) / 100;
											// console.log("tax_percent -> "+tax_percent);
											// console.log("tax_value -> "+tax_value);
											total_price = parseFloat(shipment_price) + parseFloat(tax_value) 
											InvoicesModel.create({ 
												invoice_id_random:invoice_id_random,
												shipment_id:shipment_id,
												user_id:user_id,
												shipment_price:shipment_price,
												tax_percent:tax_percent,
												tax_price:tax_value,
												total_price:total_price,
											},function(errInvoice,resultInvoice){
												if(errInvoice)
												{
													var return_response = {"error":true,success: false,errorMessage:errInvoice};
														res.status(200)
														.send(return_response);
												}else{
													//is_invoiced
													var inv_id = resultInvoice._id;
													ShipmentModel.updateMany(
													{ _id: { $in: shipment_id } },
													{ invoice_id : inv_id,is_invoiced:1 },
													{multi: true},function(errInvoice2,resultInvoice2){
														if(errInvoice2)
														{
															var return_response = {"error":true,success: false,errorMessage:errInvoice2};
																res.status(200)
																.send(return_response);
														}else{
															var return_response = {"error":false,success: true,errorMessage:"Facture générée avec succès"};
															res.status(200)
															.send(return_response);
														}
													});

													
												}
											});
										}
									}
								}
								m++;
							});
							
						}
					}else{
						var return_response = {"error":true,errorMessage:"Veuillez sélectionner n'importe quel envoi pour générer la facture" };
						res.status(200).send(return_response);
					}
        }
      });

			
			      
		}catch(err){
			console.log(err);
			var return_response = {"error":true,errorMessage:err };
			res.status(200).send(return_response);
		}
	},
  allAdminInvoice:async function(req,res,next)
	{
		
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
	    	//mongoose.set('debug', true);
        //console.log(String_qr);
				const queryJoin = [
					{
						path :"user_id",
						select:['first_name','last_name','phone','email']
					}
				];
	      InvoicesModel.find( {}, {}).populate(queryJoin).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
	      {
	        if(err)
	        {
	          console.log(err);
	        }else{
	          //console.log("result ");
	          //console.log(typeof result);
	          //console.log("result"+result);
	          if(result.length > 0)
	          {
	             var return_response = {"error":false,errorMessage:"Succès","record":result};
	                  res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	      });

	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
  allAdminInvoiceCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);

	    try{
        
        InvoicesModel.countDocuments().exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},


  allAdminInvoiceShipment:async function(req,res,next)
	{
		//console.log("line no --> 453"+JSON.stringify(req.body));
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
	    	//mongoose.set('debug', true);
        //console.log(String_qr);	    	
	      ShipmentModel.find( {invoice_id:req.body.invoice_id}, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
	      {
	        if(err)
	        {
	          console.log(err);
	        }else{
	          //console.log("result ");
	          //console.log(typeof result);
	          //console.log("result"+result);
	          if(result.length > 0)
	          {
	             var return_response = {"error":false,errorMessage:"Succès","record":result};
	                  res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	      });

	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
  allAdminInvoiceShipmentCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log("line no --> 500"+JSON.stringify(req.body));

	    try{
        
        ShipmentModel.countDocuments({invoice_id:req.body.invoice_id}).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
};