const Seller = require("../../models/seller/Seller");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const async = require("async");
const base_url = process.env.BASE_URL;
const path = require("path");
const AdminModel = require("../../models/admin/AdminModel");
const DriversModel = require("../../models/admin/DriversModel");
const RoutePlansModel = require("../../models/admin/RoutePlansModel");
const StopsModel = require("../../models/admin/StopsModel");
const DriverSupportModel = require("../../models/admin/DriverSupportModel");
const SettingModel = require("../../models/admin/SettingModel");

const fs = require('fs');
const mongoose = require("mongoose");
//var bcrypt = require('bcryptjs');
//const jwt = require("jsonwebtoken");

module.exports = {
  settingSubmit:async function(req,res,next)
  {
    try{
      //console.log(req.body);return false;
      var {tax_percent,max_merchant_limit,return_max_day} = req.body;

      let aa = await SettingModel.findOne({ attribute_key:"return_max_day"  });
      if(aa)
      {
        await SettingModel.updateOne({ attribute_key:"return_max_day"  },
        {
          attribute_key:"return_max_day",
          attribute_value:return_max_day
        });
      }else{
        await SettingModel.create({ attribute_key:"return_max_day",
              attribute_value:return_max_day  });
      }


      SettingModel.findOneAndUpdate({ attribute_key:"tax_percent"  },{
        attribute_key:"tax_percent",
        attribute_value:tax_percent
      },function(err,result){
        if(err)
        {
          var return_response = {"error":true,success: false,errorMessage:err};
            res.status(200)
            .send(return_response);
        }else{
          if(result == null)
          {
            SettingModel.create({ attribute_key:"tax_percent",
              attribute_value:tax_percent  },function(err,result){
              if(err)
              {
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                /*var return_response = {"error":false,success: true,errorMessage:"Succès"};
                  res.status(200)
                  .send(return_response);*/
              }
            });
          }else{
            /*var return_response = {"error":false,success: true,errorMessage:"Succès"};
                  res.status(200)
                  .send(return_response);*/
          }
        }
      });

      

      SettingModel.findOneAndUpdate({ attribute_key:"max_merchant_limit"  },{
        attribute_key:"max_merchant_limit",
        attribute_value:max_merchant_limit
      },function(err,result){
        if(err)
        {
          var return_response = {"error":true,success: false,errorMessage:err};
            res.status(200)
            .send(return_response);
        }else{
          if(result == null)
          {
            SettingModel.create({ attribute_key:"max_merchant_limit",
              attribute_value:max_merchant_limit  },function(err,result){
              if(err)
              {
                var return_response = {"error":true,success: false,errorMessage:err};
                  res.status(200)
                  .send(return_response);
              }else{
                var return_response = {"error":false,success: true,errorMessage:"Tous les paramètres sont mis à jour à partir de l'entrée"};
                  res.status(200)
                  .send(return_response);
              }
            });
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Tous les paramètres sont mis à jour à partir de l'entrée"};
                  res.status(200)
                  .send(return_response);
          }
        }
      });
      
    }catch(err)
    {
       console.log(err);
       var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200).send(return_response);
    }
  },

  getSetting:async function(req,res,next)
  {
    SettingModel.find({  }, {}).exec((err, result)=>
    {
      if(err)
      {
        console.log(err);
      }else{
        var return_response = {"error":false,success: true,errorMessage:"success","result":result};
              res.status(200)
              .send(return_response);
      }
    });
  },





};