const Seller = require("../../models/seller/Seller");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const async = require("async");
const base_url = process.env.BASE_URL;
const path = require("path");
const AdminModel = require("../../models/admin/AdminModel");
const DriversModel = require("../../models/admin/DriversModel");
const RoutePlansModel = require("../../models/admin/RoutePlansModel");
const StopsModel = require("../../models/admin/StopsModel");
const DriverSupportModel = require("../../models/admin/DriverSupportModel");
const SellertermsconditionModel = require("../../models/admin/SellertermsconditionModel");
const WebapppagesModel = require("../../models/admin/WebapppagesModel");
const ContactenquiryModel = require("../../models/admin/ContactenquiryModel");
const UsersModel = require("../../models/user/User");
const fs = require('fs');
const mongoose = require("mongoose");
//var bcrypt = require('bcryptjs');
//const jwt = require("jsonwebtoken");

module.exports = {
  adminLogin:async function(req,res,next)
  {
    //console.log("hereee controller");
    try{
      //mongoose.set('debug', true);
      const { email, password } = req.body;
    // Validate user input
    // Validate if user exist in our database
    var user = "";
    var date_time = new Date();
    user = await AdminModel.findOne({ email });
    //console.log("user record"+user);
    if (user && (await bcrypt.compare(password, user.password)))
    {
      // Create token
      const token = jwt.sign(
        { user_id: user._id, email },
        "OblackAdmin",
        /*process.env.TOKEN_KEY,
        {
          expiresIn: "2h",
        }*/
      );
      // save user token
      user.token = token;
      AdminModel.updateOne({ "email":email }, 
      {
        token:token,
        loggedin_time:date_time
      }, function (uerr, docs) {
        if (uerr)
        {
          //console.log(uerr);
          res.status(200)
          .send({
            error: true,
            success: false,
            errorMessage: uerr,
            //userRecord:user
          });
        }else if(user.status == 0)
        {
          res.status(200)
            .send({
              error: true,
              success: false,
              errorMessage: "Compte utilisateur inactif",
          });
        }else{
          //console.log("hereeee");return false;
          //console.log("User controller login method : ", docs);
          res.status(200)
          .send({
            error: false,
            success: true,
            errorMessage: "Utilisateur connecté avec succès",
            userRecord:user
          });
        }
      });
      // user
      //res.status(200).json(user);
      }else{
        //console.log("hereee 69");
        //res.status(400).send("Invalid Credentials");
        res.status(200)
        .send({
          error: true,
          success: false,
          errorMessage: "Connexion invalide"
        });
      }
    }catch(error){
      console.log(error);
      res.status(200)
      .send({
        error: true,
        success: false,
        errorMessage: error
      });
    }
  },
  getAdminProfile: async function(req, res,next)
  {
    //console.log(req.body);
    try{
      AdminModel.findOne({ _id:req.body.id }).exec((err, userRecord)=>{
        if(err)
        {
          //console.log("herrrrrerr");
          console.log(err);
        }else{
          //console.log(userRecord);
          var return_response = {"error":false,errorMessage:"success","record":userRecord};
          res.status(200).send(return_response);
        }
      });
    }catch(err){
      console.log(err);
      var return_response = {"error":true,errorMessage:err };
      res.status(200).send(return_response);
    }
  },
  addDriverSubmit: async function(req, res,next)
  {
    try{
      //console.log(req.body);
      var encryptedPassword = "";
      var {fullName,email,mobileNumber,vehicleType,shiftStartTime,shiftEndTime,breakStartTime,breakEndTime,startAddress,startLatitude,startLongitude,endAddress,endLatitude,endLongitude,password} = req.body;
      //var shiftEndTimeNew = new Date(shiftEndTime);
      //console.log(shiftEndTime);
      //console.log("shiftEndTimeNew "+shiftEndTimeNew);
      //console.log(shiftEndTimeNew.getHours());
      //console.log(shiftEndTimeNew.getMinutes());
      //console.log(shiftEndTimeNew.getSeconds());
      if(startLatitude == "" || startLatitude == null || startLongitude == "" || startLongitude == null)
      {
        var return_response = {"error":true,success: false,errorMessage:"l'adresse de départ doit être une adresse google"};
        res.status(200)
        .send(return_response);
      }else if(endLatitude == "" || endLatitude == null || endLongitude == "" || endLongitude == null)
      {
        var return_response = {"error":true,success: false,errorMessage:"l'adresse finale doit être une adresse google"};
        res.status(200)
        .send(return_response);
      }else{
        encryptedPassword = await bcrypt.hash(password, 10);
        //console.log("xx"+xx);
        //console.log("encryptedPassword khushal "+encryptedPassword);
        var token =  jwt.sign(
          { user_id:  Math.random().toString(36).slice(2), email },
          "Oblack-Driver",
          //{
          //  expiresIn: "2h",
          //}
        );
        DriversModel.count({ email,deleted_at:0 },(errs,emailCount)=>
        {
          if(errs)
          {
            //console.log(errs);
          }else{
            //console.log(emailCount);
            if(emailCount === 0)
            {
              DriversModel.count({ mobileNumber,deleted_at:0 },(userNameerrs,userNameCount)=>
              {
                if(userNameerrs)
                {
                  console.log(userNameerrs);
                }else{
                  if(userNameCount === 0)
                  {
                    DriversModel.create({
                      mobileNumber:mobileNumber,
                      fullName:fullName,
                      email: email.toLowerCase(), // sanitize: convert email to lowercase
                      password: encryptedPassword,
                      token: token,
                      vehicleType:vehicleType,
                      shiftStartTime:shiftStartTime,
                      shiftEndTime:shiftEndTime,
                      breakStartTime:breakStartTime,
                      breakEndTime:breakEndTime,
                      startAddress:startAddress,
                      startLocation: {
                        type: "Point",
                        coordinates: [startLatitude,startLongitude]
                      },
                      endAddress:endAddress,
                      endLocation: {
                        type: "Point",
                        coordinates: [endLatitude,endLongitude]
                      },
                    },function(err,result){
                      if(err)
                      {
                        //console.log(err);
                        var return_response = {"error":true,success: false,errorMessage:err};
                          res.status(200)
                          .send(return_response);
                      }else{
                        var return_response = {"error":false,success: true,errorMessage:"Compte chauffeur créé avec succès","driver_id":result._id};
                          res.status(200)
                          .send(return_response);
                      }
                    });
                  }else{
                    //email exist
                    res.status(200)
                          .send({
                              error: true,
                              success: false,
                              errorMessage: "Le numéro de mobile existe déjà"
                          });
                  }
                }
              });
            }else{
              //email exist
              res.status(200)
                .send({
                  error: true,
                  success: false,
                  errorMessage: "Email déjà existant"
              });
            }
          }
        }); 
      }
    }catch(err){
      console.log(err);
      var return_response = {"error":true,errorMessage:err };
      res.status(200).send(return_response);
    }
  },
  allDrivers:async function(req,res,next)
	{
		var { fullName,lastName,mobileNumber,email,city,status } = req.body;
  	/*console.log(fullName);
  	console.log(lastName);
  	console.log(email);*/

	  	var String_qr = {}; 
	    //String_qr['fullName'] = fullName;
	    if(fullName && fullName != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['fullName'] = {'$regex' : '^'+fullName, '$options' : 'i'};
	    }
	    if(lastName && lastName != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['lastName'] = {'$regex' : '^'+lastName, '$options' : 'i'};
	    }
	    if(email && email != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['email'] = {'$regex' : '^'+email, '$options' : 'i'};
	    }
	    if(mobileNumber && mobileNumber != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['mobileNumber'] = mobileNumber;
	    }
	    if(city && city != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['city'] = {'$regex' : '^'+city, '$options' : 'i'};
	    }
	    if(status && status != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['status'] = status;
	    }
      String_qr['deleted_at'] = 0;
       
	  	//console.log(String_qr);
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
	    	//mongoose.set('debug', true);

	    	
	      DriversModel.find( String_qr, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
	      {
	        if(err)
	        {
	          console.log(err);
	        }else{
	          //console.log("result ");
	          //console.log(typeof result);
	          //console.log("result"+result);
	          if(result.length > 0)
	          {
	             var return_response = {"error":false,errorMessage:"Succès","record":result};
	                  res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	      });

	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
  allDriversCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);
	    try{
        DriversModel.countDocuments({ deleted_at:0 }).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
  updateDriverStatusApi: async function(req, res,next)
  {
    try{
      //mongoose.set('debug', true);
      //console.log(req.body.status);
      DriversModel.updateOne({ "_id":req.body.id }, 
      {status:req.body.status}, function (uerr, docs) {
      if (uerr){
        //console.log(uerr);
        res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: uerr
          });
      }
      else{
        res.status(200)
          .send({
            error: false,
            success: true,
            errorMessage: "Succès complet de la mise à jour du statut"
          });
        }
      });
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,errorMessage:error };
      res.status(200).send(return_response);
    }
  },
  deleteDriverACApi: async function(req, res,next)
  {
    try{
      //mongoose.set('debug', true);
      //console.log(req.body.status);
      DriversModel.updateOne({ "_id":req.body.id }, 
      {deleted_at:1,token:null,}, function (uerr, docs) {
      if (uerr){
        //console.log(uerr);
        res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: uerr
          });
      }
      else{
        res.status(200)
          .send({
            error: false,
            success: true,
            errorMessage: "Succès complet de la mise à jour du statut"
          });
        }
      });
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,errorMessage:error };
      res.status(200).send(return_response);
    }
  },
  getSingleDriverApi: async function(req, res,next)
  {
    try{
      //mongoose.set('debug', true);
      DriversModel.findOne( {_id:req.body.id}, {}).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result)
          {
             var return_response = {"error":false,errorMessage:"Succès","record":result};
                  res.status(200).send(return_response);
          }else{
            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }

        }
      });
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,errorMessage:error };
      res.status(200).send(return_response);
    }
  },
  editDriverSubmit: async function(req, res,next)
  {
    try{
      //console.log(req.body);return false;
      var user_id = req.body.id;
      var email = req.body.email;
      var mobileNumber = parseInt(req.body.mobileNumber);
      var {fullName,email,vehicleType,shiftStartTime,shiftEndTime,breakStartTime,breakEndTime,startAddress,startLatitude,startLongitude,endAddress,endLatitude,endLongitude} = req.body;

      //console.log(user_id);
      var userRecord = await DriversModel.findOne({ "_id":user_id }); 
      //console.log("userRecord"+userRecord);
      if(req.files)
      {
        if(req.files.length > 0)
        {
          //console.log("ifffffff hereeee");
          var userImage = req.files[0].filename;
          var oldFileName = userRecord.userImage;
          var uploadDir = './public/uploads/driver/';
          let fileNameWithPath = uploadDir + oldFileName;
          //console.log(fileNameWithPath);
          //console.log(userImage);
          if (fs.existsSync(fileNameWithPath))
          {
            fs.unlink(uploadDir + oldFileName, (err) => 
            {
              console.log("unlink file error "+err);
            });
          }
        }else{
          if(userRecord.userImage)
          {
            var userImage = userRecord.userImage;
          }else{
            var userImage = null;
          }
        }            
      }else{
        if(userRecord.userImage)
        {
          var userImage = userRecord.userImage;
        }else{
          var userImage = null;
        }
      }
      // console.log("userImage");
      // console.log(userImage);
      // return false;
      if(userRecord)
      {
        
        var userRecordEmailCheck = await DriversModel.findOne({ "_id":{$ne:user_id},"email":email,deleted_at:0 }); 
        //console.log("userRecordEmailCheck"+userRecordEmailCheck);
        if(userRecordEmailCheck)
        {
          res.status(200)
            .send({
              error: true,
              success: false,
              errorMessage: "Cet email existe déjà chez un autre utilisateur"
          });
        }else{
          
          var userRecordMobileCheck = await DriversModel.findOne({ "_id":{$ne:user_id},"mobileNumber":mobileNumber,deleted_at:0 }); 
          //console.log("userRecordMobileCheck"+userRecordMobileCheck);
          if(userRecordMobileCheck)
          {
            res.status(200)
              .send({
                error: true,
                success: false,
                errorMessage: "Ce numéro de téléphone mobile existe déjà pour un autre utilisateur"
            });
          }else{
            DriversModel.findByIdAndUpdate({ _id: user_id },{ 
              mobileNumber:mobileNumber,
              fullName:fullName,
              email: email.toLowerCase(), // sanitize: convert email to lowercase
              vehicleType:vehicleType,
              shiftStartTime:shiftStartTime,
              shiftEndTime:shiftEndTime,
              breakStartTime:breakStartTime,
              breakEndTime:breakEndTime,
              startAddress:startAddress,
              startLocation: {
                type: "Point",
                coordinates: [startLatitude,startLongitude]
              },
              endAddress:endAddress,
              endLocation: {
                type: "Point",
                coordinates: [endLatitude,endLongitude]
              },
              }, function (err, user) {
              if(err){
                console.log(err);
                res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: "Un problème est survenu",
                      errorOrignalMessage: err
                  });
                
              }else{
                //console.log("L'enregistrement a été mis à jour avec succès");
                  res.status(200)
                  .send({
                      error: false,
                      success: true,
                      errorMessage: "L'enregistrement a été mis à jour avec succès"
                  });
              }
              })
          }
        }
      }else{
        //console.log("here");
        res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Détails de l'utilisateur non valides"
              });
      }
    }catch(err){
      console.log(err);
      var return_response = {"error":true,errorMessage:err };
      res.status(200).send(return_response);
    }
  },
  allDriversForAssign:async function(req,res,next)
	{
		var { fullName,lastName,mobileNumber,email,city,status } = req.body;
  	/*console.log(fullName);
  	console.log(lastName);
  	console.log(email);*/

	  	var String_qr = {}; 
	    //String_qr['fullName'] = fullName;
	    if(fullName && fullName != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['fullName'] = { $regex: '.*' + fullName + '.*' };
	    }
	    if(lastName && lastName != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['lastName'] = { $regex: '.*' + lastName + '.*' };
	    }
	    if(email && email != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['email'] = { $regex: '.*' + email + '.*' };
	    }
	    if(mobileNumber && mobileNumber != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['mobileNumber'] = { $regex: '.*' + mobileNumber + '.*' };
	    }
	    if(city && city != "")
	    {
	    	//{ "abc": { $regex: '.*' + colName + '.*' } }
	    	String_qr['city'] = { $regex: '.*' + city + '.*' };
	    } 
      String_qr['status'] = 1;
      String_qr['deleted_at'] = 0;
       
	  	//console.log(String_qr);
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
	    	//mongoose.set('debug', true);

	    	
	      DriversModel.find( String_qr, {}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
	      {
	        if(err)
	        {
	          console.log(err);
	        }else{
	          //console.log("result ");
	          //console.log(typeof result);
	          //console.log("result"+result);
	          if(result.length > 0)
	          {
	             var return_response = {"error":false,errorMessage:"Succès","record":result};
	                  res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	      });

	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
  allDriversForAssignCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);
	    try{
        DriversModel.countDocuments({ deleted_at:0,status:1 }).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},

  getSingleDriverSupportPage: async function(req, res,next)
  {
    //console.log(req.body);
    try{
      DriverSupportModel.find({  }).exec((err, userRecord)=>{
        if(err)
        {
          //console.log("herrrrrerr");
          console.log(err);
        }else{
          //console.log(userRecord);
          var return_response = {"error":false,errorMessage:"success","record":userRecord};
          res.status(200).send(return_response);
        }
      });
    }catch(err){
      console.log(err);
      var return_response = {"error":true,errorMessage:err };
      res.status(200).send(return_response);
    }
  },
  getSingleSellerTermsConditionPage: async function(req, res,next)
  {
    //console.log(req.body);
    try{
      // SellertermsconditionModel.find({  }).exec((err, userRecord)=>{
      //   if(err)
      //   {
      //     //console.log("herrrrrerr");
      //     console.log(err);
      //   }else{
      //     //console.log(userRecord);
      //     var return_response = {"error":false,errorMessage:"success","record":userRecord};
      //     res.status(200).send(return_response);
      //   }
      // });
      const userRecord = await SellertermsconditionModel.find();
      var return_response = {"error":false,errorMessage:"success","record":userRecord};
          res.status(200).send(return_response);
    }catch(err){
      console.log(err);
      var return_response = {"error":true,errorMessage:err };
      res.status(200).send(return_response);
    }
  },

  editSellerTermsCondition: async function(req, res,next)
  {
    try{
      SellertermsconditionModel.findOneAndUpdate({ _id:req.body.edit_id  },{
          description:req.body.html
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Succès"};
              res.status(200)
              .send(return_response);
          }
        });
    }catch(err)
    {
       console.log(err);
       var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200).send(return_response);
    }
  },
  editDriverSupportSubmit: async function(req, res,next)
  {
    try{
      DriverSupportModel.findOneAndUpdate({ _id:req.body.edit_id  },{
          description:req.body.html
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Succès"};
              res.status(200)
              .send(return_response);
          }
        });
    }catch(err)
    {
       console.log(err);
       var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200).send(return_response);
    }
  },
  updateWebPageSubmit: async function(req, res,next)
  {
    try{
      WebapppagesModel.findOneAndUpdate({
        _id:req.body.edit_id
        },{
          description:req.body.html
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err};
              res.status(200)
              .send(return_response);
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Succès"};
              res.status(200)
              .send(return_response);
          }
        });
    }catch(err)
    {
       console.log(err);
       var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200).send(return_response);
    }
  },
  getAllSlugWebPage: async function(req, res,next)
  {
    try{
      WebapppagesModel.find({},{
        url_slug:1
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err,"data":result};
              res.status(200)
              .send(return_response);
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Succès","data":result};
              res.status(200)
              .send(return_response);
          }
        });
    }catch(err)
    {
       console.log(err);
       var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200).send(return_response);
    }
  },
  getWebPageById: async function(req, res,next)
  {
    try{
      WebapppagesModel.findOne({_id:req.params.id},{
         
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err,"data":result};
              res.status(200)
              .send(return_response);
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Succès","data":result};
              res.status(200)
              .send(return_response);
          }
        });
    }catch(err)
    {
       console.log(err);
       var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200).send(return_response);
    }
  },
  getWebPageBySlug: async function(req, res,next)
  {
    try{
      WebapppagesModel.findOne({url_slug:req.params.url_slug},{
         
        },function(err,result){
          if(err)
          {
            var return_response = {"error":true,success: false,errorMessage:err,"data":result};
              res.status(200)
              .send(return_response);
          }else{
            var return_response = {"error":false,success: true,errorMessage:"Succès","data":result};
              res.status(200)
              .send(return_response);
          }
        });
    }catch(err)
    {
       console.log(err);
       var return_response = {"error":true,success: false,errorMessage:err};
          res.status(200).send(return_response);
    }
  },
  userWebContactUs: async function(req,res)
  {
    try{
      var {name,email,mobile,message} = req.body;
      await ContactenquiryModel.create({
        name:name,
        email:email,
        mobile:mobile,
        message:message
      }).then((result)=>{
        var return_response = {"error":false,success: true,errorMessage:"Demande envoyée avec succès nous vous contacterons bientôt"};
        return res.status(200).send(return_response);
      }).catch((error)=>{
        var return_response = {"error":true,success: false,errorMessage:err};
        return res.status(200).send(return_response);
      })
    }catch(err)
    {
       //console.log(err);
       var return_response = {"error":true,success: false,errorMessage:err};
       return res.status(200).send(return_response);
    }
  },

  updateCreditLimit: async function(req, res,next)
  {
    try{
      //mongoose.set('debug', true);
      // console.log(req.body);
      // return false;
      var {credit_limit,id}=req.body;
      if(credit_limit < 0)
      {
        return res.status(200)
                  .send({
                      error: true,
                      success: false,
                      errorMessage: "Valeur non valide"
                  });
      }
      UsersModel.updateOne({ "_id":id }, 
      {credit_limit:credit_limit}, function (uerr, docs) {
      if (uerr){
        //console.log(uerr);
        res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: uerr
          });
      }
      else{
        res.status(200)
          .send({
            error: false,
            success: true,
            errorMessage: "Succès complet de la mise à jour du statut"
          });
        }
      });
    }catch(error)
    {
      console.log(error);
      var return_response = {"error":true,errorMessage:error };
      res.status(200).send(return_response);
    }
  },
};