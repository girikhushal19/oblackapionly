const Seller = require("../../models/seller/Seller");
const orders = require("../../models/orders/order");
const PaymentRequestmodel=require("../../models/admin/PaymentRequest");
const SubscriptionModel=require("../../models/subscriptions/Subscription");
const pageViewsModel=require("../../models/products/pageViews");
const bycrypt = require("bcryptjs");

const jwt = require("jsonwebtoken");
const mongoose=require("mongoose")
const CoupensModel=require("../../models/seller/Coupens");
const SellerContactModel=require("../../models/seller/SellerContactModel");
const SellerQueryModel=require("../../models/seller/SellerQueryModel");
// const PaymentRequestmodel=require("../../models/admin/PaymentRequest");
const nodemailer = require("nodemailer");
const async = require("async");
const base_url = process.env.BASE_URL;
const path = require("path");
const ejs = require('ejs');
const {getdateinadvance,getDays,getDate} =require("../../modules/dates.js")
const customConstant = require('../../helpers/customConstant');
const SellertermsconditionModel = require("../../models/admin/SellertermsconditionModel");
const { Product } = require("../../models/products/Product");
const {
    sendmail
} = require("../../modules/sendmail");
const apptext_cmsmodel = require("../../models/admin/Apptext");
const transport = nodemailer.createTransport({
    name: "Oblack",
    host: process.env.MAILER_HOST,
    port: process.env.MAILER_PORT,
    auth: {
      user: process.env.MAILER_EMAIL_ID,
      pass: process.env.MAILER_PASSWORD,
    }
});


function generatePassword(digit) {
    var length = digit,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
}
// async function sendemail(req, res, data, done) {

//     var smtpTransport = nodemailer.createTransport({
//         host: process.env.MAILER_HOST,
//         port: process.env.MAILER_PORT,
//         secure: false,
//         auth: {
//             user: process.env.MAILER_EMAIL_ID,
//             pass: process.env.MAILER_PASSWORD,
//         },
//         tls: {
//             rejectUnauthorized: false
//         }
//     });

//     smtpTransport.verify(function (error, success) {

//         if (error) {
//             console.log(error);
//         } else {
//             console.log('Server is ready to take our messages');
//         }

//     })
//     const htmldata = await ejs.renderFile(path.resolve("./views/auth/" + data.template), {
//         name: data.context.name,
//         url: data.context.url
//     });
    
//     data.html = htmldata;

   
//     smtpTransport.sendMail(data, function (err) {
//         if (!err) {
//             return res.json({
//                 status: true,
//                 message: "Veuillez vérifier votre e-mail pour de plus amples instructions",
//                 errmessage: "",
//                 data: null,
//             });
//         } else {
//             console.log("error in email", err)
//             return done(err);
//         }
//     });
// };
module.exports = {
    register : async (req, res) => {

        //console.log(req.body);return false;
        // Our register logic starts here
        const code=Math.floor(100000 + Math.random() * 900000);
        
       
        // Get user input
        const { countrycode,email, password,fcm_token,extProvider,phone, fullname,subpackageid } = req.body;
        let encryptedPassword;
        // const newiscabinate=iscabinate==""?false:true;
        
        // Validate user input
        if (!(email )) {
          return res.status(200).send({
           message: "",
            status: false,
            errmessage: "Toute contribution est requise",
            data:null
          });
        }
      
        // check if user already exist
        // Validate if user exist in our database
        const oldUser = await Seller.findOne({ email });
       
        //   const oldUserP= await User.findOne({ email });
        if (oldUser) {
          return res.status(200).send({
            status: false,
            errmessage: "L'utilisateur existe déjà",
            message: "",
            data: null,
          });
        }
      if(password){
         encryptedPassword = await bycrypt.hash(password, 10);
          
          }else{
            if(extProvider){
             encryptedPassword=""
            }else{
              return res.send({
              status:false,
              message:"",
              errmessage:"Le mot de passe est requis !",
              data:null
             })
            }
          }
        //Encrypt user password
        data={
          email: email,
          password: encryptedPassword,
          // iscabinate:newiscabinate,
          fcm_token:fcm_token,
          fullname:fullname,
          phone:phone,
          countrycode:countrycode,
          sub_id:subpackageid
        }
      
        // Create user in our database
        const seller = await Seller.create(data);
        
        
        const token = jwt.sign(
          { user_id: seller._id, email },
          process.env.TOKEN_KEY,
          {
            expiresIn: "2h",
          }
        );
        // save user token
        seller.token = token;
        seller.extProvider=extProvider;
        seller.verification_code=code
        const page_name=process.env.register_confirm_email||"";
        const apptext=await apptext_cmsmodel.findOne({page_name:page_name});
        // Create token
        console.log("apptext 153   ---> "+apptext);
        let emailtext;
        let emailsubject;
        if(apptext?.page_content){
        emailtext=apptext.page_content;
        emailsubject=apptext.page_heading;
        }else{
          emailtext="L'inscription est réussie, continuez et mettez à jour votre profil et ceci est votre otp "+code+" pour vérifier votre compte email."
        }
        // const pathtofile=path.resolve("views/auth/confirm-register-email-confirm-register-email-lawyer.ejs");
        //  sendmail(pathtofile,{
        //     name: email,
        //     text:emailtext,
        //     code:code,
        //     base_url:base_url
           
        //   },emailsubject,email);
        // return new user
        seller.save();
        //console.log("hereeeeeeeeeeeeeeeeeee 182");
        var base_url_server = customConstant.base_url;
        let text_msg = "L'inscription est réussie, continuez et mettez à jour votre profil et ceci est votre otp "+code+" pour vérifier votre compte email.";
        var imagUrl = base_url_server+"public/uploads/logo.png";
        var html = "";
        html += "<div class='mail-content' style='position: relative;display: block;font-size: 14px;line-height: 25px;font-family: Poppins, sans-serif;'>";
        html += "<div class='content'></div>";
        html += "<p>	Bienvenue ,</p>";
        html += "<p>"+fullname+"</p>";
        html += "<p>"+text_msg+"</p>";
        html += "</div>";
        html += "<div class='mail-footer'>";
        html += "<img src='"+imagUrl+"' height='120' width='120'>";
        html += "</div>";
        html += "<i>Oblack</i>";


        let text_msg2 = "L'un des nouveaux vendeurs "+ fullname +" a été enregistré sur votre plateforme avec l'adresse e-mail "+ email +" , vérifiez ses coordonnées et mettez le compte à jour";
        var html2 = "";
        html2 += "<div class='mail-content' style='position: relative;display: block;font-size: 14px;line-height: 25px;font-family: Poppins, sans-serif;'>";
        html2 += "<div class='content'></div>";
        html2 += "<p>	Bienvenue ,</p>";
        html2 += "<p>L'administrateur</p>";
        html2 += "<p>"+text_msg2+"</p>";
        html2 += "</div>";
        html2 += "<div class='mail-footer'>";
        html2 += "<img src='"+imagUrl+"' height='120' width='120'>";
        html2 += "</div>";
        html2 += "<i>Oblack</i>";

        const message = {
            from: process.env.MAILER_FROM_EMAIL_ID, // Sender address
            to: email,         // recipients
            subject: "Vérifiez votre nouveau compte O'Black", // Subject line
            html: html
          };
          transport.sendMail(message, function(err, info) {
            if (err)
            {
              console.log("email sending error ->>> "+err);
                var return_response = {
                    status:true,
                    message:"Utilisateur créé avec succès",
                    errmessage:"",
                    data:seller
                };
                res.status(200)
                .send(return_response);
            }else{
                console.log('mail has sent.');
                const message = {
                    from: process.env.MAILER_FROM_EMAIL_ID, // Sender address
                    to: 'contact@oblackmarket.ci',         // recipients
                    subject: "Nouveau vendeur enregistré ici sur O'Black", // Subject line
                    html: html2
                  };
                  transport.sendMail(message, function(err, info) {
                    if (err)
                    {
                        console.log("email sending error ->>> "+err);
                        var return_response = {
                            status:true,
                            message:"Utilisateur créé avec succès",
                            errmessage:"",
                            data:seller
                        };
                        res.status(200)
                        .send(return_response);
                  
                    }else{
                       console.log('mail has sent.');
                        //console.log(info);
                        var return_response = {
                            status:true,
                            message:"Utilisateur créé avec succès",
                            errmessage:"",
                            data:seller
                        };
                        res.status(200)
                        .send(return_response);
                  
                    }
                  });
            }
          });


        // return res.send({
        //   status:true,
        //   message:"Utilisateur créé avec succès",
        //   errmessage:"",
        //   data:seller
      
        // });
      
        // Our register logic ends here
      },
    createseller: async (req, res) => {
        // console.log("req.body",req.body);
        // console.log("req.body",req.files);
        // return false;
        //registration_card  , 
        const {
            vendortype,
            fullname,
            ICorPassport,
            shopname,
            pickupaddress,
            email,
            countrycode,
            phone,
            password,
            newpassword,
            account_number,
            bank_name,
            account_name,
            bank_address,
            iban,
            bic,
            shopdesc,
            companyname,
            is_default,
            pickupaddressid,
            admin_commission_type,
            admin_commission,
            sub_id,
            bank_id,
            id,
            company_number,
            is_registered,
            web_reg_step,
            company_registration_number
        } = req.body;
        let photo;
        let front,back,shopphoto;
        let newpickupaddress1
        try{
            newpickupaddress1=JSON.parse(pickupaddress)
        }catch(e){
            newpickupaddress1=pickupaddress
        }
        //console.log("newpickupaddress1 ", newpickupaddress1);
        if(newpickupaddress1)
        {
            //console.log("1 ifff");
            
                //console.log("2 ifff");

                
                //console.log("newpickupaddress1 ",newpickupaddress1.latlong);
                if(newpickupaddress1.latlong)
                {
                    //console.log("3 ifff");
                    if(newpickupaddress1.latlong.length == 0)
                    {
                        console.log("4 ifff");
                        return res.send({
                            status: false,
                            message: "L'adresse de ramassage devrait être l'adresse de Google"
                        })
                    }
                }else{
                    //console.log("1 elseee");
                    return res.send({
                        status: false,
                        message: "L'adresse de ramassage devrait être l'adresse de Google"
                    })
                }
            
        }
        // else{
        //     //console.log("3 elseee");
        //     return res.send({
        //         status: false,
        //         message: "L'adresse de ramassage devrait être l'adresse de Google"
        //     })
        // }


        //console.log("hereeeeeeeee 362");
        //return false;
        if (!email) {
            return res.send({
                status: false,
                message: "something is required"
            })
        }
        //console.log("req?.files",req?.files)
        if (req?.files?.photo) {
            photo = "user/" + req.files.photo[0].filename;
        }
        if (req?.files?.front) {
            front = "user/" + req.files.front[0].filename;
        }
        if (req?.files?.back) {
            back = "user/" + req.files.back[0].filename;
        }
         if (req?.files?.shopphoto) {
            shopphoto = "user/" + req.files.shopphoto[0].filename;
        }
        var registration_card = null;
        if (req?.files?.registration_card) {
            registration_card = "user/" + req.files.registration_card[0].filename;
        }
        

        let hashedPass;
        let encryptedPasswordtest;
        if(newpassword&&password){
            encryptedPasswordtest = await bycrypt.hash(password, 10);
            
            const user = await Seller.findOne({ email:email });
            //console.log(user)
           const ispasswordcorrect=await bycrypt.compare(password, user.password);
           if(!ispasswordcorrect){
            return res.send({
              status:false,
              message:"",
              errmessage:"old password is not correct",
              data:null
            })
           }else{
            hashedPass= await bycrypt.hash(newpassword, 10);
           }
           }
        const newbank={
            ...(bank_name&&{bank_name:bank_name}),
            ...(account_number&&{account_number:account_number}),
            ...(account_name&&{account_name:account_name}),
            ...(bank_address&&{bank_address:bank_address}),
            ...(iban&&{iban:iban}),
            ...(bic&&{bic:bic}),
            ...(bank_id&&{_id:bank_id})
        }
        
        let datatosave = {
            ...(vendortype&&{vendortype:vendortype}),
            ...(fullname&&{fullname:fullname}),
            ...(ICorPassport&&{ICorPassport:ICorPassport}),
            ...(shopname&&{shopname:shopname}),
            ...(pickupaddress&&{pickupaddress:newpickupaddress1}),
            ...(email&&{email:email}),
            ...(countrycode&&{countrycode:countrycode}),
            ...(phone&&{phone:phone}),
            ...(shopdesc&&{shopdesc:shopdesc}),
            ...(bank_name&&{bank_name:bank_name}),
            ...(account_number && { account_number: account_number }),
            ...(account_name && { account_name: account_name }),
            ...(bank_address && { bank_address: bank_address }),
            ...(iban && { iban: iban }),
            ...(sub_id && {sub_id:sub_id}),
            ...(bic && { bic: bic }),
            ...(photo && { photo: photo }),
            ...(admin_commission_type && { admin_commission_type: admin_commission_type }),
            ...(admin_commission && { admin_commission: admin_commission }),
            ...(shopphoto&&{shopphoto:shopphoto}),
            ...(companyname&&{companyname:companyname}),
            ...(hashedPass && { password: hashedPass }),
            ...(front&&{"ICorPassportimage.front":front}),
            ...(back&&{"ICorPassportimage.back":back}),
            ...(company_number&&{company_number:company_number}),
            ...(is_registered&&{is_registered:is_registered}),
            ...(web_reg_step&&{web_reg_step:web_reg_step}),
            ...(registration_card&&{registration_card:registration_card}),
            ...(company_registration_number&&{company_registration_number:company_registration_number}),

        }
        //registration_card company_registration_number
        //console.log("datatosave",datatosave)
        let olduser = await Seller.findOne({email:email});
        let newpickupaddress={
            ...(newpickupaddress1?.address && { address: newpickupaddress1?.address }),
            ...(newpickupaddress1?.latlong && { latlong: newpickupaddress1?.latlong }),
            ...(pickupaddressid && { _id: pickupaddressid }),
            
            }
        console.log("newpickupaddress",newpickupaddress);
        let isbankalreadythere=olduser?.banks?.filter((add)=>{
            // add._id=mongoose.Types.ObjectId(newaddress._id)
            if(add?._id==newbank?._id){
                console.log("newaddress._id",newbank._id);
                return add
            }
            return
            
        })
        let isaddressalreadythere=olduser?.pickupaddress?.filter((add)=>{
            // add._id=mongoose.Types.ObjectId(newaddress._id)
            if(add?._id==newpickupaddress?._id){
                console.log("newaddress._id",newbank._id);
                return add
            }
            return
            
        })
        // console.log("isbankalreadythere",isbankalreadythere)
        // console.log("isaddressalreadythere",isaddressalreadythere)
        // console.log("newbank",newbank)
        if (olduser) {
            if(isbankalreadythere?.length){
                const query = { email:olduser.email };
               
                const updateDocument = {
                    "banks.$[item]": {
                     
                        _id:mongoose.Types.ObjectId(newbank._id),
                        ...(newbank.bank_name&&{bank_name:newbank.bank_name}),
                        ...(newbank.account_number&&{account_number:newbank.account_number}),
                        ...(newbank.account_name&&{account_name:newbank.account_name}),
                        ...(newbank.bank_address&&{bank_address:newbank.bank_address}),
                        ...(newbank.iban&&{iban:newbank.iban}),
                        ...(newbank.bic&&{bic:newbank.bic})
                       
                       
                    } 
                };
                console.log("updateDocument",updateDocument,"newaddress",newbank)
                const options = {
                  arrayFilters: [
                    {
                      "item._id": newbank._id,
                     
                    },
                  ],
                };
                const result = await Seller.updateOne(query, updateDocument, options);
                console.log("result",result,"cinetpayForSub.ejs")
            }
             if(isaddressalreadythere?.length){
                const query = { email:olduser.email };
               
                const updateDocument = {
                    "pickupaddress.$[item]": {
                     
                        _id:mongoose.Types.ObjectId(newpickupaddress._id),
                        ...(newpickupaddress.address&&{address:newpickupaddress.address}),
                        ...(newpickupaddress.latlong&&{latlong:newpickupaddress.latlong})
                       
                       
                    } 
                };
                // console.log("updateDocument",updateDocument,"newaddress",newbank)
                const options = {
                  arrayFilters: [
                    {
                      "item._id": newpickupaddress._id,
                     
                    },
                  ],
                };
                const result = await Seller.updateOne(query, updateDocument, options);
                
            }
            const datatest={
                ...(!isaddressalreadythere?.length&&{$push:{"pickupaddress":pickupaddress}}),
                ...(!isbankalreadythere?.length&&{$push:{"banks":newbank}}),
            }
            console.log("!isaddressalreadythere?.length",!isaddressalreadythere?.length,"datatest",datatest,"isbankalreadythere",isbankalreadythere,"newbank",newbank);

            console.log("newpickupaddress1 ",newpickupaddress1);
            await olduser.updateOne({
                ...(vendortype&&{vendortype:vendortype}),
                ...(fullname&&{fullname:fullname}),
                ...(ICorPassport&&{ICorPassport:ICorPassport}),
                ...(shopname&&{shopname:shopname}),
                ...(shopdesc&&{shopdesc:shopdesc}),
                // ...(pickupaddress&&{pickupaddress:pickupaddress}),
                ...(email&&{email:email}),
                ...(countrycode&&{countrycode:countrycode}),
                ...(phone&&{phone:phone}),
                ...(newpickupaddress1&&{"pickupaddress":newpickupaddress1}),
                ...((!isbankalreadythere?.length&&Object.keys(newbank).length)&&{$push:{"banks":newbank}}),
                ...(photo && { photo: photo }),
                ...(front&&{"ICorPassportimage.front":front}),
                ...(back&&{"ICorPassportimage.back":back}),
                ...(hashedPass && { password: hashedPass }),
                ...(shopphoto&&{shopphoto:shopphoto}),
                ...(companyname&&{companyname:companyname}),
                ...(bank_name&&{bank_name:bank_name}),
                ...(account_number && { account_number: account_number }),
                ...(account_name && { account_name: account_name }),
                ...(bank_address && { bank_address: bank_address }),
                ...(iban && { iban: iban }),
                ...(bic && { bic: bic }),
                ...(admin_commission_type && { admin_commission_type: admin_commission_type }),
                ...(admin_commission && { admin_commission: admin_commission }),
                ...(company_number && { company_number: company_number }),                
                ...(is_registered&&{is_registered:is_registered}),
                ...(web_reg_step&&{web_reg_step:web_reg_step}),
                ...(registration_card&&{registration_card:registration_card}),
                ...(company_registration_number&&{company_registration_number:company_registration_number}),
            }).then((result) => {
                // console.log("result",result);
                // return false;
                Seller.findOne({email:email}).exec((err,result_sel)=>{
                    if(err)
                    {
                        return res.send({
                            status: false,
                            message: "Enregistrer les succès ajoutés entièrement",
                            data:result_sel
                        })
                    }else{
                        return res.send({
                            status: true,
                            message: "Enregistrer les succès ajoutés entièrement",
                            data:result_sel
                        })
                    }
                });
                
            })
        } else {
            await Seller.create(datatosave).then((result) => {
                // return res.send({
                //     status: true,
                //     message: "created success",
                //     user_id:result._id
                // })
                var last_id = result._id;
                Seller.findOne({_id:last_id}).exec((err,result_sel)=>{
                    if(err)
                    {
                        return res.send({
                            status: false,
                            message: "Enregistrer les succès ajoutés entièrement",
                            user_id:last_id,
                            data:result_sel
                        });
                    }else{
                        return res.send({
                            status: true,
                            message: "Enregistrer les succès ajoutés entièrement",
                            user_id:last_id,
                            data:result_sel
                        });
                    }
                });

            }).catch((e) => {
                return res.send({
                    status: false,
                    message: e.message
                })
            })
        }
    },
    //registration_card company_registration_number
    createseller_second_update: async (req, res) => {
        //console.log("req.body",req.body)
        const {
            vendortype,
            fullname,
            ICorPassport,
            shopname,
            pickupaddress,
            email,
            countrycode,
            phone,
            password,
            newpassword,
            account_number,
            bank_name,
            account_name,
            bank_address,
            iban,
            bic,
            shopdesc,
            companyname,
            is_default,
            pickupaddressid,
            admin_commission_type,
            admin_commission,
            sub_id,
            bank_id,
            id,
            company_number,
            is_registered,
            web_reg_step
        } = req.body;
        let photo;
        let front,back,shopphoto;
         
        //console.log("hereeeeeeeee 362");
        //return false;
        if (!email) {
            return res.send({
                status: false,
                message: "something is required"
            })
        }
        console.log("req?.files",req?.files);
        if (req?.files?.photo) {
            photo = "user/" + req.files.photo[0].filename;
        }
        if (req?.files?.front) {
            front = "user/" + req.files.front[0].filename;
        }
        if (req?.files?.back) {
            back = "user/" + req.files.back[0].filename;
        }
         if (req?.files?.shopphoto) {
            shopphoto = "user/" + req.files.shopphoto[0].filename;
        }
        let hashedPass;
        let encryptedPasswordtest;
         
        let datatosave = {
            ...(vendortype&&{vendortype:vendortype}),
            ...(fullname&&{fullname:fullname}),
             
            ...(shopname&&{shopname:shopname}),
           
            //...(email&&{email:email}),
            ...(countrycode&&{countrycode:countrycode}),
            ...(phone&&{phone:phone}),
            
            ...(photo && { photo: photo }),
           
             
        }
        
        // 
        console.log("datatosave ",datatosave)
         
        // console.log("isbankalreadythere",isbankalreadythere)
        // console.log("isaddressalreadythere",isaddressalreadythere)
        // console.log("newbank",newbank)
        let olduser = await Seller.findOne({_id:id},{email:1});
        if(olduser)
        {
            await Seller.updateOne({_id:id},datatosave ).then((result) => {
                console.log("result",result);
                Seller.findOne({email:email}).exec((err,result_sel)=>{
                    if(err)
                    {
                        return res.send({
                            status: false,
                            message: "Enregistrer les succès ajoutés entièrement",
                            data:result_sel
                        })
                    }else{
                        return res.send({
                            status: true,
                            message: "Enregistrer les succès ajoutés entièrement",
                            data:result_sel
                        })
                    }
                });
                
            }).catch((error)=>{
                return res.send({
                    status: false,
                    message: error.message,
                    data:[]
                })
            });
        }else{
            return res.send({
                status: false,
                message: "Identifiant invalide",
                data:[]
            })
        }
        
    },
    updateBankDetail:async(req,res)=>{
        try{
            //console.log(req.body);
            let {seller_id,bank_id,bank_name,account_name,bank_address,iban,bic,account_number} = req.body;
            let latlong = bank_address.latlong;
            if(!latlong)
            {
                return res.send({
                    status: false,
                    message: "L'adresse de ramassage devrait être l'adresse de Google"
                })
            }
            if(!seller_id)
            {
                return res.send({
                    status: false,
                    message: "L'identifiant du vendeur est requis"
                })
            }
            // if(!bank_id)
            // {
            //     return res.send({
            //         status: false,
            //         message: "Une pièce d'identité bancaire est requis"
            //     })
            // }
            //mongoose.set("debug",true);
            let seller_record = await Seller.findOne({_id:seller_id});
            //console.log("seller_record ", seller_record);
            if(!seller_record)
            {
                return res.send({
                    status: false,
                    message: "L'enregistrement du vendeur n'est pas valide"
                })
            }else{
                if(!bank_id)
                {
                    let old_bank = seller_record.banks;
                    //console.log("heree 779");
                    //console.log(old_bank);
                    let address = bank_address.address;
                    const newbank={
                        ...(bank_name&&{bank_name:bank_name}),
                        ...(account_number&&{account_number:account_number}),
                        ...(account_name&&{account_name:account_name}),
                        ...(address&&{bank_address:address}),
                        ...(latlong && {latlong:latlong}),
                        ...(iban&&{iban:iban}),
                        ...(bic&&{bic:bic}),
                    };
                    old_bank.push(newbank);

                    //console.log("new bank ", old_bank);

                    await Seller.findByIdAndUpdate(seller_id,{
                        banks:old_bank
                    }).then((result)=>{
                        return res.send({
                            status:true,
                            message:"Le bilan de la banque s'est enrichi d'un succès complet"
                        })
                    })

                }else{
                    let bankRec = seller_record.banks.map(val=>{
                        if(val._id == bank_id)
                        {
                            val.account_number = account_number;
                            val.bank_name = bank_name;
                            val.account_name = account_name;
                            val.bank_address = bank_address.address;
                            val.latlong = bank_address.latlong;
                            val.iban = iban;
                            val.bic = bic;
                        }
                        return val;
                    })
                    //console.log("bankRec ", bankRec);
                    await Seller.updateOne({_id:seller_id},{banks:bankRec}).then((value)=>{
                        return res.send({
                            status:true,
                            errorMessage:"Succès de la mise à jour du relevé bancaire",
                        });
                    }).catch((e)=>{
                        //console.log("here 819");
                        return res.send({
                            status:false,
                            errorMessage:e.message,
                        });
                    });
                }
                
            }
        }catch(e){
            return res.send({
                status:false,
                errorMessage:e.message,
            });
        }
    },
    getSingleBankDetail:async(req,res)=>{
        try{
            //console.log(req.body);
            let {seller_id,bank_id} = req.body;
            if(!seller_id)
            {
                return res.send({
                    status: false,
                    message: "L'identifiant du vendeur est requis"
                })
            }
            if(!bank_id)
            {
                return res.send({
                    status: false,
                    message: "Une pièce d'identité bancaire est requis"
                })
            }
            //mongoose.set("debug",true);
            let seller_record = await Seller.findOne({_id:seller_id});
            //console.log("seller_record ", seller_record);
            if(!seller_record)
            {
                return res.send({
                    status: false,
                    message: "L'enregistrement du vendeur n'est pas valide"
                })
            }else{
                let bankRec = seller_record.banks.filter(val=>{
                    if(val._id == bank_id)
                    {
                        return val;
                    }
                })
                //console.log("bankRec ", bankRec);
                return res.send({
                    status:true,
                    errorMessage:"Succès",
                    data:bankRec
                });
            }
        }catch(e){
            return res.send({
                status:false,
                errorMessage:e.message,
            });
        }
    },
    login: async (req, res) => {
        try {
            // Get user input
            console.log("req.body==>",req.body)
            const { email, password, fcm_token } = req.body;
            let last_login_time = new Date();
            // Validate user input
            if (!(email)) {
                res.status(200).send(
                    {
                        status: false,
                        message: "",
                        errmessage: "Toute contribution est requise",
                        data: null
                    }
                );
            }
            // Validate if user exist in our database
            const user = await Seller.findOne({ email });
            if (password != "")
            {
                if (user && (await bycrypt.compare(password, user.password)))
                {
                    //console.log("user"+user);return false;
                    if(user.is_registered == false)
                    {
                        const user2 = await Seller.findOne({ _id:user._id } );
                        return res.status(200).json({
                            status: false,
                            errmessage: "Vous devez d'abord compléter votre inscription",
                            message: "Vous devez d'abord compléter votre inscription",
                            data: user2
                        });
                    }
                    if(user.isverfied == false)
                    {
                        return res.status(200).json({
                            status: false,
                            errmessage: "Votre email n'est pas vérifié, veuillez vérifier votre email",
                            message: "Votre email n'est pas vérifié, veuillez vérifier votre email",
                            data: []
                        });
                    }
                    if(user.is_approved == false)
                    {
                        return res.status(200).json({
                            status: false,
                            errmessage: "Votre compte est en cours d'examen attendez l'approbation",
                            message: "Votre compte est en cours d'examen attendez l'approbation",
                            data: []
                        });
                    }
                    // Create token
                    const token = jwt.sign(
                        { user_id: user._id, email },
                        process.env.TOKEN_KEY,
                        {
                            expiresIn: "24h",
                        }
                    );
                    user.fcm_token = fcm_token;
                    // save user token
                    user.token = token;
                    user.last_login_time = last_login_time;
                    user.save((result) => {
                        return res.status(200).json({
                            status: true,
                            errmessage: "",
                            message: "Connexion réussie",
                            data: user
                        });
                    })
                    // user

                } else {
                    return res.send({
                        status: false,
                        message: "",
                        errmessage: "L'adresse électronique ou le mot de passe est incorrect",
                        data: null
                    });
                }
            } else {
                if (user.extProvider) {
                    const token = jwt.sign(
                        { user_id: user._id, email },
                        process.env.TOKEN_KEY,
                        {
                            expiresIn: "24h",
                        }
                    );
                    // save user token
                    user.fcm_token = fcm_token;
                    user.token = token;
                    user.save((result) => {
                        return res.status(200).json({
                            status: true,
                            errmessage: "",
                            message: "Connexion réussie",
                            data: user
                        });
                    })
                } else {
                    return res.send({
                        status: false,
                        message: "",
                        errmessage: "Vous ne vous êtes pas réveillé en utilisant cette méthode",
                        data: null,
                    });
                }
            }

        } catch (err) {
            console.log(err);
        }
        // Our register logic ends here
    },
    // forgotpassword: (req, res) => {
    //     try {
    //         async.waterfall(
    //             [
    //                 function (done) {
    //                     Seller.findOne({
    //                         email: req.body.email,
    //                     }).exec(function (err, user) {
    //                         if (user) {
    //                             done(err, user);
    //                         } else {
    //                             done({
    //                                 status: false,
    //                                 message: "utilisateur non trouvé",
    //                                 errmessage: "",
    //                                 data: null,
    //                             });
    //                         }
    //                     });
    //                 },
    //                 function (user, done, err) {
    //                     // create a unique token
    //                     var tokenObject = {
    //                         email: user.email,
    //                         id: user._id,
    //                     };
    //                     var secret = user._id + "_" + user.email + "_" + new Date().getTime();
    //                     var token = jwt.sign(tokenObject, secret);
    //                     done(err, user, token);
    //                 },
    //                 function (user, token, done) {
    //                     Seller.findByIdAndUpdate(
    //                         { _id: user._id },
    //                         {
    //                             reset_password_token: token,
    //                             reset_password_expires: Date.now() + 86400000,
    //                         },
    //                         { new: true }
    //                     ).exec(function (err, new_user) {
    //                         done(err, token, new_user);
    //                     });
    //                 },
    //                 function (token, user, done) {
    //                     var data = {
    //                         to: user.email,
    //                         from: process.env.MAILER_EMAIL_ID,
    //                         template: "forgot-password-email.ejs",
    //                         subject: "L'aide pour les mots de passe est arrivée !",

    //                         context: {
    //                             url: base_url + "/api/seller/reset_password?token=" + token,
    //                             name: user.first_name,
    //                         },
    //                     };
    //                     // console.log(data)
    //                     sendemail(req, res, data, done);
    //                 },
    //             ],
    //             function (err) {
    //                 return res.status(422).json({
    //                     status: false,
    //                     message: "",
    //                     errmessage: err.message,
    //                     data: null,
    //                 });
    //             }
    //         );
    //     } catch (err) {
    //         console.log(err);
    //     }
    // },
    logout: async (req, res) => {
        const id = req.params.id;
        const Lawyer = await Seller.findById(id);
        Lawyer.token = "";
        Lawyer.fcm_token = "";
        Lawyer.save();
        res.status(200).json({
            status: true,
            errmessage: "",
            message: "Deconnexion réussie",
            data: null
        });
    },
   
    render_reset_password_template: (req, res) => {
        return res.render(path.resolve('./views/auth/reset-password-seller.ejs'), {
            url: base_url
        });
    },
    reset_password: async (req, res) => {
        const page_name = process.env.reset_password_confirm_email
        const apptext = await apptext_cmsmodel.findOne({ page_name: page_name });
        // Create token
        console.log(apptext);
        let emailtext;
        let emailsubject;
        if (apptext?.page_content) {
            emailtext = apptext.page_content;
            emailsubject = apptext.page_heading;
        } else {
            emailtext = "Votre mot de passe a été réinitialisé avec succès, vous pouvez maintenant vous connecter avec votre nouveau mot de passe."
            emailsubject = "Confirmation de la réinitialisation du mot de passe"
        }
        //  console.log(req.body)
        Seller.findOne({
            reset_password_token: req.body.token,
            reset_password_expires: {
                $gt: Date.now(),
            },
        }).exec(async function (err, user) {
            // console.log(user,err)
            if (!user) {
                return res.send({
                    status: false,
                    message: "page expired",
                    errmessage: "La page a expiré, demandez à nouveau la réinitialisation du mot de passe"
                })
            }
            if (user && !err) {

                if (req.body.newPassword == req.body.verifyPassword) {
                    // console.log("password matched")
                    let encryptedPassword = await bycrypt.hash(req.body.newPassword, 10);
                    user.password = encryptedPassword;
                    user.reset_password_expires = undefined;
                    user.reset_password_token = undefined;
                    user.save(async function (err, user) {
                        if (!err) {
                            var data = {
                                to: user.email,
                                from: process.env.MAILER_EMAIL_ID,
                                template: 'reset-password-email.ejs',
                                subject: emailsubject,
                                context: {
                                    name: user.first_name,
                                    emailtext: emailtext
                                }
                            };
                            var smtpTransport = nodemailer.createTransport({
                                host: process.env.MAILER_HOST,
                                port: process.env.MAILER_PORT,
                                secure: false,
                                auth: {
                                    user: process.env.MAILER_EMAIL_ID,
                                    pass: process.env.MAILER_PASSWORD,
                                },
                                tls: {
                                    rejectUnauthorized: false
                                }
                            });


                            const htmldata = await ejs.renderFile(path.resolve("./views/auth/" + data.template), {
                                base_url:base_url,
                                name: data.context.name,
                                url: data.context.url
                            });
                            
                            data.html = htmldata;
                            // console.log("data in email",data)
                            
                            smtpTransport.sendMail(data, function (err) {
                                if (!err) {
                                    return res.status(200).json({
                                        status: true,
                                        message: "Confirmer le succès du mot de passe",
                                        errmessage: "",
                                        data: null,
                                    });
                                } else {
                                    // console.log(err)
                                    return res.send(
                                        {
                                            status: false,
                                            message: "",
                                            errmessage: err.message,
                                            data: null,
                                        }
                                    );
                                }
                            });
                        } else {
                            return res.status(422).send({
                                status: false,
                                message: "",
                                errmessage: err.message,
                                data: null,
                            });
                        }
                    })

                }
            }
        });
    },
    getvendors:async(req,res)=>{
        try{
            const approved=req.body.approved;
            const pageno=req.body.pageno;
            const limit=10;
            const skip=limit*pageno;
            const pending=req.body.pending;
            const all=req.body.all;
            let query={is_deleted:false};
            if(approved){
                query["is_approved"]=true
            }
            if(pending){
                query["is_approved"]=false
            }
            if(pageno==null||pageno=="null"){
                const count=(await Seller.find(query))?.length

                const pages=Math.ceil(count/limit)
                return res.send({
                    status:true,
                    pages:pages
                })
            }
            console.log("veb 958");
            const vendors=await Seller.aggregate([
                {$match:query},
                {$sort:{createdAt:-1}},
                {$skip:skip},
                {$limit:limit}
            ])
            return res.send({
                status:true,
                data:vendors
            });
        }catch(error){
            return res.send({
                status:false,
                data:[]
            });
        }
        
    },
    activeVendors:async(req,res)=>{
        try{
            const approved=req.body.approved;
            const pageno=req.body.pageno;
            const limit=10;
            const skip=limit*pageno;
            const pending=req.body.pending;
            const all=req.body.all;
            let query={is_deleted:false};
            query["is_active"] = true;
            query["is_approved"] = true;
            if(pageno==null||pageno=="null")
            {
                const count=(await Seller.find(query,{_id:1}))?.length;
                const pages=Math.ceil(count/limit);
                return res.send({
                    status:true,
                    pages:pages
                })
            }
            console.log("veb 958");
            const vendors=await Seller.aggregate([
                {$match:query},
                {$sort:{createdAt:-1}},
                {$skip:skip},
                {$limit:limit}
            ])
            return res.send({
                status:true,
                data:vendors
            });
        }catch(error){
            return res.send({
                status:false,
                data:[]
            });
        }
        
    },
    inActiveVendors:async(req,res)=>{
        try{
            const approved=req.body.approved;
            const pageno=req.body.pageno;
            const limit=10;
            const skip=limit*pageno;
            const pending=req.body.pending;
            const all=req.body.all;
            let query={is_deleted:false};
            query["is_active"] = false;
            query["is_approved"] = false;
            if(pageno==null||pageno=="null")
            {
                const count=(await Seller.find(query,{_id:1}))?.length;
                const pages=Math.ceil(count/limit);
                return res.send({
                    status:true,
                    pages:pages
                })
            }
            console.log("veb 958");
            const vendors=await Seller.aggregate([
                {$match:query},
                {$sort:{createdAt:-1}},
                {$skip:skip},
                {$limit:limit}
            ])
            return res.send({
                status:true,
                data:vendors
            });
        }catch(error){
            return res.send({
                status:false,
                data:[]
            });
        }
        
    },
    inActiveVendors:async(req,res)=>{
        try{
            const approved=req.body.approved;
            const pageno=req.body.pageno;
            const limit=10;
            const skip=limit*pageno;
            const pending=req.body.pending;
            const all=req.body.all;
            let query={is_deleted:false};
            query["is_active"] = false;
            query["is_approved"] = false;
            if(pageno==null||pageno=="null")
            {
                const count=(await Seller.find(query,{_id:1}))?.length;
                const pages=Math.ceil(count/limit);
                return res.send({
                    status:true,
                    pages:pages
                })
            }
            console.log("veb 958");
            const vendors=await Seller.aggregate([
                {$match:query},
                {$sort:{createdAt:-1}},
                {$skip:skip},
                {$limit:limit}
            ])
            return res.send({
                status:true,
                data:vendors
            });
        }catch(error){
            return res.send({
                status:false,
                data:[]
            });
        }
        
    },
    getDeletedVendors:async(req,res)=>{
        try{
            const approved=req.body.approved;
            const pageno=req.body.pageno;
            const limit=10;
            const skip=limit*pageno;
            const pending=req.body.pending;
            const all=req.body.all;
            let query={is_deleted:true};
            // query["is_active"] = false;
            // query["is_approved"] = false;
            if(pageno==null||pageno=="null")
            {
                const count=(await Seller.find(query,{_id:1}))?.length;
                const pages=Math.ceil(count/limit);
                return res.send({
                    status:true,
                    pages:pages
                })
            }
            console.log("veb 958");
            const vendors=await Seller.aggregate([
                {$match:query},
                {$sort:{createdAt:-1}},
                {$skip:skip},
                {$limit:limit}
            ])
            return res.send({
                status:true,
                data:vendors
            });
        }catch(error){
            return res.send({
                status:false,
                data:[]
            });
        }
        
    },
    deletevendors:async(req,res)=>{

        try{
            const id=req.params.id;
            const vendor=await Seller.findById(id);
            // vendor.is_deleted=true;
            // vendor.email=vendor.email+"_"+Math.random()*1000+"_deleted";
            // vendor.save((result)=>{
            //     return res.send({
            //         status:true,
            //         message:"Compte vendeur supprimé succès complet"
            //     })
            // })
            if(vendor)
            {
                let new_email = email=vendor.email+"_"+Math.random()*1000+"_deleted";
                await Seller.updateOne({_id:id},{email:new_email,is_deleted:true}).then((result)=>{
                    return res.send({
                        status:true,
                        message:"Compte vendeur supprimé succès complet",
                    });
                }).catch((error)=>{
                    return res.send({
                        status:false,
                        message:error.message,
                        data:[]
                    });
                });
            }else{
                return res.send({
                    status:false,
                    message:"Identifiant invalide"
                })
            }
        }catch(error){
            return res.send({
                status:false,
                message:error.message,
                data:[]
            });
        }
        
    },
    deletemultipleuser:async(req,res)=>{
        try{
            const ids=req.body.ids;
            console.log("ids ", ids);
            //return false;
            for(let i=0;i<ids.length;i++)
            {
                id = ids[i];
                console.log("id ", id);
                 const user=await Seller.findById(id,{fullname:1,email:1});
                 //console.log("user ", user);
                 
                // user.email=user.email+"_"+(Math.random()*1000)+"_deleted";
                // user.is_deleted=true;
                // user.save().then((result)=>{})

                
                let new_email = email=user.email+"_"+Math.random()*1000+"_deleted";
                await Seller.updateOne({_id:id},{email:new_email,is_deleted:true});
                

                // .then((result)=>{
                //     return res.send({
                //         status:true,
                //         message:"Compte vendeur supprimé succès complet",
                //     });
                // }).catch((error)=>{
                //     return res.send({
                //         status:false,
                //         message:error.message,
                //         data:[]
                //     });
                // });
            }
            //return false;
            return res.send({
                status:true,
                message:"Compte vendeur supprimé succès complet"
            })
        }catch(error){
            return res.send({
                status:false,
                message:error.message
            })
        }
        
    },
    getallsellerforfilter:async(req,res)=>{
        try{
            const allproducts=await Seller.aggregate([
                {$match:{is_deleted:false}},
                {$project:{
                    _id:1,
                    fullname:1
                }}
            ]);
            return res.send({
                status:true,
                data:allproducts
            })
        }catch(error)
        {
            return res.send({
                status:false,
                data:[],
                message:error.message
            })
        }
    },
    markVendorasActive:async(req,res)=>{
        try{
            const id=req.body.id;
        const vendor=await Seller.findById(id);
        if(vendor)
        {
            const update_vendor = await Seller.updateOne({_id:id},{is_active:true,is_approved:true}).then(()=>{
                var base_url_server = customConstant.base_url;
                var imagUrl = base_url_server+"public/uploads/logo.png";
                let text_msg = "Votre compte a été approuvé, vous pouvez maintenant vous connecter et accéder à toutes les fonctionnalités.";
                var html = "";
                html += "<div class='mail-content' style='position: relative;display: block;font-size: 14px;line-height: 25px;font-family: Poppins, sans-serif;'>";
                html += "<div class='content'></div>";
                html += "<p>	Bienvenue ,</p>";
                html += "<p>"+vendor.fullname+"</p>";
                html += "<p>"+text_msg+"</p>";
                html += "</div>";
                html += "<div class='mail-footer'>";
                html += "<img src='"+imagUrl+"' height='120' width='120'>";
                html += "</div>";
                html += "<i>Oblack</i>";


                    const message = {
                        from: process.env.MAILER_FROM_EMAIL_ID, // Sender address
                        to: vendor.email,         // recipients
                        subject: "Vérifiez votre nouveau compte O'Black", // Subject line
                        html: html
                    };
                    transport.sendMail(message, function(err, info) {
                    if (err)
                    {
                        console.log("email sending error ->>> "+err);
                        return res.send({
                            status:true,
                            message:"Vendeur actif succès complet"
                        })
                    }else{
                        console.log('mail has sent.');
                        console.log('mail has sent.');
                        return res.send({
                            status:true,
                            message:"Vendeur actif succès complet"
                        })
                    }
                });
            }).catch((error)=>{
                var return_response = {
                    status:false,
                    message:error,
                    errmessage:"",
                    data:[]
                };
                res.status(200)
                .send(return_response);
            });
            
            
        }else{
            return res.send({
                status:false,
                message:"Numéro d'identification invalide"
            })
        }
        }catch(error){
            return res.send({
                status:false,
                message:error.message
            })
        }
    },
    markVendorasInActive:async(req,res)=>{
        try{
            const id=req.body.id;
            //console.log(req.body);return false;
            const vendor=await Seller.findById(id);
            if(vendor)            
            {
                //console.log("vendor ",vendor);
                await Seller.updateOne({_id:id},{is_active:false}).then((result)=>{
                    return res.send({
                        status:true,
                        message:"Succès total du vendeur inactif"
                    })
                }).catch((error)=>{
                    return res.send({
                        status:false,
                        message:error.message
                    })
                })
            }else{
                return res.send({
                    status:false,
                    message:"Numéro d'identification invalide"
                })
            }
        }catch(error)
        {
            return res.send({
                status:false,
                message:error.message
            })
        }
        
    },
    markAsApproved:async(req,res)=>{
        const id=req.params.id;
        const emailsubject="Oblack! Account Activated"
        const vendor=await Seller.findById(id);
        vendor.is_approved=true;
        vendor.save((result)=>{
            const pathtofile=path.resolve("views/notifications/updatesellerafterapprovelonceitisdone.ejs");
            sendmail(pathtofile,{
              
               base_url:base_url
              
             },emailsubject,vendor.email);
            return res.send({
                status:true,
                message:"deleted successfully"
            })
        })
    },
    markAsUnApproved:async(req,res)=>{
        const id=req.params.id;
        const vendor=await Seller.findById(id);
        vendor.is_approved=false;
        vendor.save((result)=>{
            return res.send({
                status:true,
                message:"deleted successfully"
            })
        })
    },
    resend_verificatio_email_code:async(req,res)=>{
        const email=req.body.email;
        const code = Math.floor(100000 + Math.random() * 900000);
        const userRecord = await Seller.findOne({email:email});
        //console.log("userRecord "+userRecord);
        if(userRecord)
        {
            userRecord.verification_code=code;
            userRecord.isverfied=false;
            userRecord.save().then((result)=>{

                var base_url_server = customConstant.base_url;
                let text_msg = "Ceci est votre otp "+code+" pour vérifier votre compte email.";
                var imagUrl = base_url_server+"public/uploads/logo.png";
                var html = "";
                html += "<div class='mail-content' style='position: relative;display: block;font-size: 14px;line-height: 25px;font-family: Poppins, sans-serif;'>";
                html += "<div class='content'></div>";
                html += "<p>	Bienvenue ,</p>";
                html += "<p>"+userRecord.fullname+"</p>";
                html += "<p>"+text_msg+"</p>";
                html += "</div>";
                html += "<div class='mail-footer'>";
                html += "<img src='"+imagUrl+"' height='120' width='120'>";
                html += "</div>";
                html += "<i>Oblack</i>";

                const message = {
                    from: process.env.MAILER_FROM_EMAIL_ID, // Sender address
                    to: email,         // recipients
                    subject: "Vérifiez votre nouveau compte O'Black", // Subject line
                    html: html
                };
                transport.sendMail(message, function(err, info) {
                    if (err)
                    {
                        console.log("email sending error ->>> "+err);
                        return res.send({
                            status:false,
                            message:err
                        });
                    }else{
                        console.log('mail has sent.');
                        //console.log(info);
                        return res.send({
                            status:true,
                            message:"Code de vérification envoyé sur votre email"
                        });
                    }
                });
                // return res.send({
                //     status:true,
                //     message:"Code de vérification envoyé sur votre email"
                // })
            }).catch((error)=>{
                return res.send({
                    status:false,
                    message:error
                })
            });
        }else{
            return res.send({
                status:false,
                message:"Identité électronique invalide"
            })
        }
    },
    verifycode:async(req,res)=>{
        const id=req.body.id;
        const code=req.body.code;
        const user=await Seller.findById(id);
        if(user){
          if(code.toString()==user.verification_code.toString()){
            user.verification_code="",
            user.isverfied=true;
            user.web_reg_step=3;
            user.save().then((result)=>{
             return res.send({
               status:true,
               message:"La vérification est réussie",
               errmessage:"",
               data:null
             })
            })
           }else{
             return res.send({
               status:false,
               message:"",
               errmessage:"Le code n'est pas valide",
               data:null
             })
           }
        }else{
          return res.send({
            status:false,
            message:"",
            errmessage:"Aucun utilisateur trouvé",
            data:null
          })
        }
      },
    
    saveorupdatecoupens:async (req,res)=>{
    const {
        products,
        provider_id,
        name,
        code,
        start_date,
        end_date,
        limit,
        type,
        monper,
        id
        }=req.body;
        //console.log("req.boyd",req.body)
    
        if(id){
        const datatoupdate={
            ...(products&&{products:products}),
            ...(name&&{name:name}),
            ...(code&&{code:code}),
            ...(monper&&{discountamount:monper}),
            ...(limit&&{limit:limit}),
            ...(provider_id&&{provider_id:provider_id}),
            ...(start_date&&{start_date:start_date}),
            ...(end_date&&{end_date:end_date}),
            ...(type&&{type:type})
        }
        const coumodel=await CoupensModel.findByIdAndUpdate(id,datatoupdate)
        .then((result)=>{
            return res.send({
                status:true,
                message:"",
                errmessage:"Coupon mis à jour avec succès"
                })
        })
        }else{
        const coumodel=new CoupensModel();
        coumodel.name=name;
        coumodel.products=products;
        coumodel.code=code;
        coumodel.discountamount=monper
        coumodel.limit=limit
        coumodel.provider_id=provider_id;
        coumodel.start_date=start_date
        coumodel.end_date=end_date
        coumodel.type=type
        
        coumodel.save().then((result)=>{
        return res.send({
            status:true,
            message:"Coupon enregistré avec succès"
        })
        })
        }
    },
    getallcouponsbyvendorid:async (req,res)=>{
        try{
            const id=req.params.id;
            await CoupensModel.find({provider_id:id}).then((result)=>{
                return res.send({
                    status:true,
                    message:"Success",
                    data:result
                })
            })
        }catch(error){
            return res.send({
                status:true,
                data:[]
            })
        }
    },
    getSingleCouponsById:async (req,res)=>{
        try{
            const id=req.params.id;
            await CoupensModel.findOne({_id:id}).then((result)=>{
                return res.send({
                    status:true,
                    message:"Success",
                    data:result
                })
            })
        }catch(error){
            return res.send({
                status:true,
                data:[]
            })
        }
    },
    deletecoupen:async (req,res)=>{
    const id=req.params.id;
    await CoupensModel.findByIdAndDelete(id).then((result)=>{
        return res.send({
        status:true,
        message:"distributer deleted successfuly"
        })
    })
    },
    deactivatecoupen:async(req,res)=>{
    const id=req.params.id;
    await CoupensModel.findByIdAndUpdate(id,{status:false}).then((result)=>{
        return res.send({
        status:true,
        message:"distributer status updated successfuly"
        })
    })
    },
      
    activatecoupen:async(req,res)=>{
    const id=req.params.id;
    await CoupensModel.findByIdAndUpdate(id,{status:true}).then((result)=>{
        return res.send({
        status:true,
        message:"distributer status updated successfuly"
        })
    })
    },
    activatecouponforproduct:async(req,res)=>{
        const coupon_id=req.body.coupon_id;
        const product_id=req.body.product_id;
        await CoupensModel.findByIdAndUpdate(coupon_id,{
            "$push":{products:product_id}
        }).then((result)=>{
            return res.send({
                status:true,
                message:"coupon activated for product"
            })
        })
        
    },
    makeapaymentrequest:async(req,res)=>{
        try{
            const provider_id=req.body.provider_id;
            const provider_name=req.body.provider_name;
            let note=req.body.note;
            const email=req.body.email
            const bank_id=req.body.bank_id;
            const amount=req.body.amount;
            const lawyer=await Seller.findById(provider_id);
            const pendingamount=lawyer.totalpendingamount;
           
            // let slr_record = await Seller.findOne({_id:seller_id},{banks:1});
            // console.log(slr_record);
            // return false;

            const pastPayRequests = await PaymentRequestmodel.find({provider_id:provider_id,payment_status:false,is_rejected:false});
            if(pastPayRequests.length)
            {
                return res.send({
                    status:false,
                    message:"La demande de paiement a déjà été faite, attendez d'abord que cette demande soit effacée."
                })
            }
            const totalrequestamount=pastPayRequests.reduce(function(accum,current){
            return accum+parseInt(current.request_amount)
            },0);
            // console.log("totalrequestamount",totalrequestamount,"pendingamount",pendingamount)
            if(amount<=pendingamount)
            {
                const newPayoutrequest=new PaymentRequestmodel();
                newPayoutrequest.provider_id=provider_id;
                newPayoutrequest.provider_name=provider_name;
                newPayoutrequest.request_amount=amount;
                newPayoutrequest.note=note;
                newPayoutrequest.bank_id=bank_id;
                newPayoutrequest.email=email;
                newPayoutrequest.save((result)=>{
                return res.send({
                    status:true,
                    message:"Demande de paiement effectuée avec succès",
                    data:result
                })
                })
            }else if(amount>pendingamount){
                return res.send({
                    status:false,
                    message:"Le montant demandé est supérieur au montant en attente",
                    errmessage:"Le montant demandé est supérieur au montant en attente"
                })
            }
        }catch(e){
            return res.send({
              status:false,
              message:e.message,
              errmessage:e.message
            });
        }
    },
    getallpaymentrequest:async(req,res)=>{
        const query={}
        const approved=req.body.approved;
        const pending=req.body.pending;
        const cancelled=req.body.cancelled;
        if(approved){
            query['payment_status']=true
        }
        if(pending){
            query['payment_status']=false
        }
        if(cancelled){
            query['is_rejected']=true
        }
        const pageno=req.body.pageno;
        const limit=10;
        
        
        const skip=limit*pageno;
    if(pageno==null||pageno=="null"){
        const count=(await PaymentRequestmodel.find(query))?.length

        const pages=Math.ceil(count/limit)
        return res.send({
            status:true,
            pages:pages
        })
    }
       
        await PaymentRequestmodel.aggregate([
            {$match:query},
            {$skip:skip},
            {$limit:limit}
        ]).then((result)=>{
            return res.send({
                status:true,
                data:result
            })
        })
    },
    getpaymentrequestbyvendor:async(req,res)=>{
        const id=req.params.id;
        await PaymentRequestmodel.find({provider_id:id}).then((result)=>{
            return res.send({
                status:true,
                data:result
            })
        })
    },
    getCountPaymentRequestVendor:async(req,res)=>{
        try{
            var seller_id = req.body.seller_id;
            if(!seller_id)
            {
                return res.send({
                    ststus:false,
                    message:"L'identifiant du vendeur est requis"
                });
            }
            //mongoose.set("debug",true);
            let responsee = await PaymentRequestmodel.count({provider_id:seller_id});
            const pages=Math.ceil(responsee/10)
            return res.send({
                status:true,
                message:"Success",
                data:pages
            });
        }catch(error)
        {
            return res.send({
                status: false,
                message: error.message
            })
        }
    },
    getPaymentRequestVendor:async(req,res)=>{
        try{
            var seller_id = req.body.seller_id;
            var page_no = req.body.page_no;
            if(!seller_id)
            {
                return res.send({
                    ststus:false,
                    message:"L'identifiant du vendeur est requis"
                });
            }
            page_no = page_no ? page_no :0;
            
            let limitt = 10;
            let skip = page_no * limitt;
            //let responsee = await PaymentRequestmodel.find({provider_id:seller_id}).skip(skip).limit(limitt).sort({"createdAt":-1});
            /////mongoose.set("debug",true);
            let responsee = await PaymentRequestmodel.aggregate([
                {
                    $match:{provider_id:seller_id}
                },
                {
                    $lookup:{
                        from:"sellers",
                        let:{"slr_id": {"$toObjectId":seller_id} },
                        pipeline:[
                            {
                                $match:{
                                    $expr:{
                                        $eq:["$_id","$$slr_id"]
                                    }
                                }
                            }
                        ],
                        as:"slr_rec"
                    }
                },
                {
                    $addFields:{
                        bankRec: {"$arrayElemAt":["$slr_rec.banks",0]}
                    }
                },
                {
                    $project:{
                        "slr_rec":0
                    }
                }
            ]);
            return res.send({
                status:true,
                message:"Success",
                data:responsee
            });
        }catch(error)
        {
            return res.send({
                status: false,
                message: error.message
            })
        }
    },
    markpaymentrequestasresolved:async(req,res)=>{
        try{
            const id=req.params.id;
            let get_re = await PaymentRequestmodel.findOne({_id:id});
            //console.log("get_re ", get_re);
            //return false;
            let amount = get_re.request_amount;

            Seller.findById(get_re.provider_id).then((doc)=>{
                //console.log("doc ", doc);
                //return false;
                if(doc.totalpendingamount!=0){
                  if(amount>doc.totalpendingamount){
                   return res.send({
                     status:false,
                     message:"Le montant saisi est supérieur au montant en attente, veuillez saisir un montant valide.",
                     errmessage:"Le montant saisi est supérieur au montant en attente, veuillez saisir un montant valide.",
                     data:null
                   })
                  }else{
                   if(doc?.totalpaidamount){
                     doc.totalpaidamount=parseInt(doc.totalpaidamount)+parseInt(amount);
                     doc.totalpendingamount=parseInt(doc.totalpendingamount)-parseInt(amount);
                    }else{
                     //  console.log(doc)
                     doc.totalpaidamount=parseInt(amount);
                     doc.totalpendingamount=parseInt(doc.totalpendingamount)-parseInt(amount);
                    }
                     
                     doc.save().then(async(result)=>{
                        await PaymentRequestmodel.findByIdAndUpdate(id,{payment_status:true}).then((result)=>{
                            return res.send({
                                status:true,
                                errorMessage:"Paiement marqué d'un succès complet",
                                data:result
                            })
                        })
                     })
                    }
                  }else{
                    return res.send({
                      status:false,
                      message:"Il n'y a plus de montant en attente",
                      errmessage:"Il n'y a plus de montant en attente",
                      data:null
                    })
                  }
                }).catch((err)=>{
                  console.log(err)
                  res.send({
                    status:false,
                    message:err.message
                  })
                })


            
        }catch(error)
        {
            return res.send({
                status: false,
                errorMessage: error.message
            })
        }
        
    },
    markpaymentrequestascanclled:async(req,res)=>{
        try{
            const id=req.params.id;
            await PaymentRequestmodel.findByIdAndUpdate(id,{is_rejected:true}).then((result)=>{
                return res.send({
                    status:true,
                    errorMessage:"Paiement marqué Rejeté Succès complet",
                    data:result
                })
            })
        }catch(error)
        {
            return res.send({
                status: false,
                errorMessage: error.message
            })
        }
        
    },
    // deletepaymentrequest:async(req,res)=>{
    //     const id=req.params.id;
    //     await PaymentRequestmodel.findByIdAndDelete(id).then((result)=>{
    //         return res.send({
    //             status:true,
    //             data:result
    //         })
    //     })
    // },
    getCommissionAmount:async(sellerid)=>{
        const seller=await Seller.findById(sellerid);
        const sub_id=seller.sub_id;
        const subs=await SubscriptionModel.findById(sub_id);
        if(subs){
            
        }else{
            return null
        }
    },
     getuserprofile:async(req,res)=>{
        await Seller.findById(req.params.id).then((result)=>{
            return res.send({
                status:true,
                data:result
            })
        })
        },
    sellerincome:async(req,res)=>{

        const start_date=req.body.start_date;
        const end_date=req.body.end_date;
        const query={};

        let totalearning = 0;
        const id=req.params.id;
        const seller=await Seller.findById(id);
        const pending_amount=seller?.totalpendingamount;
        const paid_amount=seller?.totalpaidamount;
        const allpaymentrequests=await PaymentRequestmodel.find({provider_id:id});
        const relesed=allpaymentrequests?.filter((e)=>e.payment_status==true);
        const toberelesed=allpaymentrequests?.filter((e)=>e.payment_status==false);

        if(start_date)
        {
            query["createdAt"]={
                $gte:new Date(start_date)
            }
            
        }if(end_date)
        {
            query["createdAt"]={
                $lte:new Date(end_date)
            }
        }else if(start_date && end_date)
        {
            query["createdAt"]={
                $gte:new Date(start_date),
                $lte:new Date(end_date)
            }
        }

        
        const getallorders = await orders.aggregate([
            //{$match:{status:1}},
            {$match:{products:{$elemMatch:{"product.provider_id":id } }}},
            {$match:{payment_status: true}},
            {
                $addFields:{
                    "sellerproducts":{"$map": {
                        "input": {
                          "$filter": {
                            "input": "$products",
                            "as": "product",
                            "cond": 
                                {
                                    $and:[
                                        {$eq:["$$product.product.provider_id",id]}
                                    ]
                                }
                                
                          }
                        },
                        "as": "sellerproducts1",
                        "in": {
                          "products": "$$sellerproducts1",
                          
                        }}}
                }
            },
            {
                $project:{
                    "products":0
                }
            }
        ]);
        let sumWithInitial = 0;
        let totalCancelEarning = 0;
        for(let x=0;  x<getallorders.length; x++)
        {
            for(let y=0; y<getallorders[x].sellerproducts.length; y++)
            {
                // console.log(getallorders[x].sellerproducts[y]);
                // console.log(getallorders[x].sellerproducts[y].products._id);
                // console.log(getallorders[x].sellerproducts[y].products.sellerprofit);
                totalearning = totalearning + getallorders[x].sellerproducts[y].products.sellerprofit;
                if(getallorders[x].sellerproducts[y].products.status == 2)
                {
                    totalCancelEarning = totalCancelEarning + getallorders[x].sellerproducts[y].products.sellerprofit;
                }
            }
        }
        //totalearning = sumWithInitial;
        
        // console.log("getallorders ", getallorders );
        // console.log("getallorders count ", getallorders.length );
        // return false;

        const curr = new Date; // get current date
        const first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
        const last = first + 6; // last day is the first day + 6
        const firstday = new Date(curr.setDate(first));
        const lastday = new Date(curr.setDate(last));
        // console.log("firstday ",firstday);
        // console.log("lastday ",lastday);
        const today=new Date();
        const thisweek={
            from:firstday,
            to:lastday
        }
        const thisweekorders=await orders.aggregate([
            {$match:{ createdAt:{$lte:thisweek.to,$gte:thisweek.from}}},
            {$match:{payment_status: true}},
            { $match:{ products:{ $elemMatch:{"product.provider_id":id  } } } },
            {
                $addFields:{
                    "sellerproducts":{"$map": {
                        "input": {
                          "$filter": {
                            "input": "$products",
                            "as": "product",
                            "cond": {$eq:["$$product.product.provider_id",id]}
                          }
                        },
                        "as": "sellerproducts1",
                        "in": {
                          "products": "$$sellerproducts1",
                          
                        }}}
                }
            },
            {
                $project:{
                    "products":0
                }
            }
        ]);
        let thisweekearnings = 0;
        for(let x=0;  x<thisweekorders.length; x++)
        {
            for(let y=0; y<thisweekorders[x].sellerproducts.length; y++)
            {
                // console.log(thisweekorders[x].sellerproducts[y]);
                // console.log(thisweekorders[x].sellerproducts[y].products._id);
                // console.log(thisweekorders[x].sellerproducts[y].products.sellerprofit);
                if(thisweekorders[x].sellerproducts[y].products.status != 2)
                {
                    thisweekearnings = thisweekearnings + thisweekorders[x].sellerproducts[y].products.sellerprofit;
                }
            }
        }

        const date_m = new Date(), y = date_m.getFullYear(), m = date_m.getMonth();
        const firstDay_m = new Date(y, m, 1);
        const lastDay_m = new Date(y, m + 1, 0);
        console.log("firstDay_m ",firstDay_m);
        console.log("lastDay_m ",lastDay_m);
        const thismonth={
            from:firstDay_m,
            to:lastDay_m
        }
        const thismonthorders = await orders.aggregate([
            {$match:{createdAt:{$lte:thismonth.to,$gte:thismonth.from}}},
            {$match:{payment_status: true}},
            { $match:{ products:{ $elemMatch:{"product.provider_id":id  } } } },
            {
                $addFields:{
                    "sellerproducts":{"$map": {
                        "input": {
                          "$filter": {
                            "input": "$products",
                            "as": "product",
                            "cond": {$eq:["$$product.product.provider_id",id]}
                          }
                        },
                        "as": "sellerproducts1",
                        "in": {
                          "products": "$$sellerproducts1",
                          
                        }}}
                }
            },
            {
                $project:{
                    "sellerproducts":1
                }
            }
        ]);
        //const thismonthearnings=thismonthorders?.sellerproducts?.reduce((acc,e)=>{return acc+e.sellerprofit},0)||0;
        let thismonthearnings = 0;
        for(let x=0;  x<thismonthorders.length; x++)
        {
            for(let y=0; y<thismonthorders[x].sellerproducts.length; y++)
            {
                // console.log(thismonthorders[x].sellerproducts[y]);
                // console.log(thismonthorders[x].sellerproducts[y].products._id);
                // console.log(thismonthorders[x].sellerproducts[y].products.sellerprofit);
                if(thismonthorders[x].sellerproducts[y].products.status != 2)
                {
                    thismonthearnings = thismonthearnings + thismonthorders[x].sellerproducts[y].products.sellerprofit;
                }
            }
        }
        //mongoose.set("debug",true);
        const paidorders=await orders.aggregate([
            { $match:{ products:{ $elemMatch:{"product.provider_id":id  } } } }, 
            {$match:{payment_status: true}},
            {
                $addFields:{
                    "sellerproducts":{"$map": {
                        "input": {
                          "$filter": {
                            "input": "$products",
                            "as": "product",
                            "cond": {      
                                $and:[
                                        {$eq:["$$product.product.provider_id",id]},
                                        {$eq:["$$product.status",1]},
                                            // {"$$product.is_paid":{$gt:null,$eq:true}}
                                    ]
                                }
                          }
                        },
                        "as": "sellerproducts1",
                        "in": {
                          "products": "$$sellerproducts1",
                          
                        }
                    }}
                }
            },
            
            // {
            //     $project:{
            //         "products":0
            //     }
            // }
        ]);
        //console.log("paidorders ", paidorders);
        let newpaidorders = paidorders.filter((val)=>{
            //console.log(val.sellerproducts);
            if(val.sellerproducts.length > 0)
            {
                return val;
            }
        })
        // const newpaidorders=paidorders?.filter((e)=>e.sellerproducts.length>0)
         
        const data={
            pending_amount,
            paid_amount,
            thisweekearnings:thisweekearnings,
            thismonthearnings:thismonthearnings,
            totalearning:totalearning,
            totalCancelEarning:totalCancelEarning,
            // allpaymentrequests,
            // relesed,
            paidorders:newpaidorders,
            // toberelesed
        }
// console.log("data",data)
        return res.send({
            status:true,
            message:"fetched",
            data:data
        })
    },
    sellerincomePostMethod:async(req,res)=>{

        const id=req.body.id;
        let start_date=req.body.start_date;
        let end_date=req.body.end_date;
        const query={};

        

        if(start_date)
        {
            start_date = new Date(new Date(start_date).setHours(0, 0, 0, 0)).toISOString();
            query["createdAt"]={
                $gte:new Date(start_date)
            }
            
        }if(end_date)
        {
            end_date = new Date(new Date(end_date).setHours(23, 59, 59, 999)).toISOString()
            query["createdAt"]={
                $lte:new Date(end_date)
            }
        }else if(start_date && end_date)
        {
            start_date = new Date(new Date(start_date).setHours(0, 0, 0, 0)).toISOString();
            end_date = new Date(new Date(end_date).setHours(23, 59, 59, 999)).toISOString()
            query["createdAt"]={
                $gte:new Date(start_date),
                $lte:new Date(end_date)
            }
        }

        
        
        //mongoose.set("debug",true);
        const paidorders=await orders.aggregate([
            {
                $match:query
            },
            { $match:{ products:{ $elemMatch:{"product.provider_id":id  } } } }, 
            {$match:{payment_status: true}},
            {
                $addFields:{
                    "sellerproducts":{"$map": {
                        "input": {
                          "$filter": {
                            "input": "$products",
                            "as": "product",
                            "cond": {      
                                $and:[
                                        {$eq:["$$product.product.provider_id",id]},
                                        {$eq:["$$product.status",1]},
                                            // {"$$product.is_paid":{$gt:null,$eq:true}}
                                    ]
                                }
                          }
                        },
                        "as": "sellerproducts1",
                        "in": {
                          "products": "$$sellerproducts1",
                          
                        }
                    }}
                }
            },
            
            // {
            //     $project:{
            //         "products":0
            //     }
            // }
        ]);
        //console.log("paidorders ", paidorders);
        let newpaidorders = paidorders.filter((val)=>{
            //console.log(val.sellerproducts);
            if(val.sellerproducts.length > 0)
            {
                return val;
            }
        })
        // const newpaidorders=paidorders?.filter((e)=>e.sellerproducts.length>0)
         
        const data={
            paidorders:newpaidorders,
            // toberelesed
        }
// console.log("data",data)
        return res.send({
            status:true,
            message:"fetched",
            data:data
        })
    },
    sellerincome_dashboard_page:async(req,res)=>{
        let totalearning = 0;
        const id=req.params.id;
        let numberofdaysarray = [];
        let totalearningsarray = [];
        if(!id)
        {
            return res.send({
                status:false,
                message:"L'identifiant est requis"
            })
        }

        const seller=await Seller.findById(id);
        const pending_amount=seller?.totalpendingamount;
        const paid_amount=seller?.totalpaidamount;
        const totalUploadedProduct = await Product.count({provider_id:id,is_deleted: false});
        //console.log("totalUploadedProduct ",totalUploadedProduct);
        const totalpageViews=(await pageViewsModel.findOne({provider_id:id}))?.count||0;
        
        const allpaymentrequests=await PaymentRequestmodel.find({provider_id:id});
        const relesed=allpaymentrequests?.filter((e)=>e.payment_status==true);
        const toberelesed=allpaymentrequests?.filter((e)=>e.payment_status==false);
        //mongoose.set("debug",true); 
        const getallorders = await orders.aggregate([
            //{$match:{status:1}},
            {$match:{products:{$elemMatch:{"product.provider_id":id } }}},
            {$match:{payment_status: true}},
            {
                $addFields:{
                    "sellerproducts":{"$map": {
                        "input": {
                          "$filter": {
                            "input": "$products",
                            "as": "product",
                            "cond": 
                                {
                                    $and:[
                                        {$eq:["$$product.product.provider_id",id]}
                                    ]
                                }
                                
                          }
                        },
                        "as": "sellerproducts1",
                        "in": {
                          "products": "$$sellerproducts1",
                          
                        }}}
                }
            },
            {
                $project:{
                    "products":0
                }
            }
        ]);
        let sumWithInitial = 0;
        let totalCancelEarning = 0;
        let completedorders = 0;
        var cancelledorders = 0;
        var proccessingorders = 0;
        var refundedorders = 0;
        const totoalnumberoforders = getallorders.length;
        for(let x=0;  x<getallorders.length; x++)
        {
            var vv = 0;
            var mm_n = 0;
            var pp_q = 0;
            var zz_x=0;
            for(let y=0; y<getallorders[x].sellerproducts.length; y++)
            {
                // console.log(getallorders[x].sellerproducts[y]);
                // console.log(getallorders[x].sellerproducts[y].products._id);
                // console.log(getallorders[x].sellerproducts[y].products.sellerprofit);
                totalearning = totalearning + getallorders[x].sellerproducts[y].products.sellerprofit;

                //console.log("getallorders[x].sellerproducts[y].products.status ", getallorders[x].sellerproducts[y].products.status);
                //console.log("delivery_status ", getallorders[x].sellerproducts[y].products.delivery_status);
                
                if(getallorders[x].sellerproducts[y].products.status == 2)
                {
                    //console.log("cancel order ");
                    totalCancelEarning = totalCancelEarning + getallorders[x].sellerproducts[y].products.sellerprofit;
                    cancelledorders = cancelledorders  + 1;
                }

                if(getallorders[x].sellerproducts[y].products.status == 1)
                {
                    //console.log("completedorders ", getallorders[x].sellerproducts[y].products.status);
                    completedorders = completedorders + 1;
                }

                if(getallorders[x].sellerproducts[y].products.status == 0 )
                {
                    //console.log("processing order ");
                    proccessingorders = proccessingorders + 1;
                }
                if(getallorders[x].sellerproducts[y].products.status == 6)
                {
                    refundedorders = refundedorders + 1;
                }
                
                
            }
        }
        //totalearning = sumWithInitial;
        
        // console.log("getallorders ", getallorders );
        // console.log("completedorders ", completedorders );
        // return false;
        //console.log("cancelledorders ", cancelledorders );
        
        const curr = new Date; // get current date
        const first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
        const last = first + 6; // last day is the first day + 6
        const firstday = new Date(curr.setDate(first));
        const lastday = new Date(curr.setDate(last));
        // console.log("firstday ",firstday);
        // console.log("lastday ",lastday);
        const today=new Date();
        const thisweek={
            from:firstday,
            to:lastday
        }
        const thisweekorders=await orders.aggregate([
            {$match:{ createdAt:{$lte:thisweek.to,$gte:thisweek.from}}},
            {$match:{payment_status: true}},
            { $match:{ products:{ $elemMatch:{"product.provider_id":id  } } } },
            {
                $addFields:{
                    "sellerproducts":{"$map": {
                        "input": {
                          "$filter": {
                            "input": "$products",
                            "as": "product",
                            "cond": {$eq:["$$product.product.provider_id",id]}
                          }
                        },
                        "as": "sellerproducts1",
                        "in": {
                          "products": "$$sellerproducts1",
                          
                        }}}
                }
            },
            {
                $project:{
                    "products":0
                }
            }
        ]);
        let thisweekearnings = 0;
        for(let x=0;  x<thisweekorders.length; x++)
        {
            for(let y=0; y<thisweekorders[x].sellerproducts.length; y++)
            {
                // console.log(thisweekorders[x].sellerproducts[y]);
                // console.log(thisweekorders[x].sellerproducts[y].products._id);
                // console.log(thisweekorders[x].sellerproducts[y].products.sellerprofit);
                if(thisweekorders[x].sellerproducts[y].products.status != 2)
                {
                    thisweekearnings = thisweekearnings + thisweekorders[x].sellerproducts[y].products.sellerprofit;
                }
            }
        }

        const date_m = new Date(), y = date_m.getFullYear(), m = date_m.getMonth();
        const firstDay_m = new Date(y, m, 1);
        const lastDay_m = new Date(y, m + 1, 0);
        // console.log("firstDay_m ",firstDay_m);
        // console.log("lastDay_m ",lastDay_m);
        const thismonth={
            from:firstDay_m,
            to:lastDay_m
        }
        const thismonthorders = await orders.aggregate([
            {$match:{createdAt:{$lte:thismonth.to,$gte:thismonth.from}}},
            {$match:{payment_status: true}},
            { $match:{ products:{ $elemMatch:{"product.provider_id":id  } } } },
            {
                $addFields:{
                    "sellerproducts":{"$map": {
                        "input": {
                          "$filter": {
                            "input": "$products",
                            "as": "product",
                            "cond": {$eq:["$$product.product.provider_id",id]}
                          }
                        },
                        "as": "sellerproducts1",
                        "in": {
                          "products": "$$sellerproducts1",
                          
                        }}}
                }
            },
            {
                $project:{
                    "sellerproducts":1
                }
            }
        ]);
        //const thismonthearnings=thismonthorders?.sellerproducts?.reduce((acc,e)=>{return acc+e.sellerprofit},0)||0;
        let thismonthearnings = 0;
        for(let x=0;  x<thismonthorders.length; x++)
        {
            for(let y=0; y<thismonthorders[x].sellerproducts.length; y++)
            {
                // console.log(thismonthorders[x].sellerproducts[y]);
                // console.log(thismonthorders[x].sellerproducts[y].products._id);
                // console.log(thismonthorders[x].sellerproducts[y].products.sellerprofit);
                if(thismonthorders[x].sellerproducts[y].products.status != 2)
                {
                    thismonthearnings = thismonthearnings + thismonthorders[x].sellerproducts[y].products.sellerprofit;
                }
            }
        }
        const paidorders=await orders.aggregate([
            { $match:{ products:{ $elemMatch:{"product.provider_id":id  } } } }, 
            {$match:{payment_status: true}},
            {
                $addFields:{
                    "sellerproducts":{"$map": {
                        "input": {
                          "$filter": {
                            "input": "$products",
                            "as": "product",
                            "cond": {      
                                $and:[
                                        {$eq:["$$product.product.provider_id",id]},
                                        {$eq:["$$product.status",1]},
                                            // {"$$product.is_paid":{$gt:null,$eq:true}}
                                    ]
                                }
                          }
                        },
                        "as": "sellerproducts1",
                        "in": {
                          "products": "$$sellerproducts1",
                          
                        }}}
                }
            },
            
            {
                $project:{
                    "products":0
                }
            }
        ]);
        let newpaidorders = paidorders.filter((val)=>{
            //console.log(val.sellerproducts);
            if(val.sellerproducts.length > 0)
            {
                return val;
            }
        })
        // const newpaidorders=paidorders?.filter((e)=>e.sellerproducts.length>0)
         
         
        let currDate = new Date(y, m, 1);
        let daydateto30 = new Date(y, m + 1, 0);

        let numberofdays = getDays(currDate.getFullYear(), currDate.getMonth() + 1);

        const monthrange = {
            $and: [
                {
                    createdAt: {
                        $gt: currDate
                    }
                },
                {
                    createdAt: {
                        $lte: daydateto30
                    }
                }
            ]
        };
        
        const totalordersthismonth = await orders.aggregate([
            { $match:{ products:{ $elemMatch:{"product.provider_id":id  } } } }, 
            { $match:{ createdAt: {  $gt: currDate  } } },
            { $match:{ createdAt: {  $lte: daydateto30  } } },
            {$match:{payment_status: true } },
            {
                $addFields:{
                    "sellerproducts":{"$map": {
                        "input": {
                          "$filter": {
                            "input": "$products",
                            "as": "product",
                            "cond": {$eq:["$$product.product.provider_id",id]}
                          }
                        },
                        "as": "sellerproducts1",
                        "in": {
                          "products": "$$sellerproducts1",
                          
                        }}}
                }
            }
        ]);
        for (let i = 1; i <= numberofdays; i++)
        {
            let datetocompare = getDate(currDate.getFullYear(), currDate.getMonth() + 1, i);
            numberofdaysarray.push(i);
    
            //totalearning
            let totalearningonthisdate = totalordersthismonth.filter((e) => {
                // console.log("e.createdAt.toString().split('T')[0]==datetocompare.toString().split('T')[0]",e.createdAt.toString().split("T")[0],datetocompare.toString().split("T")[0])
                return e.createdAt.toISOString().split("T")[0] == datetocompare.toISOString().split("T")[0]
            });
            //console.log("totalearningonthisdate ",totalearningonthisdate);
            if(totalearningonthisdate.length)
            {
                
                //products
                let totalearningonthisdateprice = 0;
                let pro_ern = 0;
                for(let x=0;  x<totalearningonthisdate.length; x++)
                {
                    for(let y=0; y<totalearningonthisdate[x].products.length; y++)
                    {
                        // console.log("here line n0 2074 ",totalearningonthisdate[x].products[y]);
                        // console.log(totalearningonthisdate[x].products[y].products._id);

                       // console.log(totalearningonthisdate[x].sellerproducts[y].products.sellerprofit);

                        if(totalearningonthisdate[x].sellerproducts[y].products.status != 2)
                        {
                            totalearningonthisdateprice = totalearningonthisdateprice + totalearningonthisdate[x].sellerproducts[y].products.sellerprofit;
                        }
                    }
                }
                totalearningsarray.push(totalearningonthisdateprice)

            }else{
                totalearningsarray.push(0)
            }
            // if (totalearningonthisdate.length) {
            //     let totalearningonthisdateprice = totalearningonthisdate.reduce((accum, e) => accum + e.totalPrice, 0);
            //     totalearningsarray.push(totalearningonthisdateprice)
            // } else {
            //     totalearningsarray.push(0)
            // }
    
    
        }
        const orderspie = [
            totoalnumberoforders,
            completedorders,
            proccessingorders,
            cancelledorders,
            refundedorders
            ]
           // console.log("orderspie ", orderspie);
        ///console.log("totalordersthismonth ",totalordersthismonth );
        //return false;
        const data={
            // pending_amount,
            // paid_amount,
            // thisweekearnings:thisweekearnings,
            // thismonthearnings:thismonthearnings,
            // totalearning:totalearning,
            // totalCancelEarning:totalCancelEarning,
            // // allpaymentrequests,
            // // relesed,
            // paidorders:newpaidorders,
            // // toberelesed,
            totalUploadedProduct:totalUploadedProduct,
            totalpageViews:totalpageViews,
            totoalnumberoforders:totoalnumberoforders,
            totalearnings:totalearning,
            totalgains:paid_amount,
            numberofdaysarray:numberofdaysarray,
            totalearningsarray:totalearningsarray,
            orderspie:orderspie
        }
        // console.log("data",data)
        return res.send({
            status:true,
            message:"fetched",
            data:data
        })
    },
    sellerbalance:async(req,res)=>{
        const start_date=req.body.start_date;
        const end_date=req.body.end_date;
        const id=req.body.id;
        const seller=await Seller.findById(id);
        const accounts=seller?.banks;
        const pending_amount=seller?.totalpendingamount;
        const paid_amount=seller?.totalpaidamount;
        const allpaymentrequests=await PaymentRequestmodel.find({provider_id:id});
        //{ $match:{ products:{ $elemMatch:{"product.provider_id":id  } } } }, 
        const query={products:{$elemMatch:{"product.provider_id":id} }};
        const query1={status:0}
        if(start_date)
        {
            start_date = new Date(new Date(start_date).setHours(0, 0, 0, 0)).toISOString();
            query["createdAt"]={
                $gte:new Date(start_date)
            }
            
        }if(end_date)
        {
            end_date = new Date(new Date(end_date).setHours(23, 59, 59, 999)).toISOString()
            query["createdAt"]={
                $lte:new Date(end_date)
            }
        }else if(start_date && end_date)
        {
            start_date = new Date(new Date(start_date).setHours(0, 0, 0, 0)).toISOString();
            end_date = new Date(new Date(end_date).setHours(23, 59, 59, 999)).toISOString()
            query["createdAt"]={
                $gte:new Date(start_date),
                $lte:new Date(end_date)
            }
        }
        let getallorders = await orders.aggregate([
            {$match:query},
            {
                $addFields:{
                    "sellerproducts":{"$map": {
                        "input": {
                          "$filter": {
                            "input": "$products",
                            "as": "product",
                            "cond": {$eq:["$$product.product.provider_id",id]}
                          }
                        },
                        "as": "sellerproducts1",
                        "in": {
                          "products": "$$sellerproducts1",
                          
                        }}}
                }
            },
            
            {
                $project:{
                    "products":0
                }
            }
        ]);
         
        const query2={products:{$elemMatch:{"product.provider_id":id} }}
        let getallorderspending = await orders.aggregate([
            {$match:query2},
            {
                $addFields:{
                    "sellerproducts":{"$map": {
                        "input": {
                          "$filter": {
                            "input": "$products",
                            "as": "product",
                            // "cond": {$eq:["$$product.product.provider_id",seller_id]}
                            "cond": {      
                                $and:[
                                         {$eq:["$$product.product.provider_id",id]},
                                         {$eq:["$$product.is_paid",false]},
                                            // {"$$product.is_paid":{$gt:null,$eq:true}}
                                    ]
                                }
                          }
                        },
                        "as": "sellerproducts1",
                        "in": {
                          "products": "$$sellerproducts1",
                          
                        }}}
                }
            },
            
            {
                $project:{
                    "products":0
                }
            }
        ]);
        getallorderspending=getallorderspending?.filter((e)=>e.sellerproducts.length>0)
        const totalearning=getallorders?.sellerproducts?.reduce((acc,e)=>{return acc+e.sellerprofit},0)
        const totalpending=getallorderspending?.sellerproducts?.reduce((acc,e)=>{return acc+e.sellerprofit},0)
      
        
       
       
        const data={
            pending_amount,
            paid_amount,
            totalearning,
            allpaymentrequests,
            //getallorderspending,
            //getallorders,
            accounts
        }

        return res.send({
            status:true,
            message:"fetched",
            data:data
        })
    },
    getallbanks:async(req,res)=>{
        const id=req.params.id;
        const seller=await Seller.findById(id);
        const accounts=seller?.banks;
        return res.send({
            status:true,
            message:"fetched",
            data:accounts
        })
    },
    getbankbybankid:async(req,res)=>{
        const id=req.body.id;
        const bank_id=req.body.bank_id;
        const seller=await Seller.findById(id);
        const account=seller?.banks.filter((e)=>e._id+''==bank_id);
        return res.send({
            status:true,
            message:"fetched",
            data:{
                account:account,
                email:seller?.email
            }
        })
    },
    updatebank:async(req,res)=>{
        const {
           
            email,
           
            id
        } = req.body;
        const query = { email:olduser.email };
        const options1 = {
          arrayFilters: [
            {
                "item._id":newid
             
            },
          ],
        };
       
        const result1 = await User.deleteOne(query, options1);
        console.log("result1",result1)
        return res.send({
            status:true,
            message:"success"
        })
    },
    deletebank:async(req,res)=>{
        const email=req.body.email;
        const addressid=req.body.bank_id;
        // const newid=mongoose.Types.ObjectId(addressid)
        
        const banks=(await Seller.findOne({email:email}))?.banks;
        let addresstodelete=banks?.filter((e)=>e._id+""==addressid);
        let updateaddressarray=banks?.filter((e)=>e._id+""!=addressid);
       if(updateaddressarray?.length>0){
        await Seller.updateOne({email:email}, {banks:updateaddressarray});
       
       
       }
       return res.send({
        status:true,
        message:"success"
    })
    },
    markbankasdefault:async(req,res)=>{
        const email=req.body.email;
        const addressid=req.body.bankid;
        console.log("req.body",req.body)
        const query = { email:email };
        const newid=mongoose.Types.ObjectId(addressid)
        const updateDocument1 = {
            "banks.$[item].is_default": false 
        };
        const options1 = {
          arrayFilters: [
            {
                "item._id":{$ne:newid}
             
            },
          ],
        };
        const updateDocument2 = {
            "banks.$[item].is_default": true 
        };
        const options2 = {
          arrayFilters: [
            {
                "item._id":{$eq:newid}
             
            },
          ],
        };
        const result1 = await Seller.updateOne(query, updateDocument1, options1);
        const result2 = await Seller.updateOne(query, updateDocument2, options2);
        console.log("result1",result1,"result2",result2)
        return res.send({
            status:true,
            message:"success"
        })
    
    },
    makepaymentrequest:async(req,res)=>{
        const {
            seller_id,
            seller_name,
            email,
            bank_id
        }=req.body;

      
        const query1={status:1}
        let getallorderspending = await orders.aggregate([
            {$match:query1},
            {
                $addFields:{
                    "sellerproducts":{"$map": {
                        "input": {
                          "$filter": {
                            "input": "$products",
                            "as": "product",
                            // "cond": {$eq:["$$product.product.provider_id",seller_id]}
                            "cond": {      
                                $and:[
                                         {$eq:["$$product.product.provider_id",seller_id]},
                                         {$eq:["$$product.is_paid",false]},
                                            // {"$$product.is_paid":{$gt:null,$eq:true}}
                                    ]
                                }
                          }
                        },
                        "as": "sellerproducts1",
                        "in": {
                          "products": "$$sellerproducts1",
                          
                        }}}
                }
            },
            
            {
                $project:{
                    "products":0
                }
            }
        ]);
        getallorderspending=getallorderspending?.filter((e)=>e.sellerproducts.length>0)
        console.log("getallorderspending",getallorderspending)
        const orderids=getallorderspending.map(e=>e._id);
        let totalpendingamount=0;
        for(let i=0;i<getallorderspending.length;i++){
            let order=getallorderspending[i];
            let sellerproducts=order?.sellerproducts
            for(let j=0;j<sellerproducts.length;j++){
                let singleproduct=sellerproducts[j];
                totalpendingamount+=singleproduct.products.sellerprofit
                await orders.updateOne({_id:order._id},{
                    "products.$[item].is_paid":true
                }, {
                    arrayFilters: [
                      {
                          "item._id":singleproduct.products._id
                       
                      },
                    ],
                  })
            }
        }
        if(totalpendingamount){
            const newPayoutrequest=new PaymentRequestmodel();
            newPayoutrequest.provider_id=seller_id;
            newPayoutrequest.email=email;
            newPayoutrequest.provider_name=seller_name;
            newPayoutrequest.bank_id=bank_id;
            newPayoutrequest.orders=orderids;
            newPayoutrequest.request_amount=totalpendingamount;
            newPayoutrequest.save((result)=>{
              return res.send({
                status:true,
                message:"Demande de paiement effectuée avec succès",
                data:result
              })
              })
        }else{
            return res.send({
                status:false,
                message:"nothing to demand here,make some damn sells first"
            })
        }
    },
    demandpayment:async(req,res)=>{
        try{
          const provider_id=req.body.provider_id;
          const provider_name=req.body.provider_name;
        const amount=req.body.amount;
        const email=req.body.email;
        const lawyer=await lawyermodel.findById(provider_id);
        const pendingamount=lawyer.totalpendingamount;
      const pastPayRequests=await PaymentRequestmodel.find({provider_id:provider_id,payment_status:false,is_rejected:false});
      const totalrequestamount=pastPayRequests.reduce(function(accum,current){
        return accum+parseInt(current.request_amount)
      },0);
      console.log("totalrequestamount",totalrequestamount,"pendingamount",pendingamount)
      if(amount<=pendingamount-totalrequestamount){
      const newPayoutrequest=new PaymentRequestmodel();
      newPayoutrequest.provider_id=provider_id;
      newPayoutrequest.email=email;
      newPayoutrequest.provider_name=provider_name;
      newPayoutrequest.request_amount=amount;
      newPayoutrequest.save((result)=>{
        return res.send({
          status:true,
          message:"Demande de paiement effectuée avec succès",
          data:result
        })
      })
      }else if(amount>pendingamount-totalrequestamount){
       return res.send({
        status:false,
        message:"",
        errmessage:"Le montant demandé est supérieur au montant en attente"
       })
      }
        }catch(e){
          return res.send({
            status:false,
            message:"",
            errmessage:e.message
           })
        }
      },
      
    getalldemandpayment:async(req,res)=>{
        const paymentrequest=await PaymentRequestmodel.find({});
        return res.send({
          status:true,
          message:"fetched successfylly",
          data:paymentrequest
        })
      },

      getallcouponsforuser:async(req,res)=>{
        const id=req.params.id;
        await CoupensModel.aggregate([
            {$match:{status:false,end_date:{$lte:new Date()}}},
        
            {
                $lookup:{
                    from:"products",
                    let:{products:"$products"},
                    pipeline:[
                        {$match:{
                            $expr:{
                                "products":{$elemMatch:{$eq:"_id"}}
                            }
                        }}
                    ],
                    as:"products"
                }
            }
        ]).then((result)=>{
            return res.send({
            status:true,
            data:result
            })
        })
      },
    
    submitSellerContactForm:async(req,res)=>{
        try{
            var {full_name,email,phone,message} = req.body;
            let datatosave = {
                ...(full_name&&{full_name:full_name}),
                ...(email&&{email:email}),
                ...(phone&&{phone:phone}),
                ...(message&&{message:message}),
            }
            console.log("datatosave 1738  -->>>  ",datatosave)
            await SellerContactModel.create(datatosave).then((result) => {
                return res.send({
                    status: true,
                    message: "Votre message a été envoyé avec succès"
                })
            }).catch((e) => {
                return res.send({
                    status: false,
                    message: e.message
                })
            })            
        }catch(error)
        {
            return res.send({
                status:false,
                message:error,
                data:null
            });
        }
    },
    
    sellerForgotPassword:async (req,res)=>
    {
        var {email} = req.body;
        if(email == "" || email == null || email == undefined|| email == "null" || email == "undefined" )
        {
            return res.send({
                status:false,
                message:"L'adresse électronique est requise"
            });
        }
        //const oldUser = await Seller.findOne({ email }); 
        const seller_record = await Seller.findOne({email:email,is_deleted:false},{fullname:1});
        console.log("seller_record -->>> "+ seller_record);
        if(seller_record)
        {
            var date_time = new Date();
             
            await Seller.updateOne({_id:seller_record._id},{
                forgot_pw_date:date_time
            }).then((result)=>{
                //console.log("result 1788 hereeee --->>>> "+JSON.stringify(result));
                var lastInsertId = seller_record._id;
                var email_verify_url = customConstant.base_url_web_site+"resetPassword/"+lastInsertId;
                html = "";
                html+= "Vous pouvez réinitialiser votre mot de passe, nous avons fourni un lien pour changer votre mot de passe, et il expirera dans 24 heures à partir d'ici cliquez sur le lien ci-dessous.";
                html+= '<a href="'+email_verify_url+'">'+' Cliquez ici '+'</strong>';
                const message = {
                    from: process.env.MAILER_FROM_EMAIL_ID, // Sender address
                    to: email,         // recipients
                    subject: "Mot de passe oublié O'Black", // Subject line
                    html: html
                  };
                  transport.sendMail(message, function(err, info) {
                    if (err)
                    {
                      console.log("email sending error ->>> "+err);
                        var return_response = {
                            status:true,
                            message:"Le courrier n'a pas été envoyé contact avec l'administrateur",
                        };
                        res.status(200)
                        .send(return_response);
                    }else{
                        console.log('mail has sent.');
                        //console.log(info);
                        var return_response = {
                            status:true,
                            message:"Veuillez vérifier votre courrier électronique et réinitialiser votre mot de passe"
                        };
                        res.status(200)
                        .send(return_response);
                    }
                  });
            }).catch((e)=>{
                return res.send({
                    status: false,
                    message: e.message
                });
            });
        }else{
            return res.send({
                status:false,
                message:"Identité électronique invalide"
            });
        }
    },
    submitForgotPassword:async(req,res)=>
    {
        try{
            var {id,new_password} = req.body;
            
            if(id == "" || id == null || new_password == "" || new_password == null)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant et le mot de passe sont tous deux nécessaires"
                });
            }else{
                var encryptedPassword = await bycrypt.hash(new_password, 10);
                const seller = await Seller.findById({_id:id},{forgot_pw_date:1});
                if(seller)
                {
                    console.log("seller "+seller);
                    var current_date = new Date();
                    var hours = Math.abs(current_date.getTime() - new Date(seller.forgot_pw_date).getTime()) / 3600000;
                    console.log("hours "+hours);
                    hours = parseInt(hours);
                    if(hours < 24)
                    {
                        await Seller.updateOne({_id:seller._id},{password:encryptedPassword}).then((result)=>{
                            return res.send({
                                status:true,
                                message:"Votre mot de passe a été mis à jour avec succès. Vous pouvez vous connecter avec ce mot de passe."
                            });
                        }).catch((error)=>{
                            return res.send({
                                status:false,
                                message:error
                            });
                        });
                    }else{
                        return res.send({
                            status:false,
                            message:"Le lien a expiré, vous pouvez demander à nouveau le mot de passe oublié."
                        });
                    }
                }else{
                    return res.send({
                        status:false,
                        message:"Donnée non valide"
                    });
                }
            }
        }catch(error)
        {
            return res.send({
                status:false,
                message:error
            });
        }
    },
    get_terms_condition:async(req,res)=>{
        try{
            const seller_terms= await SellertermsconditionModel.find();
            return res.send({
                status:true,
                message:"Succès",
                data:seller_terms,
            });
        }catch(error)
        {
            return res.send({
                status:false,
                message:error
            });
        }
    },
    get_seller_product:async(req,res)=>{
        try{
            //mongoose.set("debug",true);
            var id = req.params.id;
            //console.log(id);
            let skipp = 0;
            let limitt = 2000000;
            var seller_rec = await Seller.aggregate([
                { $match: { _id:  mongoose.Types.ObjectId(id) } }
            ]);
            var product_rec = await Product.aggregate([
                { $match: { provider_id:id } },
                {
                    $match:{
                        is_deleted: false
                    }
                },
                {
                  $lookup:{
                      from:"ratingnreviews",
                      let:{"prod_id":{"$toString":"$_id"}},
                      //let:{productidstring:{"$toString":"$_id"}},
                      pipeline:[ 
                          {
                              $match:{
                                  $expr:{
                                      $eq:["$product_id","$$prod_id"]
                                  }
                              }
                          } 
                      ],
                      as:"reviews"
                  },
                  
                },
                {
                  $addFields:{
                    "totalRating":{$size:"$reviews"}
                  }
                },
                //{ $match:{ "reviews":{$ne:[] } } },
                { $addFields: {
                                    
                    avgRating:{
                        $avg: "$reviews.rating"
                    } 
                }},
                {
                    $project:{
                        "reviews":0
                    }
                },
                {$skip:skipp},
                {
                    $limit:limitt
                }

            ]).then((result)=>{
                if(result.length > 0)
                {
                    seller_rec.push({"product":result});
                    //console.log("seller_rec "+JSON.stringify(seller_rec));
                    return res.send({
                        status:true,
                        message:"success",
                        data:seller_rec
                    });
                }else{
                    //seller_rec.push({"product":result});
                    //console.log("seller_rec "+JSON.stringify(seller_rec));
                    return res.send({
                        status:false,
                        message:"success",
                        data:[]
                    });
                }
                
            }).catch((error)=>{
                return res.send({
                    status:false,
                    message:error.message
                });
            })
            

        }catch(error)
        {
            return res.send({
                status:false,
                message:error
            });
        }
    },
    get_seller_product_POST:async(req,res)=>{
        try{
            //mongoose.set("debug",true);
            var id = req.body.id;
            var id = req.body.pageno;
            let skipp = pageno;
            let limitt = 20;
            console.log(id);
            var seller_rec = await Seller.aggregate([
                { $match: { _id:  mongoose.Types.ObjectId(id) } }
            ]);
            var product_rec = await Product.aggregate([
                { $match: { provider_id:id } },
                {
                    $match:{
                        is_deleted: false
                    }
                },
                {
                  $lookup:{
                      from:"ratingnreviews",
                      let:{"prod_id":{"$toString":"$_id"}},
                      //let:{productidstring:{"$toString":"$_id"}},
                      pipeline:[ 
                          {
                              $match:{
                                  $expr:{
                                      $eq:["$product_id","$$prod_id"]
                                  }
                              }
                          } 
                      ],
                      as:"reviews"
                  },
                  
                },
                {
                  $addFields:{
                    "totalRating":{$size:"$reviews"}
                  }
                },
                //{ $match:{ "reviews":{$ne:[] } } },
                { $addFields: {
                                    
                    avgRating:{
                        $avg: "$reviews.rating"
                    } 
                }},
                {
                    $project:{
                        "reviews":0
                    }
                },
                {$skip:skipp},
                {
                    $limit:limitt
                }

            ]).then((result)=>{
                if(result.length > 0)
                {
                    seller_rec.push({"product":result});
                    //console.log("seller_rec "+JSON.stringify(seller_rec));
                    return res.send({
                        status:true,
                        message:"success",
                        data:seller_rec
                    });
                }else{
                    //seller_rec.push({"product":result});
                    //console.log("seller_rec "+JSON.stringify(seller_rec));
                    return res.send({
                        status:false,
                        message:"success",
                        data:[]
                    });
                }
                
            }).catch((error)=>{
                return res.send({
                    status:false,
                    message:error.message
                });
            })
            

        }catch(error)
        {
            return res.send({
                status:false,
                message:error
            });
        }
    },
    get_seller_product_2:async(req,res)=>{
        try{
            //mongoose.set("debug",true);
            var id = req.params.id;
            console.log(id);
            var seller_rec = await Seller.aggregate([
                { $match: { _id:  mongoose.Types.ObjectId(id) } },
                {
                    $lookup:{
                        from:"products",
                        let:{"pro_id":{"$toString":"$_id"} },
                        pipeline:[
                            {
                                $match:{
                                    $expr:{
                                        $eq:["$provider_id","$$pro_id"]
                                    }
                                }
                            }
                        ],
                        as:"product"
                    }
                }
            ]);
            // var product_rec = await Product.aggregate([
            //     { $match: { provider_id:id } }
            // ]);
            // seller_rec.push({"product":product_rec});
            //console.log("seller_rec "+JSON.stringify(seller_rec));
            return res.send({
                status:true,
                message:"success",
                data:seller_rec
            });

        }catch(error)
        {
            return res.send({
                status:false,
                message:error
            });
        }
    },
    get_seller_coupon:async(req,res)=>{
        try{
            //mongoose.set("debug",true);
            var id = req.params.id;
            console.log(id);
            
            // { $match: { provider_id:  id }
                // },
            var seller_rec = await CoupensModel.aggregate([
                {$match:{status:true,end_date:{$gte:new Date()}}},
                {
                    $match:{products:{ $exists:true, $not:{$size:0} }}
                }
            ]);
            for(let x=0; x<seller_rec.length; x++)
            {
                var products = [];
                for(let m=0; m<seller_rec[x].products.length; m++)
                {
                    var product_rec = await Product.aggregate([
                        {$match:
                            { _id: mongoose.Types.ObjectId(seller_rec[x].products[m]) }
                        },
                        {
                            $project:{
                                "title":1,
                                "images":1,
                                
                            }
                        }
                    ]);
                    //console.log("product_rec"+JSON.stringify(product_rec));
                    //seller_rec.push(product_rec[0]);
                    if(product_rec.length>0)
                    {
                        //let aa = product_rec.length ? product_rec[0]:"";
                        let aa = product_rec[0]
                        products.push(aa);
                    }
                }
                //seller_rec[x]["products"].push({"products":products});
                seller_rec[x]["products_rec"] = products;
                
            }
            
            return res.send({
                status:true,
                message:"success",
                data:seller_rec
            });

        }catch(error)
        {
            console.log(error);
            return res.send({
                status:false,
                message:error
            });
        }
    },
    submitSellerQuery:async(req,res)=>{
        try{
            console.log(req.body);
            console.log(req.files);
            const {seller_id,request_purpose,reason,message} = req.body;
            if(seller_id == "" || seller_id == null || seller_id == "null")
            {
                return res.send({
                    status: false,
                    message: "Identité du vendeur requise",
                })
            }
            var pic = req.files.photo ? req.files.photo[0].filename : null;
            var datatosave = {
                ...(seller_id&&{seller_id:seller_id}),
                ...(request_purpose&&{request_purpose:request_purpose}),
                ...(reason&&{reason:reason}),
                ...(message&&{message:message}),
                ...(pic&&{photo:pic})
            };
            await SellerQueryModel.create(datatosave).then((result) => {
                return res.send({
                    status: true,
                    message: "Votre demande soumise",
                })
            }).catch((e) => {
                return res.send({
                    status: false,
                    message: e.message
                })
            })
        }catch(error)
        {
            return res.send({
                status:false,
                message:error
            });
        }
    },
    addNote:async(req,res)=>{
        try{
            var {order_id,message} = req.body;
            console.log(req.body);
            if(!order_id)
            {
                return res.send({
                    status:false,
                    errorMessage:"L'identifiant de la commande est requis"
                })
            }
            if(!message)
            {
                return res.send({
                    status:false,
                    errorMessage:"Le message est requis"
                })
            }
            const addData = {
                message:message
            };
            await orders.updateOne({_id:order_id},{
                "$push":{sellerNotes:addData}
                
            }).then((result)=>{
                return res.send({
                    status:true,
                    errorMessage:"Note ajoutée succès complet"
                })
            }).catch((error)=>{
                return res.send({
                    status:false,
                    errorMessage:error.message
                })
            })
        }catch(error){
            return res.send({
                status:false,
                errorMessage:error.message
            })
        }
    },
    getSellerNameForFilter:async(req,res)=>{
        try{
            await Seller.find({is_deleted:false},{fullname:1,shopname:1}).then((result)=>{
                return res.send({
                    status:true,
                    errorMessage:"Success",
                    data:result
                });
            }).catch((error)=>{
                return res.send({
                    status:false,
                    errorMessage:error.message
                });
            })
        }catch(error){
            return res.send({
                status:false,
                errorMessage:error.message
            });
        }
    }






}