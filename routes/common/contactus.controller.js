const Contactus=require("../../models/admin/Cotactus");
const {sendmail}=require("../../modules/sendmail");
const adminmodel=require("../../models/admin/Admin");
const path=require("path");
const notificationmodel = require("../../models/Notifications");
const client=require("../../models/user/User");
const provider=require("../../models/seller/Seller");
const NotificationPermission=require("../../models/NotificationPermission");
const OrderModel = require("../../models/orders/order");
const sellerModel=require("../../models/seller/Seller");
const mongoose=require("mongoose")
const WebapppagesModel = require("../../models/admin/WebapppagesModel");
const User = require('../../models/user/User');
const ejs = require("ejs"); 
const customConstant = require('../../helpers/customConstant');

const nodemailer = require("nodemailer");
const transport = nodemailer.createTransport({
   name: "Oblack",
   host: process.env.MAILER_HOST,
   port: process.env.MAILER_PORT,
   auth: {
     user: process.env.MAILER_EMAIL_ID,
     pass: process.env.MAILER_PASSWORD,
   }
});


   module.exports.contactus=async(req,res)=>{
      const title=req.body.title;
      const reason=req.body.reason;
      const message=req.body.message;
      const id=req.body.id;
      const type=req.body.type;
      const name=req.body.name;
      const docs=[];
      let user;
      const admin=await adminmodel.findOne({});
      if(type=="user"){
      const clientv=await client.findById(id);
      if(clientv){
         user=clientv
      }else{
         return res.send({
               status:false,
               message:"user does not exists"
         })
      }
      }else if(type=="provider"){
         const providerv=await provider.findById(id);
         if(providerv){
            user=providerv
         }else{
            return res.send({
                  status:false,
                  message:"user does not exists"
            })
         }
      }else{
         return res.send({
               status:false,
               message:"type is required"
         })
      }
      const pathtofile = path.resolve("views/notifications/contactus.ejs");
      const subject="some weired things";
      
      if (req.files?.docs) {
         for(let i=0;i<req.files.docs.length;i++){
         let file="userprofile/"+req.files.docs[i].filename;
         docs.push(file)
         }
         }
      const newRequest=new Contactus();
      newRequest.title=title;
      newRequest.reason=reason;
      newRequest.message=message;
      newRequest.id=id;
      newRequest.name=name;
      newRequest.type=type;
      newRequest.docs=docs;
      newRequest.save().then(async(result)=>{
         
         sendmail(pathtofile, 
               { name: user.first_name+" "+user.last_name,message:message,reason:reason }, 
               subject,
               admin?.email||process.env.ADMIN_EMAIL);
      })
      return res.send({
         status:true,
         message:"Le message a été envoyé avec succès.Un membre de notre équipe d'assistance à la clientèle prendra contact avec vous dans les 24 heures."
      })

   }

   module.exports.getcontactusbyid=async(req,res)=>{
      const id=req.params.id;
      const contactuspages=await Contactus.find({id:id});
      return res.send({
         status:true,
         message:"fetched success",
         data:contactuspages
      })
   }
   module.exports.contactusinfo=async(req,res)=>{
      const admin=await adminmodel.findOne({});
      return res.send({
         status:true,
         message:"fetched successfully",
         data:admin
      })
   }

   module.exports.getallnotificationsbyid=async(req,res)=>{
      const student_id=req.body.client_id;
      const type=req.body.type;
      let count=0;
   
      if(type=="unread"){
      count=(await notificationmodel.find({"to_id":student_id,"status":"unread"})).length;
      return res.send({
         status:true,
         message:"notifications fetched successfully",
         errmessage:"",
         
         count:count
      })
      }
      // const notifications=await notificationmodel.find({to_id:client_id});
      const notifications=await notificationmodel.aggregate([{$match:{to_id:student_id}},
         {$sort:{date:-1}}
      ]);
      
      if(type=="read"){
         await notificationmodel.updateMany({"to_id":student_id},{
            $set:{"status":"read"}
         })
         return res.send({
            status:true,
            message:"notifications fetched successfully",
            errmessage:"",
            data:notifications,
            count:count
         })
      }
   }

   module.exports.getnotificationsettings=async(req,res)=>{
   const id=req.params.id;
   console.log("id",id);
   const notificationpermission=await NotificationPermission.findOne({id:id});
   console.log("notificationpermission",notificationpermission)
   return res.send({
     status:true,
     message:"fetched success",
     data:notificationpermission
   })
   }

   module.exports.updatenotificationpermission=async(req,res)=>{
      const id=req.body.id;
      
   console.log("req.body",req.body)




      const user_type=req.body.user_type;
      const messagefromprovider=req.body.messagefromprovider;
      const messagefromoblack=req.body.messagefromoblack;
      const messagepromocode=req.body.messagepromocode;
      const emailpromocode=req.body.emailpromocode;
      const emailfromoblack=req.body.emailfromoblack;
      const emailfromprovider=req.body.emailfromprovider;
      const messagefromuser=req.body.messagefromuser;
      const emailfromuser=req.body.emailfromuser;
     
      const notipermission=await NotificationPermission.findOne({
        id:id
      });
      if(notipermission){

         

         notipermission.user_type=user_type
      notipermission.messagefromprovider=messagefromprovider;
      notipermission.messagefromoblack=messagefromoblack;
      notipermission.messagepromocode=messagepromocode;
      notipermission.emailpromocode=emailpromocode;
      notipermission.emailfromoblack=emailfromoblack;
      notipermission.emailfromprovider=emailfromprovider;
      notipermission.messagefromuser=messagefromuser
      notipermission.emailfromuser=emailfromuser
      
      notipermission.save().then((result)=>{
        return res.send({
          status:true,
          message:"Un record mis à jour pour un succès complet"
        })
      })
      }else{
      const newnotipermission=new NotificationPermission();
      newnotipermission.id=id;
      newnotipermission.user_type=user_type
      newnotipermission.messagefromprovider=messagefromprovider;
      newnotipermission.messagefromoblack=messagefromoblack;
      newnotipermission.messagepromocode=messagepromocode;
      newnotipermission.emailpromocode=emailpromocode;
      newnotipermission.emailfromoblack=emailfromoblack;
      newnotipermission.emailfromprovider=emailfromprovider;
      newnotipermission.messagefromuser=messagefromuser
      newnotipermission.emailfromuser=emailfromuser
      newnotipermission.save().then((result)=>{
        return res.send({
          status:true,
          message:"L'établissement d'un record ajoute au succès"
        })
      })
    
      }
    };

    module.exports.deletenotificationbyid=async(req,res)=>{
      await notificationmodel.findByIdAndDelete(req.params.id).then((result)=>{
         return res.send({
            status:true,
            message:"done"
         })
      })
    }

    module.exports.get_page_from_slug = async(req,res)=>{
      //console.log( req.params.url_slug);
      var url_slug = req.params.url_slug;
      //mongoose.set("debug",true);
      await WebapppagesModel.findOne({url_slug}).then((result)=>{
         return res.send({
            status:true,
            message:"Succès",
            data:result
         })
      })
    }

   module.exports.getUserNotification = async(req,res)=>{
      try{
         console.log(req.params);
         let user_id = req.params.id;
         if(!user_id)
         {
            return res.send({
               status:false,
               message:"L'identifiant de l'utilisateur est requis"
            })
         }
         let all_push = await notificationmodel.find({to_id:user_id},{message:1,title:1,notification_type:1,createdd_at:1}).sort({createdd_at:-1});
         return res.send({
            status:true,
            message:"Success",
            data:all_push
         });
      }catch(error){
         return res.send({
            status:false,
            message:error.message
         })
      }
   }
   module.exports.getSellerNotification = async(req,res)=>{
      try{
         console.log(req.params);
         let user_id = req.params.id;
         if(!user_id)
         {
            return res.send({
               status:false,
               message:"L'identifiant de l'utilisateur est requis"
            })
         }
         let all_push = await notificationmodel.find({to_id:user_id},{message:1,title:1,notification_type:1,createdd_at:1}).sort({createdd_at:-1});
         return res.send({
            status:true,
            message:"Success",
            data:all_push
         });
      }catch(error){
         return res.send({
            status:false,
            message:error.message
         })
      }
   }
   module.exports.getAdminNotification = async(req,res)=>{
      try{
         console.log(req.params);
         let user_id = req.params.id;
         let newMessageCount = 0;
         if(!user_id)
         {
            return res.send({
               status:false,
               message:"L'identifiant de l'utilisateur est requis"
            })
         }
        /// mongoose.set("debug",true);
         let all_push = await notificationmodel.find({to_id:user_id},{message:1,title:1,notification_type:1,createdd_at:1}).sort({createdd_at:-1});
         return res.send({
            status:true,
            message:"Success",
            data:all_push,
            newMessageCount:all_push.length
         });
      }catch(error){
         return res.send({
            status:false,
            message:error.message
         })
      }
   }

   
   module.exports.getDeleteNotificationById = async(req,res)=>{
      try{
         console.log(req.params);
         let id = req.params.id;
         if(!id)
         {
            return res.send({
               status:false,
               message:"L'identifiant est requis"
            })
         }
         let all_push = await notificationmodel.findOne({_id:id},{message:1,title:1,notification_type:1,createdd_at:1});
         if(!all_push)
         {
            return res.send({
               status:false,
               message:"Identifiant invalide"
            })
         }else{
            notificationmodel.deleteOne({_id:id}).exec((result)=>{
               return res.send({
                  status:true,
                  message:"Success",
                  data:result
               });
            });
         }
      }catch(error){
         return res.send({
            status:false,
            message:error.message
         })
      }
   }

   module.exports.getDeleteNotificationByUserId = async(req,res)=>{
      try{
         let user_id = req.params.id;
         if(!user_id)
         {
            return res.send({
               status:false,
               message:"L'identifiant de l'utilisateur est requis"
            })
         }

         let all_push = await notificationmodel.findOne({to_id:user_id},{message:1,title:1,notification_type:1,createdd_at:1});
         if(!all_push)
         {
            return res.send({
               status:false,
               message:"Identifiant invalide"
            })
         }else{
            notificationmodel.deleteMany({ to_id:user_id }).exec((result)=>{
               return res.send({
                  status:true,
                  message:"Success",
                  data:result
               });
            });
         }
      }catch(error){
         return res.send({
            status:false,
            message:error.message
         })
      }
   }
   module.exports.getSellerNotificationCount = async(req,res)=>{
      try{
         let user_id = req.params.id;
         if(!user_id)
         {
            return res.send({
               status:false,
               message:"L'identifiant de l'utilisateur est requis"
            })
         }
         let all_count = await notificationmodel.count({to_id:user_id,status:"unread"});
         return res.send({
            status:true,
            all_count:all_count,
            message:"Succès"
         })
      }catch(error)
      {
         return res.send({
            status:false,
            message:error.message
         })
      }
   }
   module.exports.markReadNotification = async(req,res)=>{
      try{
         var id = req.body.id;
         if(!id)
         {
            return res.send({
               status:false,
               message:"L'identifiant est requis"
            })
         }
         await notificationmodel.updateOne({_id:id},{status:"read"}).then((result)=>{
            return res.send({
               status:true,
               message:"Succès"
            })
         }).catch((error)=>{
            return res.send({
               status:false,
               message:error.message
            })
         });
      }catch(error)
      {
         return res.send({
            status:false,
            message:error.message
         })
      }
   }
   module.exports.checkEmailOrder = async(req,res)=>{
      try{
         //console.log("req.body.id ", req.body.id);
         var base_url_server = customConstant.base_url;
         var imagUrl = base_url_server+"public/uploads/logo.png";
         let id = req.body.id;
         let order_array = [];
         let seller_array = [];
         let order_rec_combo = await OrderModel.findOne({_id:id},{combo_id:1});
         if(order_rec_combo)
         {
            if(!order_rec_combo.combo_id)
            {
               //here single record
               //console.log("no combo id  order_rec_combo.combo_id ", order_rec_combo.combo_id);
               let all_order_combo = await OrderModel.find({_id:id} );
               //order_array.push(all_order_combo);
               order_array = all_order_combo;
            }else{
               //here multiple record
               //console.log("order_rec_combo.combo_id ", order_rec_combo.combo_id);
               let all_order_combo = await OrderModel.find({combo_id:order_rec_combo.combo_id} );
               //order_array.push(all_order_combo);
               order_array = all_order_combo;
            }
            for(let x=0; x<order_array.length; x++)
            {
               console.log(order_array[x]._id);
               console.log(order_array[x].combo_id);
               let provider_id = order_array[x].products[0].provider_id;
               let result_seller = await sellerModel.findOne({_id:provider_id},{fullname:1,shopname:1,email:1, countrycode:1, phone:1,pickupaddress:1} );
               seller_array.push(result_seller);
            }
            // first_name: 'Edward ',
            // last_name: 'Auditore',
            // email: 'edward@mailinator.com',
            // phone: '0897631256',
            let userId = order_array[0].userId;
            let result_user = await User.findOne({_id:userId},{first_name:1,last_name:1,email:1, country_code:1, phone:1 } );
            //console.log("result_seller ", result_seller);
            // const frenchdate=frenchDayDate(order_array[0].date_of_transaction);
            const today = new Date(order_array[0].date_of_transaction);
            const yyyy = today.getFullYear();
            let mm = today.getMonth() + 1; // Months start at 0!
            let dd = today.getDate();
            if (dd < 10) dd = '0' + dd;
            if (mm < 10) mm = '0' + mm;
            let formattedToday = mm + '.' + dd + '.' + yyyy;
            let hourss = today.getHours();
            let minutess = today.getMinutes();
            // console.log("hourss ",hourss);
            // console.log("minutess ",minutess);
            formattedToday = formattedToday +" "+hourss+"h"+minutess;
            // // console.log("formattedToday ",formattedToday);
            // // return false;
            // invoice=JSON.parse(JSON.stringify(invoice));
            order_array[0]["frenchdate"]=formattedToday;
            //console.log(order_array);return false;
            const filepath=path.join(__dirname, "../../views/admin-panel/paymentinvoice_3.ejs");
            //console.log(filepath)
            let less_val = order_array.length - 1;
            const html = await ejs.renderFile(filepath,{order_array:order_array,result_user:result_user,seller_array:seller_array,imagUrl:imagUrl,less_val:less_val });
            let subject = "Succès de la commande";
            var mainOptions = {
                from:process.env.MAILER_FROM_EMAIL_ID ,
                to: result_user.email,
                subject:subject ,
                html: html
            };
            // console.log("html data ======================>", mainOptions);
    
            //smtpTransport.sendMail(mainOptions, function (err, info) {
            transport.sendMail(mainOptions, function(err, info) {
                if (err) {
                    console.log(err);
                    //return true
                } else {
                    console.log('Message sent: ' + info);
                    //return true
                }
            });
            // return res.send({
            //    status:true,
            //    message:"Succès",
            //    data:order_array
            // });
         }
      }catch(e){
         console.log(e);
      }
   }