const express = require('express')
const router = express.Router();
const multer=require("multer");
const path=require("path");
const user_path=process.env.USER_PATH;
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.resolve(user_path))
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
  });
const upload2 = multer({ storage: storage });
const contactuscontroller=require("./contactus.controller");

const paymentController = require('./Payment.controller')

router.get("/getpayment/:id/:amount/:user_id/:method",paymentController.getPayment);
router.get("/getpaymentevent/:id/:amount/:user_id",paymentController.getpaymentevent);
//const url='<%=base_url%>/api/payment/cinetpaysuccess/<%=order_id%>/'+finalamount+'/'+promo/'+amount;

router.get("/getpaymentCashOnDelivery/:id/:amount/:user_id/:method",paymentController.getpaymentCashOnDelivery);

//http://localhost:9000/api/payment/cinetpaysuccess/65951345beb3414bc4bc2407/93.1/null/93.1
router.get("/cinetpaysuccess/:id/:finalamount/:promo/:amount",paymentController.cinetpaysuccess);
//cinetpaysuccess
//http://localhost:9000/api/payment/cinetpaysuccessCombo/LJOcnlUADNGFD0jcU2WX/64d4de8eb561063024242531/1343.1/null

http://localhost:9000/api/common/cinetPayEventSuccess/ZYfD3KxdaqEVKFrA0s6H/200

router.get("/cinetPayEventSuccess/:id/:amount",paymentController.cinetPayEventSuccess);

router.get("/cinetpaysuccessCombo/:id/:user_id/:finalamount/:promo",paymentController.cinetpaysuccessCombo);



router.get("/sucessSellerSubscription/:id/:amount",paymentController.sucessSellerSubscription);

router.get("/getPaymentForSub/:id/:amount",paymentController.getPaymentForSub);
router.get("/getpaymentforcombo/:id/:amount/:user_id/:method",paymentController.getpaymentforcombo);
router.post("/postPayment",paymentController.postPayment);
router.post("/postPaymentForSub",paymentController.postPaymentForSub);
//router.post("/postPaymentforcombo",paymentController.postPaymentforcombo);


router.get("/getpaymentforcomboCashOnDelivery/:id/:amount/:user_id/:method",paymentController.getpaymentforcomboCashOnDelivery);


router.post("/postPaymentforevent",paymentController.postPaymentforevent);
router.post("/contactus",upload2.fields([
    { 
      name: 'docs', 
      maxCount: 10
    }
  ]
  ), contactuscontroller.contactus);
router.get("/getcontactusbyid/:id",contactuscontroller.getcontactusbyid);
router.get("/paymentsucess/:payment_status",paymentController.paymentsucess);
router.get("/paymentsucess_sub/:payment_status",paymentController.paymentsucess_sub);

router.get("/paymentsucessEvent/:payment_status",paymentController.paymentsucessEvent);
 
//   router.get("/getallchatsbyserviceid/:service_id",chatcontroller.getallchatsbyId)
router.get("/contactusinfo",contactuscontroller.contactusinfo)
router.post("/getallnotificationsbyid",contactuscontroller.getallnotificationsbyid)
router.get("/deletenotificationbyid/:id",contactuscontroller.deletenotificationbyid)
router.post("/updatenotificationpermission", contactuscontroller.updatenotificationpermission);
router.get("/getnotificationsettings/:id",contactuscontroller.getnotificationsettings);
router.get("/get_page_from_slug/:url_slug",contactuscontroller.get_page_from_slug);

router.get("/getUserNotification/:id",contactuscontroller.getUserNotification);
router.get("/getSellerNotification/:id",contactuscontroller.getSellerNotification);
router.get("/getAdminNotification/:id",contactuscontroller.getAdminNotification);
router.get("/getDeleteNotificationById/:id",contactuscontroller.getDeleteNotificationById);
router.get("/getDeleteNotificationByUserId/:id",contactuscontroller.getDeleteNotificationByUserId);
router.get("/getSellerNotificationCount/:id",contactuscontroller.getSellerNotificationCount);

router.post("/markReadNotification",contactuscontroller.markReadNotification);
router.post("/checkEmailOrder",contactuscontroller.checkEmailOrder);




module.exports = router;