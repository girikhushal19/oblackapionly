const app = require("express");
const session = require("express-session");
const UserController=require("../../routes/user/UserController");
const SellerController=require("../../routes/seller/SellerController");
const AbuseReportController=require("../../routes/admin/AbuseReport.controller");
const GlobalSettingsController=require("../../routes/admin/GlobalSettings.controller")
const RefundController = require('../../routes/admin/Refund.controller');
const SellerRatingsController=require("../../routes/admin/SellerRatings.controller");
const AnnouncementController=require("../../routes/admin/Announcement.controller");
const ProductController=require("../../routes/products/ProductController");
const OrderController=require("../../routes/orders/OrderController");
const adminauth = require("../../middleware/adminauthtest");
const multer=require("multer");
const path=require("path");
const user_path=process.env.USER_PATH;
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      console.log("user_path "+user_path);
      cb(null, path.resolve(user_path))
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
  });
const uploadTo = multer({ storage: storage });
const admin_lawyer_profile_path=process.env.admin_lawyer_profile_path;
const admin_user_profile_path=process.env.admin_user_profile_path;
const upload1 = multer({ dest: path.resolve(admin_user_profile_path) });
const upload2 = multer({ dest: path.resolve(admin_lawyer_profile_path) });

const AdminpanelController = require("./adminpanel.controller");
const adminpanelrouter = app.Router();
adminpanelrouter.use(session({ secret: process.env.SESSIONSECRET,
  resave: true,
  saveUninitialized: true }));
adminpanelrouter.get("/", adminauth,AdminpanelController.renderhomepage);
adminpanelrouter.get("/renderhomepageSeller/:id", SellerController.sellerincome_dashboard_page);
adminpanelrouter.post("/renderhomepageSeller2", SellerController.sellerincome_dashboard_page);

adminpanelrouter.get("/users/:pageno",adminauth, AdminpanelController.users);
adminpanelrouter.get("/getSingleUsers/:user_id",adminauth, AdminpanelController.getSingleUsers);

adminpanelrouter.get("/allActiveUsersAdmin/:pageno",adminauth, AdminpanelController.allActiveUsersAdmin);
adminpanelrouter.get("/allInActiveUsersAdmin/:pageno",adminauth, AdminpanelController.allInActiveUsersAdmin);
adminpanelrouter.get("/allDeletedUsersAdmin/:pageno",adminauth, AdminpanelController.allDeletedUsersAdmin);


adminpanelrouter.post("/usersFilter",AdminpanelController.usersFilter);

 
adminpanelrouter.post("/users",adminauth,uploadTo.fields([
  { 
    name: 'photo', 
    maxCount: 10
  }
]
), UserController.createnewuser);
adminpanelrouter.get("/deleteuser/:id",UserController.deleteuser);
adminpanelrouter.get("/markuserasactive/:id",UserController.markUserAsActive)
adminpanelrouter.get("/markuserasinactive/:id",UserController.markUserAsInActive)

adminpanelrouter.post("/vendors", SellerController.getvendors);
adminpanelrouter.post("/activeVendors", SellerController.activeVendors);
adminpanelrouter.post("/inActiveVendors", SellerController.inActiveVendors);
adminpanelrouter.post("/getDeletedVendors", SellerController.getDeletedVendors);


adminpanelrouter.get("/getorderdetailbyorderid/:id", AdminpanelController.getorderdetailbyorderid);
adminpanelrouter.post("/updatevendors",uploadTo.fields([
  { 
    name: 'photo', 
    maxCount: 10
  }
]
), SellerController.createseller_second_update);
adminpanelrouter.get("/deletevendor/:id", SellerController.deletevendors);
adminpanelrouter.get("/markvendorasactive/:id", SellerController.markVendorasActive);
adminpanelrouter.get("/markvendorasInactive/:id", SellerController.markVendorasInActive);
adminpanelrouter.get("/markvendorasapproved/:id", SellerController.markAsApproved);
adminpanelrouter.get("/markvendorasunapproved/:id", SellerController.markAsUnApproved);
//abuse reports
adminpanelrouter.post('/addOrupdate',uploadTo.fields([
  { 
    name: 'photo', 
    maxCount: 10
  }
]
),AbuseReportController.addOrupdate);
adminpanelrouter.get('/deletereport/:id',AbuseReportController.delete)
adminpanelrouter.get('/markAsresolved/:id',AbuseReportController.markAsresolved)
adminpanelrouter.get('/markAsUnresolved/:id',AbuseReportController.markAsUnresolved)
adminpanelrouter.post('/getallreports',AbuseReportController.getallreports)

//refund 
adminpanelrouter.post('/addOrupdaterefund',RefundController.addOrupdate);
adminpanelrouter.get('/deleterefund/:id',RefundController.delete)
adminpanelrouter.get('/markAsdone/:id',RefundController.markAsdone)
adminpanelrouter.get('/markAsUndone/:id',RefundController.markAsUndone)
adminpanelrouter.get('/markAsCancelled/:id',RefundController.markAsCancelled)
adminpanelrouter.post('/getallrefunds',RefundController.getallrefunds)

//seller ratings
adminpanelrouter.post('/addOrupdatestorerating',uploadTo.fields([
  { 
    name: 'photo', 
    maxCount: 10
  }
]
),SellerRatingsController.addOrupdaterating)
adminpanelrouter.get('/deletestoreratingbyratingid/:id',SellerRatingsController.deleterating)
adminpanelrouter.get('/getallratingsbystoreid/:id',SellerRatingsController.getallratingsbystoreid)
adminpanelrouter.get('/getallstoresratings/:pageno',SellerRatingsController.getallstoresratings)

//announcements
adminpanelrouter.post('/makenewannouncements',AnnouncementController.newannouncement)
adminpanelrouter.get('/getallannouncements/:pageno',AnnouncementController.getallannouncements);

//reports
adminpanelrouter.post('/reports',AdminpanelController.reports)
adminpanelrouter.post('/alllogs',AdminpanelController.alllogs)
adminpanelrouter.get("/termsandcondition", AdminpanelController.termsandcondition);
adminpanelrouter.get("/privacypolicy", AdminpanelController.privacypolicy);
adminpanelrouter.get("/aboutoblack", AdminpanelController.aboutoblack);
 
//products
adminpanelrouter.post('/getallproductsforadmin',ProductController.getallproductsforadmin);
adminpanelrouter.post('/getallDeletedproductsforadmin',ProductController.getallDeletedproductsforadmin);


//orders
adminpanelrouter.post('/getallordersbyadmin',OrderController.getallordersbyadmin)

adminpanelrouter.post('/getAllProcessingOrdersByAdminCount',OrderController.getAllProcessingOrdersByAdminCount);
adminpanelrouter.post('/getAllProcessingOrdersByAdmin',OrderController.getAllProcessingOrdersByAdmin);


adminpanelrouter.post('/getAllRadyToPickUpByAdminCount',OrderController.getAllRadyToPickUpByAdminCount);
adminpanelrouter.post('/getAllRadyToPickUpByAdmin',OrderController.getAllRadyToPickUpByAdmin);

adminpanelrouter.post('/getAllPickedUpByAdminCount',OrderController.getAllPickedUpByAdminCount);
adminpanelrouter.post('/getAllPickedUpByAdmin',OrderController.getAllPickedUpByAdmin);

adminpanelrouter.post('/getAllCompletedByAdminCount',OrderController.getAllCompletedByAdminCount);
adminpanelrouter.post('/getAllCompletedByAdmin',OrderController.getAllCompletedByAdmin);

adminpanelrouter.post('/getAllUserCancledByAdminCount',OrderController.getAllUserCancledByAdminCount);
adminpanelrouter.post('/getAllUserCancledByAdmin',OrderController.getAllUserCancledByAdmin);


//global settings
adminpanelrouter.post("/updateoraddSettings",GlobalSettingsController.updateoraddSettings)
adminpanelrouter.get("/getglobalsettings",GlobalSettingsController.getglobalsettings);
module.exports = adminpanelrouter;

