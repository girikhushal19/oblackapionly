
const mongoose = require('mongoose');
const moment = require("moment")
require("../config/database").connect();
const bookingmodel=require("../models/events/Booking");
const OrganizerModel=require("../models/events/Organizer");

const checkifappointmentiscomplete = async () => {
    const appointments = await bookingmodel.find({});
    appointments.forEach(async (appointment) => {
        const current_date = new Date().toISOString().split("T")[0];
        let appointment_date = appointment.edate;
        let appointment_date_to_string = appointment_date.toISOString().split("T")[0];
        //  console.log( appointment_date_to_string,"appointment_date", current_date,"current_date",appointment.service_status,"appointment.service_status");
     
        let currenttimeformated=new Date(current_date)
        let enddateformated=new Date(appointment_date_to_string)
        const daysdiff=moment(currenttimeformated).diff(moment(enddateformated),"days");
        // console.log("idaysdiff",daysdiff, current_date , appointment_date);
        if (appointment.status == 0 && daysdiff>0) {

            appointment.status = 1;
            appointment.save();
            const org=await OrganizerModel.findById(appointment.org_id);
           // console.log("org",org);
            if(org){
                let newtotal=org?.totalpendingamount+appointment.payment_amount;
                await OrganizerModel.findByIdAndUpdate(org._id,{totalpendingamount:newtotal})
                
              
            }
        }
    })
}


module.exports = {
   
    checkifappointmentiscomplete
}
